package com.epam.training.homework4.task1;

import org.junit.Test;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestAtm {

    @Test
    public void testAtmBalanceNoCard() {
        Card card = new CreditCard("test Owner");
        Atm atm = new Atm();
        assertEquals("ATM didn't say aboute card absence", Atm.CARD_NOT_INSERTED, atm.displayBalance());
    }
    // display balance
    @Test
    public void testAtmBalanceCreditCardViewBalance() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        assertEquals("balance string is wrong", "10.00", atm.displayBalance());
    }

    // scale
    @Test
    public void testAtmBalanceSetScalePositive() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        atm.setScale(4);
        assertEquals("scale of the balance string is wrong", "10.0000", atm.displayBalance());
    }
    @Test
    public void testAtmBalanceSetScaleZero() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        atm.setScale(0);
        assertEquals("scale of the balance string is wrong", "10", atm.displayBalance());
    }

    @Test
    public void testAtmBalanceSetScaleNegative() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        atm.setScale(-1);
        assertEquals("scale of the balance string is wrong", "10.00", atm.displayBalance());
    }

    // add / withdrow money
    @Test
    public void testAtmAddMoney() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        boolean  transactionResult = atm.addMoneyToCard(new BigDecimal("5"));
        assertTrue("Addition money to card failed", transactionResult);
        assertEquals("Addition money to card works wrong", "15.00", atm.displayBalance());
    }
    @Test
    public void testAtmWithdrowMoney() {
        Card card = new CreditCard("test Owner", BigDecimal.TEN);
        Atm atm = new Atm();
        atm.assignCard(card);
        boolean  transactionResult = atm.withdrowFromCard(new BigDecimal("7"));
        assertTrue("Withdrow money from card failed", transactionResult);
        assertEquals("Withdrow money from card works wrong", "3.00", atm.displayBalance());
    }


}
