package com.epam.training.homework4.task1;

import org.junit.Test;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestDebitCard {
    // getOwner
    @Test
    public void testDebitCardGetOwnerName() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertEquals("getOwner works wrong", "test Owner", debitCard.getOwner());
    }



    //  Constructors
    @Test
    public void testDebitCardConstructorByOwner() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", debitCard.getOwner());
        assertEquals("Constructor works wrong: balance not equal to zero", new BigDecimal(Card.DEFAULT_BALANCE), debitCard.getBalance());
    }

    @Test
    public void testDebitCardConstructorByHolderAndBalancePositive() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("10"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", debitCard.getOwner());
        assertEquals("Constructor works wrong: balance differs from expected", BigDecimal.TEN, debitCard.getBalance());
    }

    @Test
    public void testDebitCardConstructorByHolderAndBalanceNegative() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("-100.5"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", debitCard.getOwner());
        BigDecimal expectedBalanceValue = new BigDecimal(DebitCard.DEFAULT_BALANCE);
        assertEquals("Result balance differs from expected", expectedBalanceValue, debitCard.getBalance());
    }

    @Test
    public void testCardConstructorByOwnerAndBalanceNulls() {
        DebitCard card = new DebitCard(null, null);
        assertEquals("Constructor works wrong: owner name differs from expected", DebitCard.DEFAULT_CARD_OWNER_NAME, card.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(DebitCard.DEFAULT_BALANCE), card.getBalance());
    }

    @Test
    public void testDebitCardConstructorByOwnerWithNull() {
        DebitCard debitCard = new DebitCard(null);
        assertEquals("Constructor works wrong: owner name differs from expected", DebitCard.DEFAULT_CARD_OWNER_NAME, debitCard.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(DebitCard.DEFAULT_BALANCE), debitCard.getBalance());
    }

    // add balance
    @Test
    public void testCardAddBalancePositive() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = debitCard.addToBalance(new BigDecimal("45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(58.6);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertTrue("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNegative() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = debitCard.addToBalance(new BigDecimal("-45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNullReference() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = debitCard.addToBalance(null);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    // withdrowFromBalance
    @Test
    public void testCardTakeFromBalancePositiveValue() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = debitCard.withdrowFromBalance(new BigDecimal("80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceNegativeValue() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = debitCard.withdrowFromBalance(new BigDecimal("-80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceZeroValue() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = debitCard.withdrowFromBalance(BigDecimal.ZERO);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, debitCard.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    // Another currency
    @Test
    public void testCardAnotherCurrencyRateIsOne() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = debitCard.getBalance(BigDecimal.ONE,2);
        String expectedCardBalance = "20.00";
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", debitCard.getOwner());
        assertEquals("Converted balance differs from expected", expectedCardBalance, anotherCurrencyValue);
        assertEquals("Result balance differs from expected", new BigDecimal("20"), debitCard.getBalance());
    }

    @Test
    public void testCardAnotherCurrencyRateIsOneAndNegativeScale() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = debitCard.getBalance(BigDecimal.ONE,-1);
        String expectedCardBalance = "20.00";
        assertEquals("Another currency balance differs from expected", expectedCardBalance, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyZeroRate() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = debitCard.getBalance(BigDecimal.ZERO,2);
        assertEquals("Conversion to another currency successed, but must fail", DebitCard.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencNegativeRate() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(-1);
        String anotherCurrencyValue = debitCard.getBalance(ratioRate,2);
        assertEquals("Conversion to another currency successed, but must fail", DebitCard.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyRateHalf() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(0.5);
        String anotherCurrencyValue = debitCard.getBalance(ratioRate,2);
        String ExpectedCardBalance = "40.00";
        assertEquals("Another currency balance differs from expected", ExpectedCardBalance, anotherCurrencyValue);
    }

    // display balance
    @Test
    public void testDisplayBalanceScalePositive() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20.1234"));
        String actualText = debitCard.displayBalance(2);
        String expectedText = "20.12";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    @Test
    public void testDisplayBalanceScaleNegative() {
        DebitCard debitCard = new DebitCard("test Owner", new BigDecimal("20.1234"));
        String actualText = debitCard.displayBalance(-2);
        String expectedText = "20";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    // isBlankString
    @Test
    public void testIsBlankStringNormalString() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertFalse("isBlankString failed for normal string",
                debitCard.isBlankString("abc"));
    }

    @Test
    public void testIsBlankStringNull() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertTrue("isBlankString failed for null-pointer",
                debitCard.isBlankString(null));
    }

    @Test
    public void testIsBlankStringSpacesAndTabs() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertTrue("isBlankString failed for string with spaces and tabs",
                debitCard.isBlankString("  \t \t"));
    }

    @Test
    public void testIsBlankStringEmpty() {
        DebitCard debitCard = new DebitCard("test Owner");
        assertTrue("isBlankString failed for empty",
                debitCard.isBlankString(""));
    }
}

