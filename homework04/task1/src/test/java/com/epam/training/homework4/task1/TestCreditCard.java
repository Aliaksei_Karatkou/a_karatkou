package com.epam.training.homework4.task1;

import org.junit.Test;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestCreditCard {
    // getOwner
    @Test
    public void testCreditCardGetOwnerName() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertEquals("getOwner works wrong", "test Owner", creditCard.getOwner());
    }

    // getBalance
    @Test
    public void testCreditCardGetBalance() {
        CreditCard creditCard = new CreditCard("test Owner");
        creditCard.balance = BigDecimal.TEN;
        assertEquals("getBalance works wrong", BigDecimal.TEN, creditCard.getBalance());
    }

    //  Constructors
    @Test
    public void testCreditCardConstructorByOwner() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", creditCard.getOwner());
        assertEquals("Constructor works wrong: balance not equal to zero", new BigDecimal(CreditCard.DEFAULT_BALANCE), creditCard.getBalance());
    }

    @Test
    public void testCreditCardConstructorByHolderAndBalancePositive() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("10"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", creditCard.getOwner());
        assertEquals("Constructor works wrong: balance differs from expected", BigDecimal.TEN, creditCard.getBalance());
    }

    @Test
    public void testCreditCardConstructorByHolderAndBalanceNegative() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("-100.5"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", creditCard.getOwner());
        BigDecimal expectedBalanceValue = new BigDecimal(CreditCard.DEFAULT_BALANCE);
        assertEquals("Result balance differs from expected", expectedBalanceValue, creditCard.getBalance());
    }

    @Test
    public void testCardConstructorByOwnerAndBalanceNulls() {
        CreditCard card = new CreditCard(null, null);
        assertEquals("Constructor works wrong: owner name differs from expected", CreditCard.DEFAULT_CARD_OWNER_NAME, card.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(CreditCard.DEFAULT_BALANCE), card.getBalance());
    }

    @Test
    public void testCreditCardConstructorByOwnerWithNull() {
        CreditCard creditCard = new CreditCard(null);
        assertEquals("Constructor works wrong: owner name differs from expected", CreditCard.DEFAULT_CARD_OWNER_NAME, creditCard.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(CreditCard.DEFAULT_BALANCE), creditCard.getBalance());
    }

    // add balance
    @Test
    public void testCardAddBalancePositive() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = creditCard.addToBalance(new BigDecimal("45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(58.6);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertTrue("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNegative() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = creditCard.addToBalance(new BigDecimal("-45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNullReference() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = creditCard.addToBalance(null);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    // withdrowFromBalance
    @Test
    public void testCardTakeFromBalancePositiveValue() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = creditCard.withdrowFromBalance(new BigDecimal("80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(-67);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertTrue("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceNegativeValue() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = creditCard.withdrowFromBalance(new BigDecimal("-80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceZeroValue() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = creditCard.withdrowFromBalance(BigDecimal.ZERO);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, creditCard.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    // Another currency
    @Test
    public void testCardAnotherCurrencyRateIsOne() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = creditCard.getBalance(BigDecimal.ONE,2);
        String expectedCardBalance = "20.00";
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", creditCard.getOwner());
        assertEquals("Converted balance differs from expected", expectedCardBalance, anotherCurrencyValue);
        assertEquals("Result balance differs from expected", new BigDecimal("20"), creditCard.getBalance());
    }

    @Test
    public void testCardAnotherCurrencyRateIsOneAndNegativeScale() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = creditCard.getBalance(BigDecimal.ONE,-1);
        String expectedCardBalance = "20.00";
        assertEquals("Another currency balance differs from expected", expectedCardBalance, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyZeroRate() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = creditCard.getBalance(BigDecimal.ZERO,2);
        assertEquals("Conversion to another currency successed, but must fail", CreditCard.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencNegativeRate() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(-1);
        String anotherCurrencyValue = creditCard.getBalance(ratioRate,2);
        assertEquals("Conversion to another currency successed, but must fail", CreditCard.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyRateHalf() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(0.5);
        String anotherCurrencyValue = creditCard.getBalance(ratioRate,2);
        String ExpectedCardBalance = "40.00";
        assertEquals("Another currency balance differs from expected", ExpectedCardBalance, anotherCurrencyValue);
    }

    // display balance
    @Test
    public void testDisplayBalanceScalePositive() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20.1234"));
        String actualText = creditCard.displayBalance(2);
        String expectedText = "20.12";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    @Test
    public void testDisplayBalanceScaleNegative() {
        CreditCard creditCard = new CreditCard("test Owner", new BigDecimal("20.1234"));
        String actualText = creditCard.displayBalance(-2);
        String expectedText = "20";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    // isBlankString
    @Test
    public void testIsBlankStringNormalString() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertFalse("isBlankString failed for normal string",
                creditCard.isBlankString("abc"));
    }

    @Test
    public void testIsBlankStringNull() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertTrue("isBlankString failed for null-pointer",
                creditCard.isBlankString(null));
    }

    @Test
    public void testIsBlankStringSpacesAndTabs() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertTrue("isBlankString failed for string with spaces and tabs",
                creditCard.isBlankString("  \t \t"));
    }

    @Test
    public void testIsBlankStringEmpty() {
        CreditCard creditCard = new CreditCard("test Owner");
        assertTrue("isBlankString failed for empty",
                creditCard.isBlankString(""));
    }
}
