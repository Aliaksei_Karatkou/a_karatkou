package com.epam.training.homework4.task1;

import java.math.BigDecimal;

/**
 * Class Card implements homework 3 task 1.
 */
public class Card {

    /**
     * String which used to inform about error in currency conversion.
     */
    static final String MESSAGE_CONVERSION_ERROR = "conversion error";

    /**
     * Scale used to format currency values of current balance and
     * conversion result.
     */
    static final int DEFAULT_DISPLAY_SCALE = 2;

    /**
     * Default owner of a card.
     * Used if owner name is incorrect
     */
    static final String DEFAULT_CARD_OWNER_NAME = "Owner unknown";

    /**
     * Default card balance. Also used if created card with incorrect balance.
     */
    static final String DEFAULT_BALANCE = "0";

    /**
     * Card's owner name.
     */
    private final String ownerName;

    /**
     * Card's balance.
     */
    protected BigDecimal balance;

    /**
     * Constructor, uses only owner name. Sets balance to zero.
     *
     * @param cardOwnerName Owner name
     */
    public Card(final String cardOwnerName) {
        this(cardOwnerName, null);
    }

    /**
     * Constructor, use owner name and initial balace.
     * Initial balance mustn't be negative
     * If initial balance is negative or null then
     * balance sets to DEFAULT_BALANCE
     * card owner name is trimmed.
     *
     * @param cardOwnerName  Owner name.
     *                       If ownerName is null then
     *                       DEFAULT_CARD_OWNER_NAME used
     * @param initialBalance Initial card's balance
     */
    public Card(final String cardOwnerName, final BigDecimal initialBalance) {
        if (isBlankString(cardOwnerName)) {
            this.ownerName = DEFAULT_CARD_OWNER_NAME;
        } else {
            this.ownerName = cardOwnerName.trim();
        }
        if (initialBalance != null) {
            if (initialBalance.signum() < 0) {
                balance = new BigDecimal(DEFAULT_BALANCE);
            } else {
                balance = initialBalance;
            }
        } else {
            balance = new BigDecimal(DEFAULT_BALANCE);
        }
    }

    /**
     * Checks if string is null or blank.
     *
     * @param string String to check
     * @return false if string exists and not blank
     */
    protected final boolean isBlankString(final String string) {
        return string == null || string.trim().isEmpty();
    }

    /**
     * returns current card's balance.
     *
     * @return current balance
     */
    public final BigDecimal getBalance() {
        return balance;
    }

    /**
     * Reurns name of the card owner.
     *
     * @return owner name
     */
    public final String getOwner() {
        return ownerName;
    }

    /**
     * Adds money to card.
     * Only positive amount of money allowed to add.
     *
     * @param moneyToAdd Money amount to add
     * @return true if operation was successful, false otherwise
     */
    public final boolean addToBalance(final BigDecimal moneyToAdd) {
        boolean operationSuccessed = false;
        if ((null != moneyToAdd) && (moneyToAdd.signum() > 0)) {
            this.balance = balance.add(moneyToAdd);
            operationSuccessed = true;
        }
        return operationSuccessed;
    }

    /**
     * Withdrow money from card.
     * Only positive amount of money allowed to withdrow
     *
     * @param moneyToWithdrow Money amount to withdrow
     * @return true if operation was sucessfull, false otherwise
     */
    public boolean withdrowFromBalance(final BigDecimal moneyToWithdrow) {
        boolean operationSuccessed = false;
        if ((null != moneyToWithdrow) && (moneyToWithdrow.signum() > 0)) {
            this.balance = balance.subtract(moneyToWithdrow);
            operationSuccessed = true;
        }
        return operationSuccessed;
    }

    /**
     * Returns string which represents balance with required scale.
     * Only positive values if the scale is allowed.
     * If scale is negative then used "0" as scale value.
     *
     * @param displayScale Scale to use
     * @return String with card balance
     */
    public final String displayBalance(final int displayScale) {
        BigDecimal displayBigDecimal = balance;
        int usingScale = displayScale;
        if (displayScale < 0) {
            usingScale = 0;
        }
        displayBigDecimal = displayBigDecimal.setScale(usingScale,
                BigDecimal.ROUND_HALF_UP);
        return displayBigDecimal.toString();
    }

    /**
     * Returns balance in diferent currency using exchange rate.
     * Using exchange rate must be positive.
     *
     * @param exchangeRate exchange rate of required currency to card currency
     * @param accountScale Used scale. Mustn't be negative
     *                     if parameter less than zero then use
     *                     DEFAULT_DISPLAY_SCALE
     *                     value instead.
     * @return string with the card's balance in diffenect currency
     * or an error message
     */
    public final String getBalance(final BigDecimal exchangeRate,
                                   final int accountScale) {
        if (exchangeRate.signum() > 0) {
            int usingScale = accountScale;
            if (accountScale < 0) {
                usingScale = DEFAULT_DISPLAY_SCALE;
            }
            BigDecimal anotherCurrencyBalance = balance.divide(exchangeRate,
                    usingScale, BigDecimal.ROUND_HALF_UP);
            return anotherCurrencyBalance.toString();
        } else {
            return MESSAGE_CONVERSION_ERROR;
        }
    }
}
