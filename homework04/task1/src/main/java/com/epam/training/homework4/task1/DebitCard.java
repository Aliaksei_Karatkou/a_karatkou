package com.epam.training.homework4.task1;

import java.math.BigDecimal;

/**
 * Debit card implementation class.
 */
public class DebitCard extends Card {

    /**
     * Constructor. Create card by owner name.
     *
     * @param cardOwnerName Owner name
     */
    public DebitCard(final String cardOwnerName) {
        super(cardOwnerName);
    }

    /**
     * Constructor. Create card by owner name and initial card's balance.
     *
     * @param cardOwnerName  Owner name
     * @param initialBalance Initial balance
     */
    public DebitCard(final String cardOwnerName,
                     final BigDecimal initialBalance) {
        super(cardOwnerName, initialBalance);
    }

    /**
     * Withdrow from card. Result balance mustn't be negative
     *
     * @param moneyToWithdrow Money amount to withdrow
     * @return true if operation is successful, false otherwise
     */
    @Override
    public boolean withdrowFromBalance(final BigDecimal moneyToWithdrow) {
        BigDecimal moneyLeft = getBalance();
        if (moneyLeft.compareTo(moneyToWithdrow) >= 0) {
            return super.withdrowFromBalance(moneyToWithdrow);
        } else {
            return false;
        }
    }
}
