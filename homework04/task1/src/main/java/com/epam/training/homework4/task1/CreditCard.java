package com.epam.training.homework4.task1;

import java.math.BigDecimal;

/**
 * Class CreditCard only inherits parent class Card.
 */
public class CreditCard extends Card {

    /**
     * Constructor.
     * @param cardOwnerName Card's owner name
     */
    public CreditCard(final String cardOwnerName) {
        super(cardOwnerName);
    }

    /**
     * Constructor.
     * @param cardOwnerName Card's owner name
     * @param initialBalance Initial balance
     */
    public CreditCard(final String cardOwnerName,
                      final BigDecimal initialBalance) {
        super(cardOwnerName, initialBalance);
    }
}
