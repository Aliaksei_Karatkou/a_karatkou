package com.epam.training.a_karatkou.homework04;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class TestTask2 {

    // Bublesort
    @Test
    public void testBoubleSorterResultOrder1() {
        int []handledArray = new int [] {6,5,2};
        int [] expectedArray= new int [] {2,5,6};
        BoubleSorter boubleSorter = new BoubleSorter();
        boubleSorter.execute(handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testBoubleSorterResultOrder2() {
        int handledArray[] = new int [] {7};
        int expectedArray[] = new int [] {7};
        BoubleSorter boubleSorter = new BoubleSorter();
        boubleSorter.execute (handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testBoubleSorterResultOrder3() {
        int handledArray[] = new int [] {-1,2,5,-1};
        int expectedArray[] = new int [] {-1,-1,2,5};
        BoubleSorter boubleSorter = new BoubleSorter();
        boubleSorter.execute(handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testBoubleSorterEmptyArray() {
        int handledArray[] = new int [] {};
        int expectedArray[] = new int [] {};
        BoubleSorter boubleSorter = new BoubleSorter();
        boubleSorter.execute(handledArray);
        assertTrue("Returned wrong items", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testBoubleSorterNullArrayCheckArray() {
        int handledArray[] = null;
        BoubleSorter boubleSorter = new BoubleSorter();
        boubleSorter.execute(handledArray);
        assertNull("Returned not null-array",handledArray);
    }

    @Test
    public void testBoubleSorterNullArrayReturnCode() {
        int handledArray[] = null;
        BoubleSorter boubleSorter = new BoubleSorter();
        int resultCode = boubleSorter.execute(handledArray);
        assertEquals("successful code returned when error-code is expected",Task2.SORTING_METHOD_BUBBLE | Task2.SORTING_ABORTED, resultCode);
    }

    // Selection sort
    @Test
    public void testSelectionSorterResultOrder1() {
        int []handledArray = new int [] {6,5,2};
        int [] expectedArray= new int [] {2,5,6};
        SelectionSorter selectionSorter = new SelectionSorter();
        selectionSorter.execute(handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testSelectionSorterResultOrder2() {
        int handledArray[] = new int [] {7};
        int expectedArray[] = new int [] {7};
        SelectionSorter selectionSorter = new SelectionSorter();
        selectionSorter.execute (handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testSelectionSorterResultOrder3() {
        int handledArray[] = new int [] {-1,2,5,-1};
        int expectedArray[] = new int [] {-1,-1,2,5};
        SelectionSorter selectionSorter = new SelectionSorter();
        selectionSorter.execute(handledArray);
        assertTrue("Returned items are in wrong order", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testSelectionSorterEmptyArray() {
        int handledArray[] = new int [] {};
        int expectedArray[] = new int [] {};
        SelectionSorter selectionSorter = new SelectionSorter();
        selectionSorter.execute(handledArray);
        assertTrue("Returned wrong items", Arrays.equals(expectedArray,handledArray));
    }

    @Test
    public void testSelectionSorterNullArrayCheckArray() {
        int handledArray[] = null;
        SelectionSorter selectionSorter = new SelectionSorter();
        selectionSorter.execute(handledArray);
        assertNull("Returned not null-array",handledArray);
    }

    @Test
    public void testSelectionSorterNullArrayReturnCode() {
        int handledArray[] = null;
        SelectionSorter selectionSorter = new SelectionSorter();
        int resultCode = selectionSorter.execute(handledArray);
        assertEquals("successful code returned when error-code is expected",Task2.SORTING_METHOD_SELECTION | Task2.SORTING_ABORTED, resultCode);
    }

    // Strategy pattern
    @Test
    public void testSelectionSorterInvoke() {
        SortingStrategy strategy = new SelectionSorter();
        int[] testArray = {};
        SortingContext context = new SortingContext(strategy);
        int invokedMethodCode = context.execute(testArray);
        assertEquals("Executed wrong sorting method", Task2.SORTING_METHOD_SELECTION, invokedMethodCode);
    }

    @Test
    public void testBoubleSorterInvoke() {
        SortingStrategy strategy = new BoubleSorter();
        int[] testArray = {};
        SortingContext context = new SortingContext(strategy);
        int invokedMethodCode = context.execute(testArray);
        assertEquals("Executed wrong sorting method", Task2.SORTING_METHOD_BUBBLE, invokedMethodCode);
    }



}
