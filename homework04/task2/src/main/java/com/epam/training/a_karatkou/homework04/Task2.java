package com.epam.training.a_karatkou.homework04;

/**
 * Interface for  implementing strategy pattern.
 */
interface SortingStrategy {
    /**
     * method to call array sorting.
     *
     * @param array array to sort
     * @return result code (for testing)
     */
    int execute(int[] array);
}

/**
 * Class BoubleSorter implements bouble sorting.
  */
class BoubleSorter implements SortingStrategy {
    /**
     * Execute bouble sorting.
     *
     * @param array array to sort.
     * @return used sorting algorithm code (Task2.SORTING_METHOD_BUBBLE)
     * In case of error (using null as array parameter) returns Task2
     * .SORTING_METHOD_BUBBLE binary OR-ed with Task2.SORTING_ABORTED
     */
    public int execute(final int[] array) {
        if (array == null) {
            return Task2.SORTING_METHOD_BUBBLE | Task2.SORTING_ABORTED;
        }
        for (int i = 0; i < array.length - 1; i++) {
            boolean exchangeOccured = false;
            for (int j = 0; j < (array.length - 1) - i; j++) {
                if (array[j] > array[j + 1]) {
                    int tempValue = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tempValue;
                    exchangeOccured = true;
                }
            }
            if (!exchangeOccured) {
                break;
            }
        }
        return Task2.SORTING_METHOD_BUBBLE;
    }
}

/**
 * Class SelectionSorter implements selection sorting.
 */
class SelectionSorter implements SortingStrategy {
    /**
     * Execute selection sorting.
     *
     * @param array array to sort.
     * @return used sorting algorithm code (Task2.SORTING_METHOD_SELECTION)
     * In case of error (using null as array parameter) returns Task2
     * .SORTING_METHOD_SELECTION binary OR-ed with Task2.SORTING_ABORTED
     */
    public int execute(final int[] array) {
        if (array == null) {
            return (Task2.SORTING_METHOD_SELECTION | Task2.SORTING_ABORTED);
        }
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j <= array.length - 1; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            if (i != min) {
                int tempValue = array[i];
                array[i] = array[min];
                array[min] = tempValue;
            }
        }
        return Task2.SORTING_METHOD_SELECTION;
    }
}

/**
 * Class SortingContext used as Context-class according to strategy pattern.
 */
class SortingContext {
    /**
     * Sorting stategy object to use.
     */
    private SortingStrategy strategy;

    /**
     * Constructor, creates and defines using strategy.
     *
     * @param initialStrategy - initial strategy
     */
    SortingContext(final SortingStrategy initialStrategy) {
        strategy = initialStrategy;
    }

    /**
     * Method invokes chosen strategy method execution for the required array.
     *
     * @param array integer array to sort
     * @return Code for the chosen sorting method and possible error.
     */
    int execute(final int[] array) {
        return strategy.execute(array);
    }
}

/**
 * Main class for the homework task. Used for constants definition.
 * Doesn't contain another functionality.
 */
public final class Task2 {

    /**
     * Indicates chosen sorting strategy for a buble sorting. Used for
     * testing purpose
     */
    public static final int SORTING_METHOD_BUBBLE = 1;

    /**
     * Indicates chosen sorting strategy for a selection sorting. Used for
     * testing purpose
     */
    public static final int SORTING_METHOD_SELECTION = 2;

    /**
     * Indicates an error. Binary OR-ed with a strategy chosen code.
     */
    public static final int SORTING_ABORTED = 4;

    /**
     * Hides default constructor.
     */
    private Task2() {

    }

    /**
     * Task's main method.
     */
    public static void main() {
        // nothing there. Please use tests (junit) to verify this homework.
        // This task doesn't require user interaction.
    }
}

