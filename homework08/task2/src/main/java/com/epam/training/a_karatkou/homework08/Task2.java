package com.epam.training.a_karatkou.homework08;

/**
 * Class holds Enum of professions.
 */
public final class Task2 {
    /**
     * Professions enumeration.
     */
    enum SKILLS {
        /**
         * Some skill №1.
         */
        SKILL_1,
        /**
         * Some skill №2.
         */
        SKILL_2,
        /**
         * Some skill №3.
         */
        SKILL_3,
        /**
         * Some skill №4.
         */
        SKILL_4
    }


    /**
     * Hidden constructor.
     */
    private Task2() {

    }

    /**
     * Main class.
     * @param args command line arguments (ignored).
     */
    public static void main(final String[] args) {
        // please use the tests.
    }


}
