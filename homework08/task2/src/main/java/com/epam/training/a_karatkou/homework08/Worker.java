package com.epam.training.a_karatkou.homework08;


import java.util.HashSet;

/**
 * Class represents worker entity.
 */
class Worker {
    /**
     * Set of skills of the worker.
     */
    protected HashSet<Task2.SKILLS> skills;

    /**
     * Default constructor.
     */
    Worker() {
        skills = new HashSet<>();
    }

    /**
     * Adds skills to worker.
     *
     * @param newProfession profession to add
     */

    public void addSkill(final Task2.SKILLS newProfession) {
        skills.add(newProfession);
    }

    /**
     * Gets professions set (clone).
     *
     * @return cloned set of professions
     */

    public HashSet<Task2.SKILLS> getProfessions() {
        HashSet<Task2.SKILLS> professionsCloned;
        return new HashSet<>(skills);
    }

    /**
     * calculatess hashCode.
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += skills.size();
        for (Task2.SKILLS skill : skills
        ) {
            hash = (hash << 1) + skill.ordinal();
        }

        return hash;
    }

    /**
     * checks if the worker equals to another one.
     *
     * @param obj Other object to compare
     * @return true if workers are equal. false otherwise.
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Worker otherWorker = (Worker) obj;
        return skills.equals(otherWorker.skills);
    }


}

