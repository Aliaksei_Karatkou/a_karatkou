package com.epam.training.a_karatkou.homework08;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class represents a brigade.
 */
class Brigade implements Iterable<Worker> {
    /**
     * List of the workers.
     */
    protected ArrayList<Worker> workers;
    /**
     * Salary of this brigade.
     */
    protected BigDecimal salary;
    /**
     * Name of this brigade.
     */
    protected String name;

    /**
     * Constructor.
     *
     * @param brigadeSalary Wished salary.
     * @param brigadeName   Brigade's name.
     */
    Brigade(final BigDecimal brigadeSalary, final String brigadeName) {
        if (brigadeName == null) {
            throw new IllegalArgumentException("Brigade's name is null");
        }
        if (brigadeSalary.signum() < 0) {
            throw new IllegalArgumentException("Brigade's salary must not be "
                    + "negative");
        }
        workers = new ArrayList<>();
        salary = brigadeSalary;
        name = brigadeName;
    }


    /**
     * Returns workers count.
     *
     * @return workers count
     */
    public int getWorkersCount() {
        return workers.size();
    }

    /**
     * Returns brigade's wished salary.
     *
     * @return salary value
     */
    public BigDecimal getSalary() {
        return salary;
    }

    /**
     * Return name of the brigade.
     *
     * @return name of the brigade.
     */
    public String getName() {
        return name;
    }


    /**
     * Adds worker to the brigade.
     *
     * @param worker Worker object
     * @throws IllegalArgumentException      if worker is null
     * @throws IllegalObjectContentException if  worker hasn't any skill.
     */
    public void addWorker(final Worker worker) {
        if (worker == null) {
            throw new IllegalArgumentException("Worker is null");
        }
        if (worker.skills.isEmpty()) {
            throw new IllegalObjectContentException("A worker must be able to"
                    + " do at least one work");
        }
        workers.add(worker);
    }

    /**
     * Iterator class.
     *
     * @return new iterator
     */
    public Iterator<Worker> iterator() {
        return new Iterator<Worker>() {
            protected int currentWorkerIndex = 0;

            /**
             * implemets hasNext of the iterator interface.
             * @return true if collection has next value
             */
            @Override
            public boolean hasNext() {
                return currentWorkerIndex < workers.size();
            }

            /**
             * implements next() of the iterator interface.
             * @return next item
             * @throws NoSuchElementException if there isn't more elements
             */
            @Override
            public Worker next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return workers.get(currentWorkerIndex++);
            }

            /**
             * Stub.
             * @throws UnsupportedOperationException if called.
             */
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }


    /**
     * calculatess hashCode.
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        int hash = name.hashCode();
        hash += salary.hashCode() << 1;
        hash += workers.size();
        for (Worker worker : workers
        ) {
            hash = (hash << 2) + worker.hashCode();
        }
        return hash;
    }

    /**
     * checks if the brigade equals to another one.
     *
     * @param obj Other object to compare
     * @return true if brigades are equal. false otherwise.
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Brigade otherBrigade = (Brigade) obj;

        if (!name.equals(otherBrigade.name)
                || (workers.size() != otherBrigade.workers.size())
                || (salary.compareTo(otherBrigade.salary) != 0)) {
            return false;
        }
        for (int i = 0; i < workers.size(); i++) {
            if (!workers.get(i).equals(otherBrigade.workers.get(i))) {
                return false;
            }
        }
        return true;
    }

}

