package com.epam.training.a_karatkou.homework08;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class implements tender.
 * Every worker can do only one-skilled work
 * (but may have many skills)
 */
class Tender {
    /**
     * Map requider skill -> required workers.
     */
    protected HashMap<Task2.SKILLS, Integer> requiredWorkers =
            new HashMap<>();

    /**
     * Allowed budget.
     */
    protected BigDecimal budget;
    /**
     * Total required workers count.
     */
    protected int requiredWorkersCount = 0;
    /**
     * Array of the brigades.
     */
    protected ArrayList<Brigade> brigades = new ArrayList<>();

    /**
     * Set the budget of the tender.
     * @param budgetValue allower budget
     */
    public void setBudget(final BigDecimal budgetValue) {
        budget = budgetValue;
    }

    /**
     * Add brigade to take part in this tender.
     * @param brigade brigade to add
     */
    public void addBrigade(final Brigade brigade) {
        brigades.add(brigade);
    }

    /**
     *  Sets number of the workers with required skill.
     * @param skill reuired skill
     * @param count required count of the wokers with this skill
     * @throws IllegalArgumentException if workers count less than 1.
     */
    public void setReqiuredWorkers(final Task2.SKILLS skill,
                                   final int count) {
        if (count <= 0) {
            throw new IllegalArgumentException("Used not positive workers "
                    + "count");
        }
        requiredWorkers.put(skill, count);
    }

    /**
     * Checks accordance of the skill layout to tender requirements.
     * @param checkingWorkersProfessions List of skills to chech
     * @return true is layout is good.
     */
    protected boolean isSkillsLayoutOk(
            final ArrayList<Task2.SKILLS> checkingWorkersProfessions) {
        HashMap<Task2.SKILLS, Integer> currentProfessionsLayout =
                new HashMap<>();
        for (Task2.SKILLS profession : checkingWorkersProfessions
        ) {
            if (currentProfessionsLayout.containsKey(profession)) {
                currentProfessionsLayout.put(profession,
                        currentProfessionsLayout.get(profession) + 1);
            } else {
                currentProfessionsLayout.put(profession, 1);
            }
        }

        for (Task2.SKILLS profession : requiredWorkers.keySet()
        ) {
            if ((!currentProfessionsLayout.containsKey(profession))
                    || (currentProfessionsLayout.get(profession)
                         <  requiredWorkers.get(profession))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Recursive brute force.
     * @param workersProfessions array with set of skills for each worker
     * @param checkingWorkersProfessions array to store possible
     *                                   worker's skills layout.
     * @param itemToChange index of worker to change skill in this iteration.
     * @return true is brute force was successful.
     */
    protected boolean doRecursiveValidation(
            final ArrayList<HashSet<Task2.SKILLS>> workersProfessions,
            final ArrayList<Task2.SKILLS> checkingWorkersProfessions,
            final int itemToChange) {
        HashSet<Task2.SKILLS> workerProfessionsToChange =
              (HashSet<Task2.SKILLS>) workersProfessions.get(itemToChange);
        boolean brigateMatches = false;
        int nextIndexToChange = itemToChange + 1;
        for (Task2.SKILLS profession : workerProfessionsToChange
        ) {
            checkingWorkersProfessions.set(itemToChange, profession);
            if (nextIndexToChange < workersProfessions.size()) {
                brigateMatches = brigateMatches
                        || doRecursiveValidation(workersProfessions,
                        checkingWorkersProfessions,
                        nextIndexToChange);
                if (brigateMatches) {
                    break;
                }
            } else {
                brigateMatches =
                        isSkillsLayoutOk(checkingWorkersProfessions);
            }
            if (brigateMatches) {
                break;
            }
        }
        return brigateMatches;
    }

    /**
     * Validates the brigade.
     * @param brigade brigade to validate
     * @return true if brigade pass tender check.
     */


    protected boolean validateBrigade(final Brigade brigade) {
        // class Tender is responsible for decision, so we must implement
        // check brigades in this class.
        //  System.out.println("New brigade's salary: " + brigade.salary);
        if ((brigade.salary.compareTo(budget) > 0)
                || (brigade.getWorkersCount() <= 0)
                || (brigade.getWorkersCount() < requiredWorkersCount)) {
            return false;
        }
        // "original" workers' professions sets.
        ArrayList<HashSet<Task2.SKILLS>> workersProfessions =
                new ArrayList<>(brigade.getWorkersCount());
        // ArrayList to combine skills.
        ArrayList<Task2.SKILLS> checkingWorkersProfessions =
                new ArrayList<>(brigade.getWorkersCount());
        for (int i = 0; i < brigade.getWorkersCount(); i++) {
            checkingWorkersProfessions.add(null);
        }

        for (Worker worker : brigade
        ) {
            workersProfessions.add(worker.getProfessions());
        }
        return doRecursiveValidation(workersProfessions,
                checkingWorkersProfessions, 0);
    }

    /**
     * Search brigade which wins tender.
     * It will be first suitable brigade ordered by salary.
     * Every worker can do work for only one skill.
     * @return winnder brigade.
     */
    public Brigade holdTender() {
        // First we will sort brigades by salary to minimize brute force
        Collections.sort(brigades, (b1, b2) -> {
            if (b1.getSalary().compareTo(b2.getSalary()) > 0) {
                return 1;
            } else if (b1.getSalary() == b2.getSalary()) {
                return 0;
            } else {
                return -1;
            }
        });

        Brigade suitableBrigade = null;

        for (Brigade brigade : brigades
        ) {
            if (validateBrigade(brigade)) {
                suitableBrigade = brigade;
                break;
            }
        }
        return suitableBrigade;
    }


}
