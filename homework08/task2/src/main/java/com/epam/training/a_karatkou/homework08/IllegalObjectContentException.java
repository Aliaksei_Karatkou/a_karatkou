package com.epam.training.a_karatkou.homework08;

/**
 * IllegalObjectContentException expection class.
 */
public class IllegalObjectContentException extends RuntimeException {
    /**
     * IllegalObjectContentException exection.
     * @param message Message to describe exception
     */
    public IllegalObjectContentException(
            final String message) {
        super(message);
    }

}
