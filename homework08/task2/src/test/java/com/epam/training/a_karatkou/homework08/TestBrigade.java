package com.epam.training.a_karatkou.homework08;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestBrigade {
    @Test(expected = IllegalArgumentException.class)
    public void testBrigadeCreationWithNullName() {
        Brigade brigade = new Brigade(BigDecimal.ONE, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBrigadeCreationWithNegativeSalary() {
        Brigade brigade = new Brigade(BigDecimal.valueOf(-1), "Stakhanov&Co");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBrigadeAddNullWorker() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        brigade.addWorker(null);
    }

    @Test(expected = IllegalObjectContentException.class)
    public void testBrigadeAddWorkerWithoutAnyProfession() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker = new Worker();
        brigade.addWorker(worker);
    }

    @Test
    public void testBrigadeAddWorker() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker = new Worker();
        worker.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker);
        assertEquals("wrong workers count", 1, brigade.workers.size());
        assertEquals(worker, brigade.workers.get(0));
    }

    @Test
    public void testBrigadeGetWorkersCount() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker1);

        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker2);
        assertEquals("wrong workers count", 2, brigade.getWorkersCount());
        assertEquals(worker1, brigade.workers.get(0));
        assertEquals(worker2, brigade.workers.get(1));
    }

    @Test
    public void testBrigadeGetSalary() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        BigDecimal expectedSalary = BigDecimal.ONE;
        assertEquals(0, expectedSalary.compareTo(brigade.getSalary()));
    }

    @Test
    public void testBrigadeName() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        assertEquals("Stakhanov&Co", brigade.getName());
    }



    @Test
    public void testBrigadeHashCode() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker1);
        int hash = brigade.hashCode();
        assertEquals(-40355708, hash);
    }
    @Test
    public void testBrigadeEqualsTrue1() {
        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade1.addWorker(worker1);

        Brigade brigade2 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_3);
        brigade2.addWorker(worker2);
        assertTrue(brigade1.equals(brigade2));
    }
    @Test
    public void testBrigadeEqualsTrue2() {
        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        assertTrue(brigade1.equals(brigade1));
    }
    @Test
    public void testBrigadeEqualsFalse1() {
        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");

        assertFalse(brigade1.equals(null));
    }
    @Test
    public void testBrigadeEqualsFalse2() {
        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade1.addWorker(worker1);

        Brigade brigade2 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_3);
        worker2.addSkill(Task2.SKILLS.SKILL_4);
        brigade2.addWorker(worker2);
        assertFalse(brigade1.equals(brigade2));
    }
    @Test
    public void testBrigadeEqualsFalse3() {
        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade1.addWorker(worker1);

        Brigade brigade2 = new Brigade(BigDecimal.ONE, "Ivanov");
        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_3);
        brigade2.addWorker(worker2);
        assertFalse(brigade1.equals(brigade2));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testBrigadeIteratorRemove() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "abc");
        Iterator<Worker> iter = brigade.iterator();
        iter.remove();
    }

    @Test
    public void testBrigadeIteratorHasNext() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "abc");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_1);
        brigade.addWorker(worker1);
        Iterator<Worker> iter = brigade.iterator();
        Worker worker2 = iter.next();
        assertTrue(worker1 == worker2);
    }
    @Test(expected = NoSuchElementException.class)
    public void testBrigadeIteratorHasNextException() {
        Brigade brigade = new Brigade(BigDecimal.ONE, "abc");
        Iterator<Worker> iter = brigade.iterator();
        Worker worker2 = iter.next();
    }

}
