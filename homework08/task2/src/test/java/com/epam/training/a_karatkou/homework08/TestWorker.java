package com.epam.training.a_karatkou.homework08;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;


public class TestWorker {
    @Test
    public void testWorkerAddProfesion ()  {
        Worker worker = new Worker();
        worker.addSkill(Task2.SKILLS.SKILL_1);
        worker.addSkill(Task2.SKILLS.SKILL_3);
        assertEquals("Professions count is wrong",2,worker.skills.size());
        HashSet<Task2.SKILLS> expectedProfessions = new HashSet<>();
        expectedProfessions.add(Task2.SKILLS.SKILL_3);
        expectedProfessions.add(Task2.SKILLS.SKILL_1);
        assertTrue(worker.skills.equals(expectedProfessions) );
    }

    @Test
    public void testWorkerGetProfesions ()  {
        Worker worker = new Worker();
        worker.addSkill(Task2.SKILLS.SKILL_1);
        worker.addSkill(Task2.SKILLS.SKILL_3);
        assertEquals("Professions count is wrong",2,worker.skills.size());
        HashSet<Task2.SKILLS> expectedProfessions = new HashSet<>();
        expectedProfessions.add(Task2.SKILLS.SKILL_3);
        expectedProfessions.add(Task2.SKILLS.SKILL_1);
        HashSet<Task2.SKILLS> returnedProfessions = worker.getProfessions();
        assertTrue( expectedProfessions.equals(returnedProfessions));
    }
    @Test
    public void testWorkerHashCode () {
        Worker worker = new Worker();
        worker.addSkill(Task2.SKILLS.SKILL_1);
        int hash = worker.hashCode();
        assertEquals(2, hash);
    }

    @Test
    public void testWorkerEqualsTrue1 () {
        Worker worker1 = new Worker();
        Worker worker2 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_1);
        worker2.addSkill(Task2.SKILLS.SKILL_1);
        assertTrue(worker1.equals(worker2));
    }
    @Test
    public void testWorkerEqualsTrue2 () {
        Worker worker1 = new Worker();
        assertTrue(worker1.equals(worker1));
    }


    @Test
    public void testWorkerEqualsFalse1() {
        Worker worker1 = new Worker();
        Worker worker2 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_1);
        worker2.addSkill(Task2.SKILLS.SKILL_2);
        assertFalse(worker1.equals(worker2));
    }
    @Test
    public void testWorkerEqualsFalse2 () {
        Worker worker1 = new Worker();
        assertFalse(worker1.equals(null));
    }



}
