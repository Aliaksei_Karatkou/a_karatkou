package com.epam.training.a_karatkou.homework08;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.*;
public class TestTender {

    @Test
    public void testAddBrigade () {
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker1);
        Tender tender = new Tender();
        tender.addBrigade(brigade);
        assertEquals(1, tender.brigades.size());
        assertEquals(brigade, tender.brigades.get(0));
    }

    @Test
    public void testSetBudget () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        assertTrue(BigDecimal.TEN.compareTo(tender.budget) == 0);
    }

    @Test
    public void testSetReqiuredWorkers () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 3);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 10);
        assertEquals(2, tender.requiredWorkers.size());
        assertEquals(3, (int) tender.requiredWorkers.get(Task2.SKILLS.SKILL_1));
        assertEquals(10, (int)tender.requiredWorkers.get(Task2.SKILLS.SKILL_3));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSetReqiuredWorkersNegaive () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, -3);
    }

    @Test
    public void testIsSKILLSLayoutOkActuallyOk () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 2);
        ArrayList<Task2.SKILLS> SKILLS = new ArrayList<>();
        SKILLS.add(Task2.SKILLS.SKILL_3);
        SKILLS.add(Task2.SKILLS.SKILL_2);
        SKILLS.add(Task2.SKILLS.SKILL_1);
        SKILLS.add(Task2.SKILLS.SKILL_3);
        SKILLS.add(Task2.SKILLS.SKILL_3);
        SKILLS.add(Task2.SKILLS.SKILL_4);
        assertTrue(tender.isSkillsLayoutOk(SKILLS));
    }

    @Test
    public void testIsSKILLSLayoutOkActuallyBad () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 2);
        ArrayList<Task2.SKILLS> SKILLS = new ArrayList<>();
        SKILLS.add(Task2.SKILLS.SKILL_3);
        SKILLS.add(Task2.SKILLS.SKILL_1);
        SKILLS.add(Task2.SKILLS.SKILL_2);
        SKILLS.add(Task2.SKILLS.SKILL_2);
        SKILLS.add(Task2.SKILLS.SKILL_2);
        SKILLS.add(Task2.SKILLS.SKILL_4);
        assertFalse(tender.isSkillsLayoutOk(SKILLS));
    }

    @Test
    public void testValidateBrigadeOk () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 1);

        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker1);
        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_1);
        worker2.addSkill(Task2.SKILLS.SKILL_2);
        brigade.addWorker(worker2);
        assertTrue(tender.validateBrigade(brigade));
    }


    @Test
    public void testValidateBrigadeFail () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 1);

        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker1 = new Worker();
        worker1.addSkill(Task2.SKILLS.SKILL_3);
        brigade.addWorker(worker1);
        Worker worker2 = new Worker();
        worker2.addSkill(Task2.SKILLS.SKILL_4);
        worker2.addSkill(Task2.SKILLS.SKILL_2);
        brigade.addWorker(worker2);
        assertFalse(tender.validateBrigade(brigade));
    }
    @Test
    public void testValidateBrigadeNoWorkers () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.TEN);
        Brigade brigade = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        assertFalse(tender.validateBrigade(brigade));
    }

    @Test
    public void testHoldTender () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.ONE);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 1);

        Brigade brigade1 = new Brigade(BigDecimal.ONE, "Stakhanov&Co");
        Worker worker11 = new Worker();
        worker11.addSkill(Task2.SKILLS.SKILL_1);
        brigade1.addWorker(worker11);
        Worker worker12 = new Worker();
        worker12.addSkill(Task2.SKILLS.SKILL_4);
        worker12.addSkill(Task2.SKILLS.SKILL_3);
        brigade1.addWorker(worker12);


        Brigade brigade2 = new Brigade(BigDecimal.ONE,
                "Stakhanov's son &Co");
        Worker worker21 = new Worker();
        worker21.addSkill(Task2.SKILLS.SKILL_3);
        brigade2.addWorker(worker21);
        Worker worker22 = new Worker();
        worker22.addSkill(Task2.SKILLS.SKILL_4);
        worker22.addSkill(Task2.SKILLS.SKILL_2);
        brigade2.addWorker(worker22);

        ArrayList<Brigade> brigades = new ArrayList<>();
        tender.addBrigade(brigade1);
        tender.addBrigade(brigade2);
        assertEquals(brigade1,tender.holdTender());
    }
    @Test
    public void testHoldTenderNoWinners () {
        Tender tender = new Tender();
        tender.setBudget(BigDecimal.ONE);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_1, 1);
        tender.setReqiuredWorkers(Task2.SKILLS.SKILL_3, 1);




        Brigade brigade1 = new Brigade(BigDecimal.TEN, "Stakhanov&Co");
        Worker worker11 = new Worker();
        worker11.addSkill(Task2.SKILLS.SKILL_1);
        brigade1.addWorker(worker11);
        Worker worker12 = new Worker();
        worker12.addSkill(Task2.SKILLS.SKILL_4);
        worker12.addSkill(Task2.SKILLS.SKILL_3);
        brigade1.addWorker(worker12);


        Brigade brigade2 = new Brigade(BigDecimal.valueOf(20), "Stakhanov's son &Co");
        Worker worker21 = new Worker();
        worker21.addSkill(Task2.SKILLS.SKILL_3);
        brigade2.addWorker(worker21);
        Worker worker22 = new Worker();
        worker22.addSkill(Task2.SKILLS.SKILL_4);
        worker22.addSkill(Task2.SKILLS.SKILL_2);
        brigade2.addWorker(worker22);

        Brigade brigade3 = new Brigade(BigDecimal.ONE, "Ivanov");
        Worker worker31 = new Worker();
        worker31.addSkill(Task2.SKILLS.SKILL_1);
        brigade3.addWorker(worker31);


        ArrayList<Brigade> brigades = new ArrayList<>();
        tender.addBrigade(brigade1);
        tender.addBrigade(brigade2);
        tender.addBrigade(brigade3);
        Brigade winner = tender.holdTender();

        assertEquals(null,winner);
    }




}
