package com.epam.training.a_karatkou.homework08;


import org.junit.Test;

import static org.junit.Assert.*;


public class TestTrainingList {

    @Test(expected = IllegalArgumentException.class)
    public void testTrainingListWrongInitialCapacity() {
        new TrainingList<>(-1, 123);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTrainingListWrongMaximalCapacity() {
        new TrainingList<>(10, -4);
    }

    @Test
    public void testTrainingList() {
        TrainingList<Integer> list = new TrainingList<>(2, 32);
        assertEquals("wrong list actual capacity", 2, list.listData.length);
        assertEquals("wrong max capacity", 32, list.maxCapacity);
    }

    @Test
    public void testGrowTrainigList() {
        TrainingList<Integer> list = new TrainingList<>(2, 32);
        list.listData[0] = 123;
        list.size = 1;
        list.growTrainigList(4);
        assertEquals("wrong list actual capacity", 4, list.listData.length);
        assertEquals("previous data is lost", 123, list.listData[0]);
    }


    @Test
    public void testEnsureEnoughCapacityLessThanMinimal() {
        TrainingList<Integer> list = new TrainingList<>(0, 32);
        list.ensureEnoughCapacity(1);
        assertEquals("wrong list actual capacity",
                TrainingList.MINIMAL_CAPACITY, list.listData.length);
    }

    @Test
    public void testEnsureEnoughCapacityNormal() {
        TrainingList<Integer> list = new TrainingList<>(0, 15);
        list.ensureEnoughCapacity(1);
        assertEquals("wrong list actual capacity", 2, list.listData.length);
        list.ensureEnoughCapacity(3);
        assertEquals("wrong list actual capacity", 3, list.listData.length);
        list.ensureEnoughCapacity(4);
        assertEquals("wrong list actual capacity", 4, list.listData.length);
        list.ensureEnoughCapacity(5);
        assertEquals("wrong list actual capacity", 6, list.listData.length);
        list.ensureEnoughCapacity(6);
        assertEquals("wrong list actual capacity", 6, list.listData.length);
        list.ensureEnoughCapacity(7);
        assertEquals("wrong list actual capacity", 9, list.listData.length);
        list.ensureEnoughCapacity(10);
        assertEquals("wrong list actual capacity", 13, list.listData.length);
        list.ensureEnoughCapacity(14);
        assertEquals("wrong list actual capacity", 15, list.listData.length);
    }

    @Test
    public void testAdd() {
        TrainingList<Integer> list = new TrainingList<>(0, 2);
        list.add(5);
        list.add(6);
        assertEquals("wrong list data length", 2, list.size);
        assertEquals("wrong list actual data", 5, list.listData[0]);
        assertEquals("wrong list actual data", 6, list.listData[1]);

    }

    @Test(expected = ListOverflowException.class)
    public void testAddExceedLimit() {
        TrainingList<Integer> list = new TrainingList<>(0, 2);
        list.add(5);
        list.add(5);
        list.add(5);
    }

    @Test
    public void testIsIndexInRange() {
        TrainingList<Integer> list = new TrainingList<>(0, 2);
        assertFalse("wrong result for negative index and empty list",
                list.isIndexInRange(-1));
        assertFalse("wrong result for emply list", list.isIndexInRange(0));
        assertFalse("wrong result for empty list", list.isIndexInRange(1));
        list.size = 1;
        assertFalse("wrong result for negative index", list.isIndexInRange(-1));
        assertTrue("wrong result", list.isIndexInRange(0));
        assertFalse("wrong result for higher index", list.isIndexInRange(1));
    }

    @Test
    public void testGet() {
        TrainingList<Integer> list = new TrainingList<>(0, 2);
        list.add(5);
        assertEquals((Integer) 5, list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetWrongIndex() {
        TrainingList<Integer> list = new TrainingList<>(0, 11);
        list.add(5);
        assertEquals((Integer) 5, list.get(3));
    }

    @Test
    public void testRemoveFromMiddle() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(11);
        list.add(22);
        list.add(33);
        list.remove(1);
        assertEquals((Integer) 11, list.listData[0]);
        assertEquals((Integer) 33, list.listData[1]);
        assertEquals("List size is wrong", 2, list.size);
    }

    @Test
    public void testRemoveFromBeginning() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(11);
        list.add(22);
        list.add(33);
        list.remove(0);
        assertEquals((Integer) 22, list.listData[0]);
        assertEquals((Integer) 33, list.listData[1]);
        assertEquals("List size is wrong", 2, list.size);
    }

    @Test
    public void testRemoveFromEnd() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(11);
        list.add(22);
        list.add(33);
        list.remove(2);
        assertEquals((Integer) 11, list.listData[0]);
        assertEquals((Integer) 22, list.listData[1]);
        assertEquals("List size is wrong", 2, list.size);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByWrongIndex() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(11);
        list.add(22);
        list.add(33);
        list.remove(7);
    }

    @Test
    public void testSet() {
        TrainingList<Integer> list = new TrainingList<>(0, 2);
        list.add(5);
        list.add(6);
        list.set(0, 7);
        assertEquals("wrong list actual data", 7, list.listData[0]);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetWrongIndex() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(5);
        list.set(3, 7);
    }

    @Test
    public void testTrimToSize() {
        TrainingList<Integer> list = new TrainingList<>(10, 10);
        list.add(11);
        list.add(22);
        list.add(33);
        list.trimToSize();
        assertEquals("List capacity is wrong", 3, list.listData.length);
    }

    @Test
    public void testTrimToSizeEmptyList() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(4);
        list.remove(0);
        list.trimToSize();
        assertEquals("List capacity is wrong", 0, list.listData.length);
    }

    @Test
    public void testClear() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(11);
        list.clear();
        assertEquals(0, list.size);
    }

    @Test
    public void testIsEmptyForEmptyList() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testIsEmptyForNotEmptyList() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(4);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testHashCode() {
        TrainingList<Integer> list = new TrainingList<>(0, 10);
        list.add(4);
        assertEquals(71, list.hashCode());
    }

    @Test
    public void testEqualsTrue() {
        TrainingList<Integer> list1 = new TrainingList<>(0, 10);
        TrainingList<Integer> list2 = new TrainingList<>(0, 7);
        list1.add(4);
        list2.add(4);
        assertTrue(list1.equals(list2));
    }

    @Test
    public void testEqualsFalse1() {
        TrainingList<Integer> list1 = new TrainingList<>(0, 10);
        TrainingList<Integer> list2 = new TrainingList<>(0, 7);
        list1.add(4);
        list2.add(4);
        list2.add(0);
        assertFalse(list1.equals(list2));
    }

    @Test
    public void testEqualsFalse2() {
        TrainingList<Integer> list1 = new TrainingList<>(0, 10);
        assertFalse(list1.equals(null));
    }

    @Test
    public void testEqualsFalse3() {
        TrainingList<Integer> list1 = new TrainingList<>(0, 10);
        assertFalse(list1.equals(new Double(4.)));
    }


}

