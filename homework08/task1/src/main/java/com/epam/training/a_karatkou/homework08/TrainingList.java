package com.epam.training.a_karatkou.homework08;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class to implement homework 8, task1.
 * Implements homework, extends ArrayList and overrides some it's methods.
 *
 * @param <E> Type of the items to store.
 */

public class TrainingList<E> extends ArrayList<E> {
    /**
     * Minimal capacity of the List if it is required to increase items count.
     * Used in automatic List widening.
     */
    public static final int MINIMAL_CAPACITY = 2;

    /**
     * Current List items with actual data.
     */
    protected int size = 0;

    /**
     * Maximal items count in the List.
     */
    protected final int maxCapacity;

    /**
     * Items to store.
     */
    protected Object[] listData;

    /**
     * Magic number for hash calculation.
     */
    protected static final int HASH_INIT_MAGIC_NUMBER = 31;

    /**
     * Constructor.
     *
     * @param initialCapacity  initial capacity
     * @param capacityMaxValue maximal capacity
     * @thrown IllegalArgumentException if capacityMaxValue is less
     * than MINIMAL_CAPACITY or initialCapacity less than zero.
     */

    public TrainingList(final int initialCapacity, final int capacityMaxValue) {
        if ((capacityMaxValue < MINIMAL_CAPACITY)
                || (initialCapacity < 0)) {
            throw new IllegalArgumentException();
        }

        listData = new Object[initialCapacity];
        maxCapacity = capacityMaxValue;
        size = 0;
    }

    /**
     * Grows array for storing items.
     *
     * @param newCapacity new capacity
     */
    protected final void growTrainigList(final int newCapacity) {
        Object[] newListData = new Object[newCapacity];
        System.arraycopy(listData, 0, newListData, 0, size);
        listData = newListData;
    }


    /**
     * Ensures that TrainingList is big enough to store requiredCapacity items.
     *
     * @param requiredCapacity items count used to check.
     */
    protected final void ensureEnoughCapacity(final int requiredCapacity) {
        if (requiredCapacity <= listData.length) {
            return;
        }
        int newCapacity;
        if ((requiredCapacity < MINIMAL_CAPACITY)
                || (listData.length < MINIMAL_CAPACITY)) {
            newCapacity = MINIMAL_CAPACITY;
        } else {
            newCapacity = listData.length + (listData.length >> 1);
            if (newCapacity > maxCapacity) {
                newCapacity = maxCapacity;
            }
        }
        growTrainigList(newCapacity);
    }

    /**
     * Add new item to TrainigList.
     *
     * @param e item to add
     * @return result of the operation
     * @throws ListOverflowException if capacity reached maximal size.
     */
    @Override
    public final boolean add(final E e) {
        if (size < maxCapacity) {
            ensureEnoughCapacity(size + 1);
            listData[size++] = e;
            return true;
        } else {
            throw new ListOverflowException("Capacity has maximum value, but "
                    + "'add' method is called.");
        }
    }

    /**
     * Checks is index is in range of the inserted items' indexes.
     *
     * @param index index to check
     * @return true if index in range. false otherwise.
     */
    protected final boolean isIndexInRange(final int index) {
        return (index >= 0) && (index < size);
    }


    /**
     * retursns item to index.
     *
     * @param index index of the item
     * @return item
     * @throws IndexOutOfBoundsException if index is out of bounds.
     */
    @Override
    public final E get(final int index) {
        if (isIndexInRange(index)) {
            return (E) listData[index];
        } else {
            throw new IndexOutOfBoundsException("Index is out of allowed "
                    + "range");
        }
    }

    /**
     * remove item from list by index.
     *
     * @param index index of the item to return
     * @return removed item
     * @throws IndexOutOfBoundsException if index is out of bounds.
     */
    @Override
    public final E remove(final int index) {
        if (isIndexInRange(index)) {
            E resultValue = (E) listData[index];
            int itemsToMove = size - index - 1;
            if (itemsToMove > 0) {
                System.arraycopy(listData, index + 1, listData, index,
                        itemsToMove);
            }
            size--;
            return resultValue;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Set value of the TrainingList by index.
     *
     * @param index index of item
     * @param value new value
     * @return previous value
     * @throws IndexOutOfBoundsException if index is out of bounds.
     */
    @Override
    public final E set(final int index, final E value) {
        if (isIndexInRange(index)) {
            E resultValue = (E) listData[index];
            listData[index] = value;
            return resultValue;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Trims list's capacity to size.
     */
    @Override
    public final void trimToSize() {
        if (size < listData.length) {
            if (size == 0) {
                listData = new Object[0];
            } else {
                listData = Arrays.copyOf(listData, size);
            }
        }
    }

    /**
     * Clear list.
     */
    @Override
    public final void clear() {
        size = 0;
    }

    /**
     * checks if the list is empty.
     *
     * @return true if list if empty. false otherwise.
     */
    @Override
    public final boolean isEmpty() {
        return (size == 0);
    }

    /**
     * checks if the list equals to another list.
     *
     * @return true if list is equal. false otherwise.
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        TrainingList<E> trainingList = (TrainingList<E>) obj;
        if (size != trainingList.size) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!listData[i].equals(trainingList.listData[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * caclulates hashCode of the list.
     *
     * @return hash code.
     */
    @Override
    public final int hashCode() {
        int thisHash = HASH_INIT_MAGIC_NUMBER;
        for (int i = 0; i < size; i++) {
            thisHash += listData[i].hashCode();
        }
        thisHash = (thisHash << 1) + size;
        return thisHash;
    }
}
