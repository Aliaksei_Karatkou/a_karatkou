package com.epam.training.a_karatkou.homework08;

/**
 * Exception that is thrown if TrainigList items count is more than allowed.
 */
public class ListOverflowException extends RuntimeException {
    /**
     * Exception constructor.
     * @param exceptionMessage Description of the exception cause.
     */
    public ListOverflowException(final String exceptionMessage) {
        super(exceptionMessage);
    }
}
