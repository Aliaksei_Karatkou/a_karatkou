package com.epam.training.a_karatkou.homework11.task1;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.*;


public class TestCommandsInterpreter {

    @Test
    public void testCommandsInterpreterWrongInititalizationRootIsNull(){
        boolean thrown = false;
        try {
            Scanner scanner = new Scanner (System.in);
            CommandsInterpreter cmd = new CommandsInterpreter(null,  scanner);
            scanner.close();
        } catch (CommandLineInterpreterWrongInitializationException e){
            thrown = true;
        }
        assertTrue(thrown);

    }

    @Test
    public void testCommandsInterpreterWrongInititalizationScannerIsNull(){
        boolean thrown = false;
        try {
            CommandsInterpreter cmd = new CommandsInterpreter(new Directory(""), null);
        } catch (CommandLineInterpreterWrongInitializationException e){
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void testDoInterpreterCommandSetFileSystemTree() {
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        cmd.doInterpreterCommand("a1/b1/c.c");
        Directory dir1 = (Directory) root.items.get("a1");
        assertNotEquals(null,dir1);
        Directory dir2 = (Directory) dir1.items.get("b1");
        assertNotEquals(null,dir2);
        FileSystemItem file = dir2.items.get("c.c");
        scanner.close();
        assertNotEquals(null,file);
    }

    @Test
    public void testGetDirectoriesStructureString (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        cmd.doInterpreterCommand("a1/b1/c.c");
        String actual_result = cmd.getDirectoriesStructureString();
        String expected_result = " a1/\n"
                +"  b1/\n"
                +"   c.c\n";
        assertEquals(expected_result,actual_result);
        scanner.close();
    }
    @Test
    public void testGetDirectoriesStructureStringTwice (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        cmd.doInterpreterCommand("a1/b1/c.c");
        cmd.doInterpreterCommand("a1/b2/c.c");
        String actual_result = cmd.getDirectoriesStructureString();
        String expected_result = " a1/\n"
                +"  b1/\n"
                +"   c.c\n"
                +"  b2/\n"
                +"   c.c\n";
        scanner.close();
        assertEquals(expected_result,actual_result);
    }

    @Test
    public void testDoInterpreterCommandDoNullCommand (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String actualResult = cmd.doInterpreterCommand(null);
        scanner.close();
        assertEquals(CommandsInterpreter.STRING_INPUT_PARAMETERS_ERROR, actualResult);
    }

    @Test
    public void testDoInterpreterCommandParseError (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String actualResult = cmd.doInterpreterCommand("///");
        scanner.close();
        assertEquals(CommandsInterpreter.STRING_PARSING_ERROR, actualResult);
    }

    // ---- cutFirstWord
    @Test
    public void testCutFirstWordNormal (){
        assertEquals("def", CommandsInterpreter.cutFirstWord("abc   def  "));
    }
    @Test (expected = IllegalArgumentException.class)
    public void testCutFirstWordNull (){
        CommandsInterpreter.cutFirstWord(null);
    }

    @Test
    public void testCutFirstWordNoMoreWords (){
        assertEquals("", CommandsInterpreter.cutFirstWord("abc"));
    }

    // --------- save and load DirectoriesTree (with commands) ---------------

    // --- United test :-( ----
    @Test
    public void testSaveAndLoadTree1 (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root,scanner);
        cmd.doInterpreterCommand("a1/b1/c.c");
        cmd.doInterpreterCommand("a1/b2/c.c");
        cmd.doInterpreterCommand("save serial2.test");
        String prevTree = cmd.doInterpreterCommand("print");
        cmd.root = new Directory("");
        cmd.doInterpreterCommand("load serial2.test");
        String newTree = cmd.doInterpreterCommand("print");
        scanner.close();
        assertEquals(prevTree,newTree);
    }

    @Test
    public void testDoInterpreterCommandShowSaveUsage (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String answer = cmd.doInterpreterCommand("save");
        scanner.close();
        assertEquals(CommandsInterpreter.ERROR_MESSAGE_SHOW_SAVE_USAGE_COMMAND,answer);
    }

    @Test
    public void testDoInterpreterCommandShowLoadUsage (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String answer = cmd.doInterpreterCommand("load");
        scanner.close();
        assertEquals(CommandsInterpreter.ERROR_MESSAGE_SHOW_LOAD_USAGE_COMMAND,answer);
    }
    // ---
    @Test
    public void testSaveDirectoriesTreeIOError (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String answer = cmd. saveDirectoriesTree("/");
        scanner.close();
        assertEquals(CommandsInterpreter.ERROR_MESSAGE,answer);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSaveDirectoriesTreeNull (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String answer = cmd. saveDirectoriesTree(null);
        scanner.close();

    }

    @Test
    public void testLoadDirectoriesTreeIOError (){
        Directory root = new Directory("");
        Scanner scanner = new Scanner (System.in);
        CommandsInterpreter cmd = new CommandsInterpreter(root, scanner);
        String answer = cmd. loadDirectoriesTree("/");
        scanner.close();
        assertEquals( CommandsInterpreter.ERROR_MESSAGE,answer);
    }


}
