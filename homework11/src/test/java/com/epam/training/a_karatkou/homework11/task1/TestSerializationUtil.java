package com.epam.training.a_karatkou.homework11.task1;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestSerializationUtil {
    @Test(expected = IllegalArgumentException.class)
    public void testLoadDirectoriesTreeFilenameIsNull() throws ClassNotFoundException, IOException {
        SerializationUtil.loadDirectoriesTree(null);
    }
    @Test
    public void testLoadDirectoriesTreeNormal() throws ClassNotFoundException, IOException{
        String filename = "save.serial";
        SerializationUtil.saveDirectoriesTree(filename, new Directory(""));
        Directory loadedDirTree;
        loadedDirTree = SerializationUtil.loadDirectoriesTree(filename);
        // exact body of the loaded data is tested with the
        // testDoInterpreterSaveAndLoadTree1 method of the class
        // TestCommandsInterpreter.
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSaveDirectoriesTreeFilenameIsNull(){
        assertFalse(SerializationUtil.saveDirectoriesTree(null, new Directory("")));
    }
    @Test (expected = IllegalArgumentException.class)
    public void testSaveDirectoriesTreeDirectoryIsNull(){
        assertFalse(SerializationUtil.saveDirectoriesTree("abc", null));
    }
    @Test
    public void testSaveDirectoriesTreeWrongPath(){
        assertFalse(SerializationUtil.saveDirectoriesTree("/", new Directory("")));
    }
    @Test
    public void testSaveDirectoriesTreeNormal() throws IOException{
        String filenameToSave = "save.serial";
        boolean allOk=true;
        java.io.File file = new File(filenameToSave);
        if (file.exists()) {
            assertTrue("temporary file "+filenameToSave+" actually is not a file", file.isFile());
            assertTrue("cannot delete "+filenameToSave+" to clear the initial state", file.delete());
        }
        assertTrue (SerializationUtil.saveDirectoriesTree(filenameToSave, new Directory("")));
        assertTrue(file.exists());
    }


    @Test (expected = IllegalArgumentException.class)
    public void testPrepareDirectoriesNull() throws IOException{
        SerializationUtil.prepareDirectories(null);
    }

    @Test
    public void testPrepareDirectoriesCurrentDirectoryWithoutFile()
            throws IOException{
        assertFalse(SerializationUtil.prepareDirectories(System.getProperty("user.dir")));
    }

    @Test
    public void testPrepareDirectoriesCurrentDirectoryWithFile()
            throws IOException{
        assertTrue(SerializationUtil.prepareDirectories(System.getProperty("user.dir")+ File.separator+ "junit.temptorary_file.$$$"));
    }

    @Test
    public void testPrepareDirectoriesCurrentDirectoryAndOneSubdirectoryMore()
            throws IOException{
        String subDirName = System.getProperty("user.dir")+ File.separator+"junit.temporary_subdir";
        File tmp_dir = new File (subDirName);
        if (tmp_dir.exists() && tmp_dir.isDirectory()){
            assertTrue ("canot delete junit temporary directory", tmp_dir.delete());
        }
        assertTrue(SerializationUtil.prepareDirectories(subDirName + File.separator+  "junit.temptorary_file.$$$"));

        assertTrue ("canot delete junit temporary directory", tmp_dir.delete());

    }


}
