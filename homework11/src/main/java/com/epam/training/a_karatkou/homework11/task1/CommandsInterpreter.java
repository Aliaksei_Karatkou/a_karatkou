package com.epam.training.a_karatkou.homework11.task1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class to interact with user.
 */
public class CommandsInterpreter {
    /**
     * Command prompt string.
     */
    private static final String COMMAND_PROMPT = "$>";
    /**
     * String to inform about wrong input parameters.
     */
    protected static final String STRING_INPUT_PARAMETERS_ERROR = "Subrouting"
            + " input parameters error\n";
    /**
     * String to inform about parsing error.
     */
    protected static final String STRING_PARSING_ERROR = "Parse error\n";

    /**
     * Sets to true is user want to interrupt input.
     */
    private boolean exitCommandEntered = false;
    /**
     * root directory.
     */
    protected Directory root;
    /**
     * used scanner to interact with user.
     */
    private Scanner scanner;

    /**
     * Command to exit from this program.
     */
    private static final String COMMAND_EXIT = "exit";
    /**
     * Command to print filesystem tree.
     */
    private static final String COMMAND_PRINT_TREE = "print";


    /**
     * Command to save directories tree.
     */
    private static final String COMMAND_SAVE_TREE = "save";
    /**
     * Command to load directories tree.
     */
    private static final String COMMAND_LOAD_TREE = "load";

    /**
     * Commands delimiter.
     */
    private static final String COMMANDS_DELIMITER = " ";
    /**
     * An error messages.
     */
    public static final String ERROR_MESSAGE = "Error.\n";
    /**
     * Error message if class not found.
     */
    public static final String ERROR_MESSAGE_CLASS_NOT_FOUND =
            "Error: Class not found";

    /**
     * Short description how to load directories tree.
     * This text is shown if no filename defined, but load-command is entered.
     */
    public static final String ERROR_MESSAGE_SHOW_LOAD_USAGE_COMMAND = "Usage"
            + ": " + COMMAND_LOAD_TREE + COMMANDS_DELIMITER + "filename.ext\n";

    /**
     * Short description how to save directories tree.
     * This text is shown if no filename defined, but save-command is entered.
     */
    public static final String ERROR_MESSAGE_SHOW_SAVE_USAGE_COMMAND = "Usage"
            + ": " + COMMAND_SAVE_TREE + COMMANDS_DELIMITER + "+save filename"
            + ".ext\n";

    /**
     * Description how to use this program.
     */

    public static final String TEXT_USAGE = "Usage:\n"
            + COMMAND_EXIT + " - exit program\n"
            + COMMAND_PRINT_TREE + " - display directories tree\n"
            + COMMAND_SAVE_TREE + COMMANDS_DELIMITER + "filename.ext - save "
            + "directories tree into filename.ext\n"
            + COMMAND_LOAD_TREE + COMMANDS_DELIMITER + "filename.ext - load "
            + "directories tree from filename.ext\n"
            + "otherwise the entered text will be interpreted as a file or "
            + "as a directory path";

    /**
     * Constructor.
     *
     * @param rootDirectory root directory object
     * @param usedScanner   scanner to use
     * @throws CommandLineInterpreterWrongInitializationException if root
     *                                                            directory
     *                                                            or scanner
     *                                                            is null.
     */
    public CommandsInterpreter(final Directory rootDirectory,
                               final Scanner usedScanner) {
        if (rootDirectory == null) {
            throw new CommandLineInterpreterWrongInitializationException(
                    "rootDirectory is null");
        }
        if (usedScanner == null) {
            throw new CommandLineInterpreterWrongInitializationException(
                    "scaner is null");
        }
        root = rootDirectory;
        scanner = usedScanner;
        System.out.println(TEXT_USAGE);
    }

    /**
     * Runs filesystem tree generating.
     *
     * @return String with filesystem tree
     */
    final String getDirectoriesStructureString() {
        return root.printName("");
    }

    /**
     * Main mathod for interacting with user.
     */
    public final void listenToCommands() {
        if ((root == null) || (scanner == null)) {
            return;
        }
        do {
            System.out.print(COMMAND_PROMPT);
            String commandLine = scanner.nextLine();
            commandLine = commandLine.trim();
            if (!commandLine.isEmpty()) {
                System.out.print(doInterpreterCommand(commandLine));
            }
        } while (!exitCommandEntered);
    }


    /**
     * Cuts first word from the string and returns the result.
     * (used COMMANDS_DELIMITER as commands delimiter.
     *
     * @param string String where should be removed first word.
     * @return string whithout first word
     * @throws IllegalArgumentException if argument is illegal (null).
     */
    protected static String cutFirstWord(final String string) {
        if (string == null) {
            throw new IllegalArgumentException("String must be passed as "
                    + "argument");
        }
        int firstSpacePosition = string.indexOf(COMMANDS_DELIMITER);
        if (firstSpacePosition < 0) {
            return "";
        } else {
            String otherStringPart = string.substring(firstSpacePosition + 1);
            return otherStringPart.trim();
        }
    }

    /**
     * Calls directories tree saving and returns an error (if it happens).
     *
     * @param filename filename to save.
     * @return Either error message or empty string.
     * @throws IllegalArgumentException if filename is null.
     */
    protected final String saveDirectoriesTree(final String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("argument is null");
        }
        if (!filename.isEmpty()) {
            boolean saveResult;
            try {
                saveResult = SerializationUtil.saveDirectoriesTree(filename,
                        root);
            } catch (IllegalArgumentException ex) {
                saveResult = false;
            }
            if (!saveResult) {
                return ERROR_MESSAGE;
            }
            return "";
        } else {
            return ERROR_MESSAGE_SHOW_SAVE_USAGE_COMMAND;
        }
    }

    /**
     * Calls loading of the directories tree and returns an error (if it
     * happens).
     *
     * @param filename filename to load.
     * @return Either error message or empty string.
     */
    protected final String loadDirectoriesTree(final String filename) {
        String result = "";
        Directory newRoot = null;
        boolean errorOccurred = false;

        if (filename.isEmpty()) {
            result = ERROR_MESSAGE_SHOW_LOAD_USAGE_COMMAND;
        } else {
            newRoot = SerializationUtil.loadDirectoriesTree(filename);
            if (newRoot != null) {
                root = newRoot;
            } else {
                return (ERROR_MESSAGE);
            }
        }
        return result;
    }

    /**
     * Calls parsing of the entered path and adds the result to the memory.
     *
     * @param path The entered path
     * @return Either empty string if path is correct or error message.
     */
    protected final String processPathWithParser(final String path) {
        PathParser parser = new PathParser();
        ArrayList<String> parsedPath = new ArrayList<>();
        if (parser.parsePath(path, parsedPath)) {
            Directory currentDir = root;
            for (String newItem : parsedPath) {
                currentDir = currentDir.addItem(newItem);
            }
            return "";
        } else {
            return STRING_PARSING_ERROR;
        }
    }

    /**
     * Execut user's command.
     *
     * @param command command to execute
     * @return result of the excecution of the command
     */
    final String doInterpreterCommand(final String command) {
        if (command == null) {
            return STRING_INPUT_PARAMETERS_ERROR;
        }
        if (command.equals(COMMAND_PRINT_TREE)) {
            return getDirectoriesStructureString();
        }
        if ((command.startsWith(COMMAND_SAVE_TREE + COMMANDS_DELIMITER))
                || (command.equals(COMMAND_SAVE_TREE))) {
            return saveDirectoriesTree(cutFirstWord(command));
        }
        if ((command.startsWith(COMMAND_LOAD_TREE + COMMANDS_DELIMITER))
                || (command.equals(COMMAND_LOAD_TREE))) {
            return loadDirectoriesTree(cutFirstWord(command));
        }
        if (command.equals(COMMAND_EXIT)) {
            exitCommandEntered = true;
            return "";
        }
        return processPathWithParser(command);
    }

}
