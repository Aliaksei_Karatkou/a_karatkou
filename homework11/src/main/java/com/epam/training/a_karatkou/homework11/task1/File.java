package com.epam.training.a_karatkou.homework11.task1;

import java.io.Serializable;

/**
 * Class to represent a file.
 */
public class File implements FileSystemItem, Serializable {
    /**
     * file name.
     */
    private String name;

    /**
     * Constrictor.
     *
     * @param fileName - the name of the file to create
     */
    public File(final String fileName) {
        this.name = fileName;
    }

    /**
     * Returns string with the file name to display.
     *
     * @param spacesForFormating string to format output
     * @return string with filename.
     * Include spaces to generate formatted output.
     */
    public final String printName(final String spacesForFormating) {
        return spacesForFormating + name + "\n";
    }
}
