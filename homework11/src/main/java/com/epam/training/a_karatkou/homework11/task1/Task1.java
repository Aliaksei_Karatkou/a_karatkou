package com.epam.training.a_karatkou.homework11.task1;

import java.util.Scanner;

/**
 * Class for implementing homework 5.
 */
public final class Task1 {
    /**
     * main method.
     *
     * @param args Command line arguments
     */
    public static void main(final String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        CommandsInterpreter commandsInterpreter =
                new CommandsInterpreter(new Directory(""),
                        inputScanner);
        commandsInterpreter.listenToCommands();
        inputScanner.close();
    }

    /**
     * Constructor (hidden).
     */
    private Task1() {

    }
}

