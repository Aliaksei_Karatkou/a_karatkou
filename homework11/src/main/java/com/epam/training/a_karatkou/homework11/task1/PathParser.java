package com.epam.training.a_karatkou.homework11.task1;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class for parsing directory and file path.
 */
public class PathParser {
    /**
     * Parses path.
     *
     * @param inputString String with path to parse
     * @param parsedPath  ArrayList with parsed items
     * @return true if parsing completed successfully
     */
    public final boolean parsePath(final String inputString,
                                   final ArrayList<String> parsedPath) {
        if ((inputString == null) || (parsedPath == null)) {
            return false;
        }

        ArrayList parsedItems =
                new ArrayList<>(Arrays.asList(inputString.split("/")));
        int len = parsedItems.size();
        if (len == 0) {
            return false;
        }
        // only last item may have "." in it's name
        for (int i = 0; i < len - 1; i++) {
            String itemName = (String) parsedItems.get(i);
            if (itemName.contains(".")) {
                return false;
            }
        }
        for (int i = 0; i < len; i++) {
            String itemName = (String) parsedItems.get(i);
            if (itemName.isEmpty()) {
                return false;
            }
        }
        parsedPath.clear();
        for (int i = 0; i < parsedItems.size(); i++) {
            String parsedItem = (String) parsedItems.get(i);
            parsedPath.add(parsedItem);
        }
        return true;
    }
}
