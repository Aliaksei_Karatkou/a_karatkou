package com.epam.training.a_karatkou.homework11.task1;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Class to implement serialization of the directories tree.
 */
public final class SerializationUtil {

    /**
     * If it necessary, creates directories chunk to save the file.
     *
     * @param filename name of the file
     * @return true if directory tree exists or has been created.
     * @throws IllegalArgumentException if filename is null.
     */
    protected static boolean prepareDirectories(final String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("filename is null.");
        }
        File file = new File(filename);
        if (file.exists()) {
            // we can overwrite only files.
            return file.isFile();
        }
        String parentDirName = file.getParent();
        if (parentDirName == null) {
            return true; // current directory is always directory.
        }
        File parentDir = new File(parentDirName);
        if (parentDir.exists()) {
            return parentDir.isDirectory();
        }
        return parentDir.mkdirs();
    }

    /**
     * Loads a directories tree from file.
     *
     * @param fileNameToLoad name of the file to load
     * @return root Directory item
     * @throws IllegalArgumentException if fileNameToLoad is null.
     */
    public static Directory loadDirectoriesTree(final String fileNameToLoad) {
        if (fileNameToLoad == null) {
            throw new IllegalArgumentException("Null is passed as parameter");
        }
        Directory treeRoot = null;
        try (FileInputStream fileStream = new FileInputStream(fileNameToLoad);
             ObjectInputStream objectStream =
                     new ObjectInputStream(fileStream)) {
            treeRoot = (Directory) objectStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            treeRoot = null;
        }
        return treeRoot;
    }

    /**
     * Saves the directories tree into the file.
     *
     * @param fileNameToSave name of the file
     * @param treeRoot       The root item of the directories tree
     * @throws IllegalArgumentException of any argument is null.
     */

    public static boolean saveDirectoriesTree(final String fileNameToSave,
                                              final Directory treeRoot) {
        if ((fileNameToSave == null) || (treeRoot == null)) {
            throw new IllegalArgumentException("Null is passed as parameter");
        }
        if (!prepareDirectories(fileNameToSave)) {
            return false;
        }
        try (FileOutputStream fileStream = new FileOutputStream(fileNameToSave);
             ObjectOutputStream objectStream =
                     new ObjectOutputStream(fileStream)) {
            objectStream.writeObject(treeRoot);
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * Hidden constructor.
     */
    private SerializationUtil() {
    }
}
