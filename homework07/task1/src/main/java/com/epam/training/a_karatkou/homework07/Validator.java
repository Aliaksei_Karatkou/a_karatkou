package com.epam.training.a_karatkou.homework07;

/**
 *  Interface for executing validation method.
 * @param <T> Type of the validation values
 */
interface Validator<T> {
    /**
     * Method to execute for validation values.
     * @param value Validation value
     * @throws ValidationFailedException throws in case of validation error.
     */
    void validate(T value) throws ValidationFailedException;
}
