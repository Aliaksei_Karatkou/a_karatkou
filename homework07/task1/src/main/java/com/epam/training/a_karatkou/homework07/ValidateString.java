package com.epam.training.a_karatkou.homework07;

/**
 * Class to implement a String validator.
 */
class ValidateString implements Validator<String> {
    /**
     * Regular expression to check if the first letter is capital.
     * It supports not only english letters.
     */
    private static final String
            REGEXP_STARTS_WITH_CAPITAL_LETTER = "^\\p{Lu}.*";

    /**
     * Implements String value validation.
     *
     * @param value value to validate
     * @throws ValidationFailedException if validation failed
     */
    public void validate(final String value) throws ValidationFailedException {
        if (!value.matches(REGEXP_STARTS_WITH_CAPITAL_LETTER)) {
            throw new ValidationFailedException("First letter isn't capital");
        }
    }
}
