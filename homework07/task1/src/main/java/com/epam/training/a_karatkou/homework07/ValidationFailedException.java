package com.epam.training.a_karatkou.homework07;

/**
 * Class of the validation fail exception.
 */
public class ValidationFailedException extends Exception {
    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message
     */
    public ValidationFailedException(
            final String message) {
        super(message);
    }
}
