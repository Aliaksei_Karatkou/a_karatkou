package com.epam.training.a_karatkou.homework07;

/**
 * Class to imlement an Integer value validator.
 */
class ValidateInteger implements Validator<Integer> {
    /**
     * Mininal allowed value.
     */
    private static final Integer MIN_ALLOWED_VALUE = 1;
    /**
     * Maximum allowed value.
     */
    private static final Integer MAX_ALLOWED_VALUE = 10;

    /**
     * Implements an integer value validation.
     *
     * @param value value to validate
     * @throws ValidationFailedException if validation failed
     */
    public void validate(final Integer value) throws ValidationFailedException {
        if ((value.compareTo(MIN_ALLOWED_VALUE) < 0)
                || (value.compareTo(MAX_ALLOWED_VALUE) > 0)) {
            throw new ValidationFailedException("out of range ["
                    + MIN_ALLOWED_VALUE + ".." + MAX_ALLOWED_VALUE + "]");
        }
    }
}
