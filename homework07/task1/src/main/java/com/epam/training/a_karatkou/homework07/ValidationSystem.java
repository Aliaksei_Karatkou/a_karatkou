package com.epam.training.a_karatkou.homework07;

import java.util.HashMap;

/**
 * Class to call validation methods.
 */
public final class ValidationSystem {
    /**
     * Stores accordance between object's class to validate and validator.
     * <p>
     * Key - (full) class name of the object to validate.
     * To be able to handle different classes (not only default wrappers of
     * primitive values) the key includes full class name.
     * <p>
     * Value - validator class
     */
    private static HashMap<String, Validator> validators = new HashMap<>();


    /**
     * Static initialization to create and register minimal set of validators.
     */
    static {
        registerValidator(new ValidateInteger(), Integer.class.getName());
        registerValidator(new ValidateString(), String.class.getName());
    }

    /**
     * Hidden constuctor.
     */
    private ValidationSystem() {
    }

    /**
     * Registers validator.
     * Registration for a validator can be changed by re-registration.
     *
     * @param newValidator        validator object.
     * @param classNameToValidate class name to validate by this validator.
     */
    public static void registerValidator(final Validator newValidator,
                                         final String classNameToValidate) {
        if (newValidator == null) {
            return;
        }
        if ((classNameToValidate == null) || (classNameToValidate.isEmpty())) {
            return;
        }
        validators.put(classNameToValidate, newValidator);
    }

    /**
     * Validates object's value.
     *
     * @param obj Object to validate.
     * @throws ValidationFailedException throws in case of eny validation error.
     */
    public static void validate(final Object obj) throws
            ValidationFailedException {
        if (obj == null) {
            throw new ValidationFailedException("Object is null");
        }
        String classNameToValidate = obj.getClass().getName();
        if (validators.containsKey(classNameToValidate)) {
            Validator validator = validators.get(classNameToValidate);
            validator.validate(obj);
        } else {
            throw new ValidationFailedException("Validation class is not "
                    + "registered (" + classNameToValidate + ").");
        }
    }

}
