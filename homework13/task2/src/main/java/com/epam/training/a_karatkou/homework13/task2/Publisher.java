package com.epam.training.a_karatkou.homework13.task2;

interface Publisher {

    Publication getPublication(int publicationId, String baseURL);

    Publication writePublication(Publication publication, String baseURL);
}
