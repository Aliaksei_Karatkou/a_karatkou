package com.epam.training.a_karatkou.homework13.task2;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import java.io.IOException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.CloseableHttpResponse;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;


public class WebRackHttpClient implements Publisher {

    public final Publication getPublication(final int publicationId,
                                            final String baseURL) {

        Publication returnedPublication = null;

        if (baseURL == null) {
            throw new IllegalArgumentException("baseURL is null");
        }

        try (
                CloseableHttpClient client =
                        HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(baseURL + "/" + publicationId);
            request.addHeader(HttpHeaders.ACCEPT, "application/json");
            request.addHeader(HttpHeaders.ACCEPT_ENCODING, "utf-8");
            CloseableHttpResponse httpResponse = client.execute(request);
            if (httpResponse.getStatusLine().getStatusCode() != HTTP_OK) {
                return null;
            }
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                String responseLine =
                        EntityUtils.toString(httpResponse.getEntity());
                ObjectMapper objectMapper = new ObjectMapper();
                returnedPublication =
                        objectMapper.readValue(responseLine,
                                Publication.class);
            }
            httpResponse.close();

        } catch (IOException ex) {
            return null;
        }
        return returnedPublication;
    }

    public final Publication writePublication(final Publication publication,
                                              final String baseURL) {
        Publication returnedPublication = null;
        if ((publication == null) || (baseURL == null)) {
            throw new IllegalArgumentException("argument is null");
        }
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            HttpPost request = new HttpPost(baseURL);
            request.addHeader(HttpHeaders.ACCEPT, "application/json");
            request.addHeader(HttpHeaders.ACCEPT_ENCODING, "utf-8");
            request.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            ObjectMapper objectMapperOut = new ObjectMapper();
            String jsonOut = objectMapperOut.writeValueAsString(publication);
            StringEntity entity = new StringEntity(jsonOut);
            request.setEntity(entity);
            CloseableHttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() != HTTP_CREATED) {
                return null;
            }

            String responseLine = EntityUtils.toString(response.getEntity());
            ObjectMapper objectMapper = new ObjectMapper();
            returnedPublication =
                    objectMapper.readValue(responseLine,
                            Publication.class);
        } catch (IOException ex) {
            return null;
        }
        return returnedPublication;
    }
}
