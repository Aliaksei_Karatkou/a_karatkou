package com.epam.training.a_karatkou.homework13.task2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestValidationResult {
    @Test
    public void TestGetMethod() {
        ValidationResult result = new ValidationResult();
        result.method = "abc";
        assertEquals("abc", result.getMethod());
    }

    @Test
    public void TestSetMethod() {
        ValidationResult result = new ValidationResult();
        result.setMethod("method");
        assertEquals("method", result.method);
    }

    @Test
    public void TestGetArticleId() {
        ValidationResult result = new ValidationResult();
        result.articleId = 45;
        assertEquals(45, result.getArticleId());
    }


    @Test
    public void TestSetArticleId() {
        ValidationResult result = new ValidationResult();
        result.setArticleId(100);
        assertEquals(100, result.articleId);
    }

    @Test
    public void TestIsValidateOk() {
        ValidationResult result = new ValidationResult();
        result.validateOk = true;
        assertEquals(true, result.isValidateOk());
    }

    @Test
    public void TestSetValidateOk() {
        ValidationResult result = new ValidationResult();
        result.setValidateOk();
        assertEquals(true, result.validateOk);
    }

    @Test
    public void TestGetErrorMessage() {
        ValidationResult result = new ValidationResult();
        result.errorMessage = "err msg";
        assertEquals("err msg", result.getErrorMessage());
    }

    @Test
    public void TestSetErrorMessage() {
        ValidationResult result = new ValidationResult();
        result.setErrorMessage("msg");
        assertEquals("msg", result.getErrorMessage());
    }
}
