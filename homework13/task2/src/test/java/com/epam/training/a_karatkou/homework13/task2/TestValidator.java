package com.epam.training.a_karatkou.homework13.task2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestValidator {
    @Test(expected = IllegalArgumentException.class)
    public void testValidateParamsNull() {
        Validator.validateParams(null);
    }

    @Test
    public void testValidateParamsSmallArray() {
        ValidationResult result = Validator.validateParams(new String[1]);
        assertFalse(result.isValidateOk());
        assertEquals(Validator.ERROR_TEXT_WRONG_PARAMS_COUNT,
                result.getErrorMessage());
    }

    @Test
    public void testValidateParamsBigArray() {
        ValidationResult result = Validator.validateParams(new String[3]);
        assertFalse(result.isValidateOk());
        assertEquals(Validator.ERROR_TEXT_WRONG_PARAMS_COUNT,
                result.getErrorMessage());
    }

    @Test
    public void testValidateParamsWrongMethod() {
        ValidationResult result = Validator.validateParams(new String[]{"G",
                "4"});
        assertFalse(result.isValidateOk());
        assertEquals(Validator.ERROR_TEXT_WRONG_METHOD,
                result.getErrorMessage());
    }

    @Test
    public void testValidateParamsErrorInArticleNumber() {
        ValidationResult result = Validator.validateParams(new String[]{"Get"
                , "f"});
        assertFalse(result.isValidateOk());
        assertEquals(Validator.ERROR_TEXT_ARTICLE_NUMBER_ERROR,
                result.getErrorMessage());
    }

    @Test
    public void testValidateParamsErrorArticleNumberIsNotPositive() {
        ValidationResult result = Validator.validateParams(new String[]{"Get"
                , "0"});
        assertFalse(result.isValidateOk());
        assertEquals(Validator.ERROR_TEXT_ARTICLE_NUMBER_ERROR,
                result.getErrorMessage());
    }

    @Test
    public void testValidateParamsGetOk() {
        ValidationResult result = Validator.validateParams(new String[]{"Get"
                , "4"});
        assertTrue(result.isValidateOk());
        assertEquals(result.getMethod(), "get");
        assertEquals(result.getArticleId(), 4);
    }

    @Test
    public void testValidateParamsPostOk() {
        ValidationResult result = Validator.validateParams(new String[]{"Post"
                , "4"});
        assertTrue(result.isValidateOk());
        assertEquals(result.getMethod(), "post");
        assertEquals(result.getArticleId(), 4);
    }

}
