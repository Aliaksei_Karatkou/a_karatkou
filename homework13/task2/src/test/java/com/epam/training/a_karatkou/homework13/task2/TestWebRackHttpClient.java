package com.epam.training.a_karatkou.homework13.task2;


import org.junit.Test;

import static com.epam.training.a_karatkou.homework13.task2.Librarian.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestWebRackHttpClient {


    @Test(expected = IllegalArgumentException.class)
    public void testGetPublication_NullAsUrl() {
        new WebRackHttpClient().getPublication(1, null);
    }

    @Test
    public void testGetPublication_WrongProtecol() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        assertTrue(null == webRack.getPublication(1, "gggf://sdfsdgsgsdf" +
                ".fdfgdf"));
    }

    @Test
    public void testGetPublication_WrongUrl() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        assertTrue(null == webRack.getPublication(1, "http://sdfsdgsgsdf" +
                ".fdfgdf"));
    }

    @Test
    public void testGetPublication_WrongPublication() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        assertTrue(null == webRack.getPublication(-1, Librarian.BASE_URL));
    }

    @Test
    public void testGetPublication_Normal() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        Publication publication = webRack.getPublication(99, Librarian.BASE_URL);
        System.out.println(publication);
        String expectedResult = "Article [99]: User [10] Title [\"temporibus " +
                "sit alias delectus eligendi possimus magni\"] Message [\"quo" +
                " deleniti praesentium dicta non quod\n" +
                "aut est molestias\n" +
                "molestias et officia quis nihil\n" +
                "itaque dolorem quia\"]";
        assertEquals(expectedResult, publication.toString());
    }

    // ---------------
    @Test(expected = IllegalArgumentException.class)
    public void testWritePublication_NullAsParam1() {
        new WebRackHttpClient().writePublication(null, "as");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWritePublication_NullAsParam2() {
        new WebRackHttpClient().writePublication(new Publication(), null);
    }

    @Test
    public void testWritePublication_WrongProtecol() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        assertTrue(null == webRack.writePublication(new Publication(), "gggf" +
                "://sdfsdgsgsdf.fdfgdf"));
    }

    @Test
    public void testWritePublication_WrongUrl() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        assertTrue(null == webRack.writePublication(new Publication(), "http" +
                "://sdfsdgsgsdf.fdfgdf"));
    }

    @Test
    public void testWritePublication_Normal() {
        WebRackHttpClient webRack = new WebRackHttpClient();
        Publication publication = new Publication();
        publication.id = -1;
        publication.setBody(DEFAULT_PUBLICATION_BODY);
        publication.setId(444);
        publication.setUserId(DEFAULT_PUBLICATION_USER_ID);
        publication.setTitle(DEFAULT_PUBLICATION_TITLE);
        Publication newPublication = webRack.writePublication(publication,
                Librarian.BASE_URL);
        String expectedStringedResult =
                "Article [" + DEFAULT_STUB_SITE_PUBLICATION_NEW_ARTICLE_ID +
                        "]:"
                + " User [" + DEFAULT_PUBLICATION_USER_ID + "]"
                + " Title [\"" + DEFAULT_PUBLICATION_TITLE + "\"]"
                + " Message [\"" + DEFAULT_PUBLICATION_BODY + "\"]";
        System.out.println(newPublication.toString());
        assertEquals(expectedStringedResult, newPublication.toString());
    }

}
