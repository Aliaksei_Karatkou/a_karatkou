package com.epam.training.a_karatkou.homework13.task1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPublication {
    @Test
    public void testGetTitle() {
        Publication publication = new Publication();
        publication.title = "abc_title";
        assertEquals("abc_title", publication.getTitle());
    }

    @Test
    public void testSetTitle() {
        Publication publication = new Publication();
        publication.setTitle("abc_title");
        assertEquals("abc_title", publication.title);
    }

    @Test
    public void testGetBody() {
        Publication publication = new Publication();
        publication.body = "abc_body";
        assertEquals("abc_body", publication.getBody());
    }

    @Test
    public void testSetBody() {
        Publication publication = new Publication();
        publication.setBody("abc_body");
        assertEquals("abc_body", publication.body);
    }

    @Test
    public void testGetUserId() {
        Publication publication = new Publication();
        publication.userId = 5;
        assertEquals(5, publication.getUserId());
    }

    @Test
    public void testSetUserId() {
        Publication publication = new Publication();
        publication.setUserId(5);
        assertEquals(5, publication.userId);
    }

    @Test
    public void testGetId() {
        Publication publication = new Publication();
        publication.id = 50;
        assertEquals(50, publication.getId());
    }

    @Test
    public void testSetId() {
        Publication publication = new Publication();
        publication.setId(50);
        assertEquals(50, publication.id);
    }

    @Test
    public void testToString() {
        Publication publication = new Publication();
        publication.userId = 1;
        publication.id = 2;
        publication.title = "abc";
        publication.body = "def";
        String expected = "Article [2]: User [1] Title [\"abc\"] Message " +
                "[\"def\"]";
        assertEquals(expected, publication.toString());
    }
}
