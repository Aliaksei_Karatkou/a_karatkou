package com.epam.training.a_karatkou.homework13.task1;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;

public class WebRack implements Publisher {

    public final Publication getPublication(final int publicationId,
                                            final String baseURL) {
        try {
            if (baseURL == null) {
                throw new IllegalArgumentException("baseURL is null");
            }
            URL url = new URL(baseURL + "/" + publicationId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            if (con.getResponseCode() != HTTP_OK) {
                return null;
            }
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                ObjectMapper objectMapper = new ObjectMapper();
                return objectMapper.readValue(response.toString(),
                        Publication.class);
            }
        } catch (IOException ex) {
            return null;
        }
    }

    public final Publication writePublication(final Publication publication,
                                              final String baseURL) {
        Publication returnedPublication = null;
        if ((publication == null) || (baseURL == null)) {
            throw new IllegalArgumentException("argument is null");
        }
        try {
            URL url = new URL(baseURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            try (OutputStream os = con.getOutputStream()) {
                ObjectMapper objectMapperOut = new ObjectMapper();
                objectMapperOut.writeValue(os, publication);
            }

            if (con.getResponseCode() != HTTP_CREATED) {
                return null;
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                ObjectMapper objectMapper = new ObjectMapper();
                returnedPublication =
                        objectMapper.readValue(response.toString(),
                                Publication.class);
            }
        } catch (IOException e) {
            return null;
        }
        return returnedPublication;
    }
}
