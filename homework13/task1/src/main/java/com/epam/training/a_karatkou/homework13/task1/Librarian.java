package com.epam.training.a_karatkou.homework13.task1;

public final class Librarian {
    public static final String BASE_URL = "http://jsonplaceholder.typicode"
            + ".com/posts";
    public static final String DEFAULT_PUBLICATION_TITLE = "My new title";
    public static final int DEFAULT_PUBLICATION_USER_ID = 5;

    public static final String DEFAULT_PUBLICATION_BODY = "The text of the "
            + "new article";
    public static final int DEFAULT_STUB_SITE_PUBLICATION_NEW_ARTICLE_ID = 101;

    /**
     * hidden constructor.
     */
    private Librarian() {

    }

    public static void main(final String[] args) {

        ValidationResult parameters = Validator.validateParams(args);
        Publisher publicationsRack = new WebRack();
        Publication publication = null;
        if (!parameters.isValidateOk()) {
            System.out.println(parameters.getErrorMessage());
            return;
        }

        if (parameters.method.equals("get")) {
            publication =
                    publicationsRack.getPublication(parameters.getArticleId(),
                            BASE_URL);
        } else {
            Publication newPublication = new Publication();
            newPublication.setId(parameters.getArticleId());
            newPublication.setTitle(DEFAULT_PUBLICATION_TITLE);
            newPublication.setUserId(DEFAULT_PUBLICATION_USER_ID);
            newPublication.setBody(DEFAULT_PUBLICATION_BODY);
            publication = publicationsRack.writePublication(newPublication,
                    BASE_URL);
        }
        if (publication != null) {
            System.out.println(publication.toString());
        } else {
            System.out.println("error");
        }
    }

}
