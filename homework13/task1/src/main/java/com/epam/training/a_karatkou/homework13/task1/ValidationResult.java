package com.epam.training.a_karatkou.homework13.task1;

public class ValidationResult {
    protected String method = null;
    protected int articleId = -1;
    protected boolean validateOk = false;
    protected String errorMessage = null;

    public final String getMethod() {
        return method;
    }

    public final void setMethod(final String chosenMethod) {
        this.method = chosenMethod;
    }

    public final int getArticleId() {
        return articleId;
    }

    public final void setArticleId(final int arcicleId) {
        this.articleId = arcicleId;
    }

    public final boolean isValidateOk() {
        return validateOk;
    }

    public final void setValidateOk() {
        this.validateOk = true;
    }

    public final String getErrorMessage() {
        return errorMessage;
    }

    public final void setErrorMessage(final String newErrorMessage) {
        errorMessage = newErrorMessage;
    }
}

