package com.epam.training.a_karatkou.homework13.task1;

public class Validator {
    public static final int ARGS_POSITION_METHOD = 0;
    public static final int ARGS_POSITION_ARTICLE_ID = 1;
    public static final int ARGS_COUNT_REQUIRED = 2;

    public static final String ERROR_TEXT_WRONG_PARAMS_COUNT = "Wrong "
            + "parameters count";
    public static final String ERROR_TEXT_WRONG_METHOD = "Wrong method";
    public static final String ERROR_TEXT_ARTICLE_NUMBER_ERROR = "Article "
            + "number error";

    private Validator() {

    }

    protected static ValidationResult validateParams(final String[] args) {
        ValidationResult result = new ValidationResult();
        if (args == null) {
            throw new IllegalArgumentException("parameter is null");
        }
        if (args.length != ARGS_COUNT_REQUIRED) {
            result.setErrorMessage(ERROR_TEXT_WRONG_PARAMS_COUNT);
            return result;
        }
        String method = args[ARGS_POSITION_METHOD].toLowerCase();
        if ((!method.equals("get") && !method.equals("post"))) {
            result.setErrorMessage(ERROR_TEXT_WRONG_METHOD);
            return result;
        }
        result.setMethod(method);
        int articleNumber = 0;
        try {
            articleNumber = Integer.valueOf(args[ARGS_POSITION_ARTICLE_ID]);
        } catch (NumberFormatException ex) {
            result.setErrorMessage(ERROR_TEXT_ARTICLE_NUMBER_ERROR);
            return result;
        }
        if (articleNumber < 1) {
            result.setErrorMessage(ERROR_TEXT_ARTICLE_NUMBER_ERROR);
            return result;
        }
        result.setArticleId(articleNumber);
        result.setValidateOk();
        return result;
    }

}
