package com.epam.training.a_karatkou.homework13.task1;

public class Publication {
    protected int id;
    protected int userId;
    protected String title;
    protected String body;

    public final String getTitle() {
        return title;
    }

    public final void setTitle(final String newTitle) {
        this.title = newTitle;
    }

    public final String getBody() {
        return body;
    }

    public final void setBody(final String newBody) {
        this.body = newBody;
    }


    public final int getUserId() {
        return userId;
    }

    public final void setUserId(final int newUserId) {
        this.userId = newUserId;
    }

    public final int getId() {
        return id;
    }

    public final void setId(final int newId) {
        this.id = newId;
    }

    @Override
    public final String toString() {
        return "Article [" + id + "]: User [" + userId + "] Title [\"" + title
                + "\"] Message [\"" + body + "\"]";
    }

}
