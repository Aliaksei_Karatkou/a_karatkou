package com.epam.training.a_karatkou.homework13.task1;

interface Publisher {

    Publication getPublication(int publicationId, String baseURL);

    Publication writePublication(Publication publication, String baseURL);
}
