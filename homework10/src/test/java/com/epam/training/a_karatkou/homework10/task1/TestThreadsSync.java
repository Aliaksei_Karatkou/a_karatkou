package com.epam.training.a_karatkou.homework10.task1;

import org.junit.Test;

import java.math.BigDecimal;

import static com.epam.training.a_karatkou.homework10.task1.ThreadsSync.BALLANCE_IN_THE_RANGE_MESSAGE;
import static com.epam.training.a_karatkou.homework10.task1.ThreadsSync.BALLANCE_OUT_OF_THE_RANGE_MESSAGE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestThreadsSync {
    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyCardIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( null, BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "str");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyMoneyIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),null, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "str");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyMoneyIsZero() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal.ZERO, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "str");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyMoneyIsNegative() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal. valueOf(-1), this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "str");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyWorkerIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal.TEN, null,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "str");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyMoneyProducerIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal.TEN, this,
               null,
                MoneyConsumer.class.getName(),
                "str");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyMoneyConsumerIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                null,
                "str");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testChangeMoneyTaskNameIsNull() {
        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.changeMoney( new Card("we"),BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                null);
    }


    @Test
    public void testChangeMoneyBalanceOutOfRangeLess() {
        ThreadsSync threadsSync = new ThreadsSync();
        String result = threadsSync.changeMoney( new Card("we",BigDecimal.ZERO),BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
        assertTrue(result.compareTo (BALLANCE_OUT_OF_THE_RANGE_MESSAGE) == 0);
    }

    @Test
    public void testChangeMoneyBalanceOutOfRangeMore() {
        ThreadsSync threadsSync = new ThreadsSync();
        String result = threadsSync.changeMoney( new Card("we",BigDecimal.valueOf(10000)),BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
        assertTrue(result.compareTo (BALLANCE_OUT_OF_THE_RANGE_MESSAGE) == 0);
    }
    @Test
    public void testChangeMoneyBalanceOk() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.TEN);
        MoneyProducer atm = new MoneyProducer(BigDecimal.ONE,"trId",threadsSync,card);
        String result = threadsSync.changeMoney( card,BigDecimal.TEN, atm,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
        assertTrue(result.compareTo (BALLANCE_IN_THE_RANGE_MESSAGE) == 0);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testChangeMoneyWrongWorker() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.TEN);
        MoneyProducer atm = new MoneyProducer(BigDecimal.ONE,"trId",threadsSync,card);
        String result = threadsSync.changeMoney( card,BigDecimal.TEN, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
    }


    @Test (expected = IllegalArgumentException.class)
    public void testIsBalanceInRangeNull() {

        ThreadsSync threadsSync = new ThreadsSync();
        threadsSync.isBalanceInRange(null);
    }
     @Test
    public void testIsBalanceInRangeNotEnought() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.ZERO);
        assertFalse (threadsSync.isBalanceInRange(card));
    }
    @Test
    public void testIsBalanceInRangeTooMuch() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.valueOf(99999));
        assertFalse (threadsSync.isBalanceInRange(card));
    }
    @Test
    public void testIsBalanceNorm() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.valueOf(99));
        assertTrue (threadsSync.isBalanceInRange(card));
    }

    @Test
    public void testChangeMoneyBalanceOverlimit() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.valueOf(1000));
        MoneyProducer atm = new MoneyProducer(BigDecimal.TEN,"trId",threadsSync,card);
        String result = threadsSync.changeMoney( card,BigDecimal.TEN, atm,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
        assertTrue(result.compareTo (BALLANCE_OUT_OF_THE_RANGE_MESSAGE) == 0);
    }
    @Test
    public void testChangeMoneyBalanceUnderlimit() {
        ThreadsSync threadsSync = new ThreadsSync();
        CreditCard card = new CreditCard("e",BigDecimal.valueOf(1));
        MoneyConsumer atm = new MoneyConsumer(BigDecimal.TEN,"trId",threadsSync,card);
        String result = threadsSync.changeMoney( card,BigDecimal.TEN, atm,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                "trd");
        assertTrue(result.compareTo (BALLANCE_OUT_OF_THE_RANGE_MESSAGE) == 0);
    }

}
