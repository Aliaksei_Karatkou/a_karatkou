package com.epam.training.a_karatkou.homework10.task1;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.epam.training.a_karatkou.homework10.task1.ThreadsSync.MONEY_MAX_VALUE;
import static com.epam.training.a_karatkou.homework10.task1.ThreadsSync.MONEY_MIN_VALUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static com.epam.training.a_karatkou.homework10.task1.Juggler.*;

public class TestJuggler {

    @Test
    public void TestCommonMoreThanMax() {

        CreditCard card = new CreditCard("public", INITIAL_BALANCE);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(
                ATM_TOTAL_ITEMS_COUNT);
        ThreadsSync threadSync = new ThreadsSync();
        Atm[] atms = new Atm[ATM_TOTAL_ITEMS_COUNT];


        for (int i = 0; i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            atms[i] =
                    new MoneyProducer(BigDecimal.valueOf(
                            ATM_PRODUCER_MONEY_CHANGE), "Prod[" + i + "]",
                            threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            atms[i] =
                    new MoneyConsumer(BigDecimal.valueOf(
                            ATM_CONSUMER_MONEY_CHANGE),
                            "Cons[" + i + "]", threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = 0;
                i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            executor.scheduleAtFixedRate((MoneyProducer) atms[i],
                    0, ATM_PRODUCER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            executor.scheduleAtFixedRate((MoneyConsumer) atms[i],
                    0, ATM_CONSUMER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }

        while (threadSync.isBalanceInRange(card)) {
            try {
                Thread.sleep(CHECK_RESULT_PERIOD_MILISECS);
            } catch (InterruptedException ex) {
                break;
            }
        }
        executor.shutdown();
        assertTrue(card.balance.compareTo(MONEY_MAX_VALUE) >0 );
    }

    @Test
    public void TestCommonLessThanMin() {

        CreditCard card = new CreditCard("public", INITIAL_BALANCE);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(
                ATM_TOTAL_ITEMS_COUNT);
        ThreadsSync threadSync = new ThreadsSync();
        Atm[] atms = new Atm[ATM_TOTAL_ITEMS_COUNT];


        for (int i = 0; i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            atms[i] =
                    new MoneyProducer(BigDecimal.valueOf(
                            ATM_PRODUCER_MONEY_CHANGE), "Prod[" + i + "]",
                            threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            atms[i] =
                    new MoneyConsumer(BigDecimal.valueOf(
                            ATM_CONSUMER_MONEY_CHANGE*3),
                            "Cons[" + i + "]", threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = 0;
                i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            executor.scheduleAtFixedRate((MoneyProducer) atms[i],
                    0, ATM_PRODUCER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            executor.scheduleAtFixedRate((MoneyConsumer) atms[i],
                    0, ATM_CONSUMER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }

        while (threadSync.isBalanceInRange(card)) {
            try {
                Thread.sleep(CHECK_RESULT_PERIOD_MILISECS);
            } catch (InterruptedException ex) {
                break;
            }
        }
        executor.shutdown();
        assertTrue(card.balance.compareTo(MONEY_MIN_VALUE) <=0 );
    }
}
