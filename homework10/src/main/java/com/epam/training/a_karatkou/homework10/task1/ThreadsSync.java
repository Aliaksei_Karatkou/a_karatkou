package com.epam.training.a_karatkou.homework10.task1;

import java.math.BigDecimal;

/**
 * Class used to synchronized change money.
 */
public class ThreadsSync {
    /**
     * Max allowed money for running threads.
     */
    public static final BigDecimal MONEY_MAX_VALUE = BigDecimal.valueOf(1000);
    /**
     * Min allowed money for running thhreads.
     */
    public static final BigDecimal MONEY_MIN_VALUE = BigDecimal.ZERO;
    /**
     * Message if balance out of the range.
     */
    public static final String BALLANCE_OUT_OF_THE_RANGE_MESSAGE =
            "Balance out of the range";
    /**
     * Message if balance in the range.
     */
    public static final String BALLANCE_IN_THE_RANGE_MESSAGE = "";

    /**
     * Parameters validation fail exception description.
     */
    public static final String PARAMETERS_VALIDATION_FAILED =
            "Parameters validation failed";

    /**
     * Invalid worker message.
     */
    public static final String INVALID_WORKER_MESSAGE = "Invelid worker";

    /**
     * Checks if balance is in the allowed range.
     *
     * @param card card to process.
     * @return true if balance is in the allowed range
     */
    protected synchronized boolean isBalanceInRange(final Card card) {
        if (card == null) {
            throw new IllegalArgumentException(PARAMETERS_VALIDATION_FAILED);
        }
        return (!((card.getBalance().signum() <= 0)
                || (card.getBalance().compareTo(MONEY_MAX_VALUE) > 0)));
    }

    /**
     * Changes money.
     *
     * @param usingCard         card to user (must be able to set negative
     *                          balance)
     * @param moneyDiff         money's diff ( increase of decrease)
     * @param worker            ATM class to use
     * @param producerClassName MoneyProducer's class name
     * @param consumerClassName MoneyConsumer's class name
     * @param taskName          task name for debug (to write to the system
     *                          .out stream).
     * @return BALLANCE_OUT_OF_THE_RANGE_MESSAGE if balace is out of the
     * allowed range, or  BALLANCE_IN_THE_RANGE_MESSAGE otherwise.
     */
    public synchronized String changeMoney(final Card usingCard,
                                           final BigDecimal moneyDiff,
                                           final Object worker,
                                           final String producerClassName,
                                           final String consumerClassName,
                                           final String taskName) {
        if ((usingCard == null)
                || (moneyDiff == null)
                || (worker == null)
                || (producerClassName == null)
                || (consumerClassName == null)
                || (taskName == null)
                || (moneyDiff.signum() <= 0)) {
            throw new IllegalArgumentException(PARAMETERS_VALIDATION_FAILED);
        }


        if (isBalanceInRange(usingCard)) {
            BigDecimal currentBalance = usingCard.getBalance();

            BigDecimal expectedBalace;
            if (worker.getClass().getName().compareTo(producerClassName) == 0) {
                expectedBalace = currentBalance.add(moneyDiff);
                usingCard.addToBalance(moneyDiff);
            } else if (worker.getClass().getName()
                    .compareTo(consumerClassName) == 0) {
                expectedBalace = currentBalance.subtract(moneyDiff);
                usingCard.withdrowFromBalance(moneyDiff);
            } else {
                throw new IllegalArgumentException(INVALID_WORKER_MESSAGE);
            }
            String logStr = taskName + " " + currentBalance + " -> ";

            BigDecimal resultBalance = usingCard.getBalance();
            logStr += resultBalance;
            if (resultBalance.compareTo(expectedBalace) != 0) {
                logStr += " (expected: " + expectedBalace + ")";
            }
            System.out.println(logStr);
            if (!isBalanceInRange(usingCard)) {
                System.out.println(taskName + ":"
                        + BALLANCE_OUT_OF_THE_RANGE_MESSAGE);
                return BALLANCE_OUT_OF_THE_RANGE_MESSAGE;
            } else {
                return BALLANCE_IN_THE_RANGE_MESSAGE;
            }
        }
        return BALLANCE_OUT_OF_THE_RANGE_MESSAGE;
    }
}
