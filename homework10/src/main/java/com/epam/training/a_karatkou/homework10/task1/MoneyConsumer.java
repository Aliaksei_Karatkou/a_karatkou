package com.epam.training.a_karatkou.homework10.task1;

import java.math.BigDecimal;

/**
 * Class implements a consumer ATM.
 */
public class MoneyConsumer extends Atm implements Runnable {

    /**
     * How much money must this ATM withdrows.
     */
    protected BigDecimal moneyDecrement;
    /**
     * String to identification of this thread.
     */
    protected String taskName;

    /**
     * Object which implements syndronized method to withdraw money.
     */
    protected ThreadsSync threadsSync;

    /**
     * credit card to use.
     */
    protected Card usingCard;

    /**
     * Constructor.
     *
     * @param decrementMoneyAmmount How much money must this ATM withdrow.
     * @param threadId              String to identification of this thread
     * @param commonTheadsSync      Object which implements syndronized
     *                              method to withdraw money.
     * @param card                  card to use.
     * @throws IllegalArgumentException if any argument is null or
     *                                  money to withdraw is not positive.
     */
    public MoneyConsumer(final BigDecimal decrementMoneyAmmount,
                         final String threadId,
                         final ThreadsSync commonTheadsSync,
                         final Card card) {
        if ((decrementMoneyAmmount == null)
                || (decrementMoneyAmmount.signum() <= 0)
                || (threadId == null)
                || (commonTheadsSync == null)
                || (card == null)) {
            throw new IllegalArgumentException("Bad argument");
        }
        moneyDecrement = decrementMoneyAmmount;
        taskName = threadId;
        threadsSync = commonTheadsSync;
        usingCard = card;
    }

    /**
     * Method to implement Runnable interface.
     * It is started periodically.
     */
    public final void run() {
        threadsSync.changeMoney(usingCard, moneyDecrement,
                this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(),
                taskName);
    }


}
