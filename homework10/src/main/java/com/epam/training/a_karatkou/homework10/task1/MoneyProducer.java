package com.epam.training.a_karatkou.homework10.task1;

import java.math.BigDecimal;

/**
 * Class implements a producer ATM.
 */
public class MoneyProducer extends Atm implements Runnable {
    /**
     * How much money must this ATM adds.
     */
    protected BigDecimal moneyIncrement;
    /**
     * String to identification of this thread.
     */
    protected String taskName;

    /**
     * Object which implements syndronized method to withdraw money.
     */
    protected ThreadsSync threadsSync;

    /**
     * credit card to use.
     */
    protected Card usingCard;

    /**
     * Bad argument exception text.
     */
    public static final String BAD_ARGUMENT = "Bad argument";

    /**
     * Constructor.
     *
     * @param incrementMoneyAmmount How much money must this ATM add.
     * @param threadId              String to identification of this thread.
     * @param commonTheadsSync      Object which implements syndronized
     *                              method to withdraw money.
     * @param card                  card to use.
     * @throws IllegalArgumentException if any argument is null or
     *                                  money to add is not positive.
     */
    public MoneyProducer(final BigDecimal incrementMoneyAmmount,
                         final String threadId,
                         final ThreadsSync commonTheadsSync,
                         final Card card) {
        if ((incrementMoneyAmmount == null)
                || (incrementMoneyAmmount.signum() <= 0)
                || (threadId == null)
                || (commonTheadsSync == null)
                || (card == null)) {
            throw new IllegalArgumentException(BAD_ARGUMENT);
        }
        moneyIncrement = incrementMoneyAmmount;
        taskName = threadId;
        threadsSync = commonTheadsSync;
        usingCard = card;
    }

    @Override
    public final void run() {
        threadsSync.changeMoney(usingCard, moneyIncrement, this,
                MoneyProducer.class.getName(),
                MoneyConsumer.class.getName(), taskName);
    }


}
