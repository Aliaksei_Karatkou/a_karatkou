package com.epam.training.a_karatkou.homework10.task1;

import java.math.BigDecimal;


/**
 * Class to implement homework 10.
 */
public final class Juggler {
    /**
     * moneyProducer's ATMs count.
     */
    protected static final int ATM_PRODUCER_TEAM_ITEMS_COUNT = 5;

    /**
     * moneyConsumer's ATMs count.
     */
    protected static final int ATM_CONSUMER_TEAM_ITEMS_COUNT = 3;

    /**
     * Total ATMs count.
     */
    protected static final int ATM_TOTAL_ITEMS_COUNT =
            ATM_PRODUCER_TEAM_ITEMS_COUNT + ATM_CONSUMER_TEAM_ITEMS_COUNT;


    /**
     * Perion for running producer ATM (miliseconds).
     */
    protected static final int ATM_PRODUCER_TEAM_START_PERIOD_MILISECS = 2000;

    /**
     * Perion for running consumer ATM (miliseconds).
     */
    protected static final int ATM_CONSUMER_TEAM_START_PERIOD_MILISECS = 2500;

    /**
     * How much money add producer ATM.
     */
    protected static final int ATM_PRODUCER_MONEY_CHANGE = 5;

    /**
     * How much money withdrow consumer ATM.
     */
    protected static final int ATM_CONSUMER_MONEY_CHANGE = 5;


    /**
     * How often to check result (milisecs).
     */
    protected static final int CHECK_RESULT_PERIOD_MILISECS = 250;

    /**
     * Initial balance.
     */
    protected static final BigDecimal INITIAL_BALANCE = BigDecimal.valueOf(500);

    /**
     * Hidden constructor.
     */
    private Juggler() {

    }

    /**
     * Main method.
     *
     * @param args commsnd line parameters (ignored).
     */
    static void main(final String[] args) {
        // please use tests
    }
}
