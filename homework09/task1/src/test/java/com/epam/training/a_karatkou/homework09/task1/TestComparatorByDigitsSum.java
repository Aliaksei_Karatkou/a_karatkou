package com.epam.training.a_karatkou.homework09.task1;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static java.math.BigInteger.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestComparatorByDigitsSum {
    @Test(expected = IllegalArgumentException.class)
    public void testGetDigitsSumNull() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        comparator.getDigitsSum(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDigitsNotInteger() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        comparator.getDigitsSum((Float) 4.5f);
    }

    @Test
    public void testGetDigitsNormal() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        assertEquals(10, comparator.getDigitsSum((Long) 1234L));
    }

    @Test
    public void testGetDigitsBigInteger() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        BigInteger bigInteger = BigInteger.valueOf(9000000000000000001L);
        bigInteger = bigInteger.add(BigInteger.valueOf(9000000000000000002L));
        assertEquals(12, comparator.getDigitsSum(bigInteger));
    }

    @Test
    public void testGetDigitsNegative() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        assertEquals(10,
                comparator.getDigitsSum(BigDecimal.valueOf(-4312)));
    }

    // --- compare
    @Test(expected = IllegalArgumentException.class)
    public void testCompareParam1IsNull() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        comparator.compare(null, (Integer) 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCompareParam2IsNull() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        comparator.compare((Integer) 3, null);
    }

    @Test
    public void testCompareLess() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        assertTrue(comparator.compare((Integer) 3, (Integer) 5) < 0);
    }

    @Test
    public void testCompareEqual() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        assertTrue(comparator.compare((Integer) 3, (Integer) 3) == 0);
    }

    @Test
    public void testCompareGreate() {
        ComparatorByDigitsSum comparator = new ComparatorByDigitsSum();
        assertTrue(comparator.compare((Integer) 5, (Integer) 3) > 0);
    }

}
