package com.epam.training.a_karatkou.homework09.task1;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class TestSorter {
    @Test(expected = IllegalArgumentException.class)
    public void testdoSortNull() {
        new Sorter().doSort(null);
    }

    @Test
    public void testdoSort() {
        Sorter sorter = new Sorter();
        Integer[] array = {6, 3, 16, 234, 8, 99, -98};
        sorter.doSort(array);
        Integer[] expectedArray = {3, 6, 16, 8, 234, -98, 99};
        assertTrue(Arrays.equals(expectedArray, array));
    }


}
