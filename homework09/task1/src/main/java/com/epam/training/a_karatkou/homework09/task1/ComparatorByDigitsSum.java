package com.epam.training.a_karatkou.homework09.task1;

import java.util.Comparator;

/**
 * Implements comparator.
 * Compares by sum of the integer number's digits.
 *
 * @param <T> Type of the items
 */
class ComparatorByDigitsSum<T> implements Comparator<T> {
    /**
     * RegExp to validate an integer number that stores in a String.
     */
    public static final String INTEGER_NUMBER_REGEXP = "^-?\\d+$";

    /**
     * returns sum of the digits of the integer number.
     *
     * @param number Number to process
     * @param <T>    Type of the integer variable
     * @return Sum of the digits of the integer number.
     * @throws IllegalArgumentException if null is passed as argument or passed
     *                                  not integer value.
     */
    protected <T> int getDigitsSum(final T number) {
        if (number == null) {
            throw new IllegalArgumentException("Null is used as argument.");
        }

        String strNumber = number.toString();
        if (strNumber.matches(INTEGER_NUMBER_REGEXP)) {
            int sum = 0;
            for (int i = 0; i < strNumber.length(); i++) {
                char digit = strNumber.charAt(i);
                if (digit != '-') {
                    sum += Character.getNumericValue(digit);
                }
            }
            return sum;
        } else {
            throw new IllegalArgumentException("We can handle only integer "
                    + "values.");
        }
    }

    /**
     * Compares two numbers using ComparatorByDigitsSum comparator.
     *
     * @param number1 The first value to compare.
     * @param number2 The second value to compare.
     * @return 1 if number1 is greater than number2, -1 if number1 is less than
     * number2, 0 if number1 equals to number2.
     * @thows IllegalArgumentException if null is passed as any argument.
     */
    public int compare(final T number1, final T number2) {
        if ((number1 == null) || (number2 == null)) {
            throw new IllegalArgumentException("Null is used as argument.");
        }
        int sum1 = getDigitsSum(number1);
        int sum2 = getDigitsSum(number2);
        if (sum1 > sum2) {
            return 1;
        } else if (sum1 == sum2) {
            return 0;
        } else {
            return -1;
        }
    }
}
