package com.epam.training.a_karatkou.homework09.task1;

import java.util.Arrays;

/**
 * Sorter to sort integer arrays.
 */
public class Sorter {
    /**
     * Sorts array by sum of the digits of each number.
     * If array contain any value that doesn't consists of "-" and digits
     * an IllegalArgumentException will be thrown by underlying methods.
     * @param array array to sort
     * @param <T> Type of the array's items.
     */
    public final <T extends Number> void doSort(final T[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Null is used as argument.");
        }
        Arrays.sort(array, new ComparatorByDigitsSum<T>());
    }

}


