package com.epam.training.a_karatkou.homework09.task2;

import java.util.HashSet;

/**
 * Class implements operations on sets.
 * Each operation has two implementations.
 */
public class SetsCalculator {
    /**
     * Checks if objects equal null.
     *
     * @param obj1 object 1
     * @param obj2 object 2.
     * @throws IllegalArgumentException if any of objects equals null.
     */
    protected final void validateParametersAgainstNull(final Object obj1,
                                                       final Object obj2) {
        if ((obj1 == null) || (obj2 == null)) {
            throw new IllegalArgumentException("Set is null.");
        }
    }

    /**
     * Implements union of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> union(final HashSet<E> setA,
                                      final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>(setA);
        result.addAll(setB);
        return result;
    }

    /**
     * Alternative implementation of the union of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> alternativeUnion(final HashSet<E> setA,
                                                 final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>();
        for (E item : setA) {
            result.add(item);
        }
        for (E item : setB) {
            result.add(item);
        }
        return result;
    }

    /**
     * Implements intersection of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> intersection(final HashSet<E> setA,
                                             final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>(setA);
        result.retainAll(setB);
        return result;
    }


    /**
     * Alternative implementation of the intersection of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> alternativeIntersection(final HashSet<E> setA,
                                                        final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>();
        for (E item : setA) {
            if (setB.contains(item)) {
                result.add(item);
            }
        }
        return result;
    }

    /**
     * Implements "minus" of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> minus(final HashSet<E> setA,
                                      final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>(setA);
        result.removeAll(setB);
        return result;
    }

    /**
     * Alternative implementation of the "minus" of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> alternativeMinus(final HashSet<E> setA,
                                                 final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>();
        for (E item : setA) {
            if (!setB.contains(item)) {
                result.add(item);
            }
        }
        return result;
    }


    /**
     * Implements difference of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> difference(final HashSet<E> setA,
                                           final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>(setA);
        for (E item : setB) {
            if (!result.add(item)) {
                result.remove(item);
            }
        }
        return result;
    }

    /**
     * Alternative implementation of the difference of the set1 and set2.
     *
     * @param setA set1
     * @param setB set2
     * @param <E>  type of the sets
     * @return Resulting set.
     */
    public final <E> HashSet<E> alternativeDifference(final HashSet<E> setA,
                                                      final HashSet<E> setB) {
        validateParametersAgainstNull(setA, setB);
        HashSet<E> result = new HashSet<>();

        for (E item : setA) {
            if (!setB.contains(item)) {
                result.add(item);
            }
        }
        for (E item : setB) {
            if (!setA.contains(item)) {
                result.add(item);
            }
        }
        return result;
    }
}
