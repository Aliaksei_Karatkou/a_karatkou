package com.epam.training.a_karatkou.homework09.task2;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSetsCalculator {
    @Test(expected = IllegalArgumentException.class)
    public void testValidateParametersAgainstParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.validateParametersAgainstNull(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateParametersAgainstParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.validateParametersAgainstNull(new HashSet<String>(), null);
    }

    //------ Union -----
    @Test(expected = IllegalArgumentException.class)
    public void testUnionParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.union(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnionParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.union(new HashSet<String>(), null);
    }

    @Test
    public void testUnionNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.union(setA, setB);
        assertEquals(3, setResult.size());
        assertTrue(setResult.contains("A"));
        assertTrue(setResult.contains("B"));
        assertTrue(setResult.contains("C"));
    }

    // ----- Intersection
    @Test(expected = IllegalArgumentException.class)
    public void testIntersectionParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.intersection(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIntersectionParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.intersection(new HashSet<String>(), null);
    }

    @Test
    public void testIntersectionNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.intersection(setA, setB);
        assertEquals(1, setResult.size());
        assertTrue(setResult.contains("B"));
    }

    // ------- Minus ------

    @Test(expected = IllegalArgumentException.class)
    public void testMinusParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.minus(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinusParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.minus(new HashSet<String>(), null);
    }

    @Test
    public void testMinusNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.minus(setA, setB);
        assertEquals(1, setResult.size());
        assertTrue(setResult.contains("A"));
    }

    // ----- Difference
    @Test(expected = IllegalArgumentException.class)
    public void testDifferenceParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.difference(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDifferenceParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.difference(new HashSet<String>(), null);
    }

    @Test
    public void testDifferenceNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.difference(setA, setB);
        assertEquals(2, setResult.size());
        assertTrue(setResult.contains("A"));
        assertTrue(setResult.contains("C"));
    }

    // ------- Alternative ----------
    //------ Alternative Union -----
    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeUnionParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeUnion(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeUnionParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeUnion(new HashSet<String>(), null);
    }

    @Test
    public void testAlternativeUnionNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.alternativeUnion(setA, setB);
        assertEquals(3, setResult.size());
        assertTrue(setResult.contains("A"));
        assertTrue(setResult.contains("B"));
        assertTrue(setResult.contains("C"));
    }

    // ----- alternativeIntersection
    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeIntersectionParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeIntersection(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeIntersectionParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeIntersection(new HashSet<String>(), null);
    }

    @Test
    public void testAlternativeIntersectionNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.alternativeIntersection(setA, setB);
        assertEquals(1, setResult.size());
        assertTrue(setResult.contains("B"));
    }

    // ------- alternativeMinus ------

    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeMinusParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeMinus(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeMinusParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeMinus(new HashSet<String>(), null);
    }

    @Test
    public void testAlternativeMinusNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.alternativeMinus(setA, setB);
        assertEquals(1, setResult.size());
        assertTrue(setResult.contains("A"));
    }

    // ----- alternativeDifference
    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeDifferenceParam1Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeDifference(null, new HashSet<String>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAlternativeDifferenceParam2Null() {
        SetsCalculator calc = new SetsCalculator();
        calc.alternativeDifference(new HashSet<String>(), null);
    }

    @Test
    public void testAlternativeDifferenceNormal() {
        SetsCalculator calc = new SetsCalculator();
        HashSet<String> setA = new HashSet<>();
        HashSet<String> setB = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB.add("B");
        setB.add("C");
        HashSet<String> setResult = calc.alternativeDifference(setA, setB);
        assertEquals(2, setResult.size());
        assertTrue(setResult.contains("A"));
        assertTrue(setResult.contains("C"));
    }

}

