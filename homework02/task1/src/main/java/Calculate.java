/**
 * Class Calculate implements homework 2 task 1.
 * <p>
 * {@value #PARAMETER_NUM_A} fff ggg.
 */
public class Calculate {

    /**
     * PARAMETER_NUM_A index of the argument "a" in the command line.
     */
    static final int PARAMETER_NUM_A = 0;

    /**
     * PARAMETER_NUM_P index of the argument "p" in the command line.
     */
    static final int PARAMETER_NUM_P = 1;

    /**
     * PARAMETER_NUM_M1 index of the argument "m1" in the command line.
     */
    static final int PARAMETER_NUM_M1 = 2;

    /**
     * PARAMETER_NUM_M2 index of the argument "m2" in the command line.
     */
    static final int PARAMETER_NUM_M2 = 3;

    /**
     * PARAMETERS_COUNT_REQUIRED total required command line parameters.
     */
    static final int PARAMETERS_COUNT_REQUIRED = 4;
    /**
     * STR_PARAMETERS_HAVE_NOT_NUMBERS.
     * String to write when parameters have one or more not-number
     * or number is in wrong format
     */
    static final String STR_PARAMETERS_HAVE_NOT_NUMBERS =
            "Wrong parameters: not numbers or wrong number's type";
    /**
     *  STR_PARAMETERS_COUNT_MISSMATCH.
     * String to write when parameters count doesn't match required
     */
    static final String STR_PARAMETERS_COUNT_MISSMATCH =
        "Wrong parameters: count must be equal " + PARAMETERS_COUNT_REQUIRED;

    /**
     * Actual calculation.
     *
     * @param a  purpose unknown.
     * @param p  purpose unknown.
     * @param m1 purpose unknown.
     * @param m2 purpose unknown.
     * @return result of the calculation.
     */
    public static strictfp double doCalculation(final int a,
                                                final int p,
                                                final double m1,
                                                final double m2) {


        double dividend = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3);
        double divider = Math.pow(p, 2) * (m1 + m2);
        return dividend / divider;
    }

    /**
     * Wrapper for main method (for testing purposes).
     * This method also checks number of arguments
     *
     * @param args arguments of the "main" method
     * @return string which contains result or error message
     */

    public static String calculationWrapper(final String[] args) {
        String result;
        if (args.length != PARAMETERS_COUNT_REQUIRED) {
            result = STR_PARAMETERS_COUNT_MISSMATCH;
        } else {
            try {
                int a;
                int p;
                double m1;
                double m2;

                a = Integer.parseInt(args[PARAMETER_NUM_A]);
                p = Integer.parseInt(args[PARAMETER_NUM_P]);
                m1 = Double.parseDouble(args[PARAMETER_NUM_M1]);
                m2 = Double.parseDouble(args[PARAMETER_NUM_M2]);

                double calculationResult = doCalculation(a, p, m1, m2);
                result = Double.toString(calculationResult);
            } catch (NumberFormatException e) {
                result = STR_PARAMETERS_HAVE_NOT_NUMBERS;
            }
        }
        return result;
    }

    /**
     * Main method of this class.
     * Calls CalculationWrapper and prints result of calculation
     * Caclulates value G=4*Pi^2*a^3/(p^2*(m1+m2)
     *
     * @param args Command line parameters
     */
    public static void main(final String[] args) {
        System.out.println(calculationWrapper(args));
    }
}
