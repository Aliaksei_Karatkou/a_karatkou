import org.junit.Test;

import static org.junit.Assert.*;

public class TestCaclulate {
    final static double DELTA = 0;

    @Test
    public void testWrongParametersArgumentsAreAbsent() {
        String[] args = {};
        assertEquals("Wrong parameters: count must be equal 4", Calculate.calculationWrapper(args));
    }

    @Test
    public void testWrongParametersArgumentsCountNotEqualToRequired() {
        String[] args = {"1", "2", "3", "4", "5"};
        assertEquals("Wrong parameters: count must be equal 4", Calculate.calculationWrapper(args));
    }

    @Test
    public void testAnyArgumentIsNotNumber() {
        String[] args = {"1", "2", "Hello", "4"};
        assertEquals("Wrong parameters: not numbers or wrong number's type", Calculate.calculationWrapper(args));
    }

    @Test
    public void testCalculationWrapper1() {
        String[] args = {"1", "2", "3", "4"};
        assertEquals("1.4099434858699083", Calculate.calculationWrapper(args));
    }

    @Test
    public void testCalculationWrapper2() {
        String[] args = {"-1", "0", "3", "4"};
        assertEquals("-Infinity", Calculate.calculationWrapper(args));
    }

    @Test
    public void testCalculationWrapper3() {
        String[] args = {"1", "0", "3", "4"};
        assertEquals("Infinity", Calculate.calculationWrapper(args));
    }

    @Test
    public void testCalculationNormal() {
        assertEquals(1.4099434858699083, Calculate.doCalculation(1, 2, 3, 4), DELTA);
    }

    @Test
    public void testCalculationNegativeInfinity() {
        assertEquals(Double.NEGATIVE_INFINITY, Calculate.doCalculation(-1, 0, 3, 4), DELTA);
    }

    @Test
    public void testCalculationPositiveInfinity() {
        assertEquals(Double.POSITIVE_INFINITY, Calculate.doCalculation(1, 0, 3, 4), DELTA);
    }

    @Test
    public void testCalculationNaN() {
        assertEquals(Double.NaN, Calculate.doCalculation(0, 0, 0, 0), DELTA);
    }


}
