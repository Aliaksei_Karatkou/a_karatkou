import org.junit.Test;

import static org.junit.Assert.*;

public class TestHomework2Task2 {

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_Empty() {
        String[] args = {};
        assertEquals("Wrong parameters: count must be equal 3", Homework2Task2.mainMethodWrapper(args));
    }

    // Fibonacci While
    @Test
    public void testMainMethodWrapper_Fibonacci_While_num_negative() {
        String[] args = {"1", "1", "-1"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_While_num_zero() {
        String[] args = {"1", "1", "0"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_While_num_1() {
        String[] args = {"1", "1", "1"};
        assertEquals("0", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_While_num_2() {
        String[] args = {"1", "1", "2"};
        assertEquals("0 1", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_While_num_6() {
        String[] args = {"1", "1", "6"};
        assertEquals("0 1 1 2 3 5", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_While_letters() {
        String[] args = {"a", "b", "c"};
        assertEquals("Wrong parameters: not integer numbers", Homework2Task2.mainMethodWrapper(args));
    }

    // Fibonacci Do-While

    @Test
    public void testMainMethodWrapper_Fibonacci_DoWhile_num_negative() {
        String[] args = {"1", "2", "-1"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_DoWhile_num_zero() {
        String[] args = {"1", "2", "0"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_DoWhile_num_1() {
        String[] args = {"1", "2", "1"};
        assertEquals("0", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_DoWhile_num_2() {
        String[] args = {"1", "2", "2"};
        assertEquals("0 1", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_DoWhile_num_6() {
        String[] args = {"1", "2", "6"};
        assertEquals("0 1 1 2 3 5", Homework2Task2.mainMethodWrapper(args));
    }
    // Fibonacci For

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_num_negative() {
        String[] args = {"1", "3", "-1"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_num_zero() {
        String[] args = {"1", "3", "0"};
        assertEquals("n must be 1 or greater", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_num_1() {
        String[] args = {"1", "3", "1"};
        assertEquals("0", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_num_2() {
        String[] args = {"1", "3", "2"};
        assertEquals("0 1", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Fibonacci_ForLoop_num_6() {
        String[] args = {"1", "3", "6"};
        assertEquals("0 1 1 2 3 5", Homework2Task2.mainMethodWrapper(args));
    }
    // Factorial

    // Fibonacci While
    @Test
    public void testMainMethodWrapper_Factorial_While_num_negative() {
        String[] args = {"2", "1", "-1"};
        assertEquals("n must be [0..20]", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_num_zero() {
        String[] args = {"2", "1", "0"};
        assertEquals("1", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_num_1() {
        String[] args = {"2", "1", "1"};
        assertEquals("1", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_num_2() {
        String[] args = {"2", "1", "2"};
        assertEquals("2", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_num_6() {
        String[] args = {"2", "1", "6"};
        assertEquals("720", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_letters() {
        String[] args = {"a", "b", "c"};
        assertEquals("Wrong parameters: not integer numbers", Homework2Task2.mainMethodWrapper(args));
    }

    @Test
    public void testMainMethodWrapper_Factorial_While_num_TooBig() {
        String[] args = {"2", "1", "77"};
        assertEquals("n must be [0..20]", Homework2Task2.mainMethodWrapper(args));
    }
}
