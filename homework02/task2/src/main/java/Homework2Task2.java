/**
 * Class Homework2Task2 implements homework 2 task 2.
 */

public class Homework2Task2 {
    /**
     * ALGORITHM_FIBONACCI ID for fibonacci calculation algorithm.
     */
    static final int ALGORITHM_FIBONACCI = 1;

    /**
     * ALGORITHM_FACTORIAL id for factorial calculation algorithm.
     */
    static final int ALGORITHM_FACTORIAL = 2;

    /**
     * LOOP_WHILE id for "while"-loop.
     */
    static final int LOOP_WHILE = 1;

    /**
     *  LOOP_DO_WHILE id for "do-while"-loop.
     */

    static final int LOOP_DO_WHILE = 2;

    /**
     * LOOP_FOR id for "for"-loop.
     */

    static final int LOOP_FOR = 3;

    /**
     * FACTORIAL_MAX_N
     * maximal number, allowed to calculate factorial (using long variables).
     */
    static final int FACTORIAL_MAX_N = 20;

    /**
     *  ERROR_PARAMETERS_COUNT_MISSMATCH
     *     String to report if count of parameters not equals 3.
     */
    static final String ERROR_PARAMETERS_COUNT_MISSMATCH =
            "Wrong parameters: count must be equal 3";
    /**
     * ERROR_PARAMETERS_NOT_NUMBERS
     *   String to report if some parameters are not integer numbers.
     */
    static final String ERROR_PARAMETERS_NOT_NUMBERS =
            "Wrong parameters: not integer numbers";
    /**
     *  ERROR_ALGORITHM_MISSMATCH
     *     String to report if algorithm id is incorrect.
     */
    static final String ERROR_ALGORITHM_MISSMATCH =
            "algorithmId must be "
                    + ALGORITHM_FIBONACCI
                    + " or "
                    + ALGORITHM_FACTORIAL;
    /**
     *  ERROR_LOOP_ID_MISSMATCH.
     *     String to report if loop id is incorrect
     */
    static final String ERROR_LOOP_ID_MISSMATCH =
            "loopType must be "
                    + LOOP_WHILE + "," + LOOP_DO_WHILE
                    + " or " + LOOP_FOR;
    /**
     * ERROR_FACTIORIAL_N_OUT_OF_RANGES.
     *     String to report if N is out of ranges
     *     and we must calculate factorial
     */
    static final String ERROR_FACTIORIAL_N_OUT_OF_RANGES =
            "n must be [0.." + FACTORIAL_MAX_N + "]";
    /**
     * ERROR_FIBONACCI_N_OUT_OF_RANGES.
     *     String to report if N is out of ranges
     *     and we must calculate fibonacci numbers
     */
    static final String ERROR_FIBONACCI_N_OUT_OF_RANGES =
            "n must be 1 or greater";

    /**
     * PARAMETER_NUM_ALGORITHM_ID.
     *     Index of the algorigth id in input array of command line parameters
     */
    static final int PARAMETER_NUM_ALGORITHM_ID = 0;
    /**
     *  PARAMETER_NUM_LOOP_KIND.
     *     Index of the algorigth id in input array of command line parameters
     */
    static final int PARAMETER_NUM_LOOP_KIND = 1;
    /**
     * PARAMETER_NUM_N.
     *     Index of the algorigth id in input array of command line parameters
     */
    static final int PARAMETER_NUM_N = 2;
    /**
     *  PARAMETERS_REQIRED.
     *     Required number of parameters of a command line.
     */
    static final int PARAMETERS_REQIRED = 3;


    /**
     * Method returns factorial calculated using While-loop.
     *
     * @param n calculates factorial of this number
     * @return Factorial (n)
     */
    private static long getFactorialLoopWhile(final int n) {
        long result = 1;
        int i = 2;
        while (i <= n) {
            result *= i;
            i++;
        }
        return result;
    }

    /**
     * Method returns factorial calculated using Do-While-loop.
     *
     * @param n calculates factorial of this number
     * @return Factorial (n)
     */
    private static long getFactorialLoopDoWhile(final int n) {
        long result = 1;
        int i = 1;
        do {
            result *= i;
            i++;
        } while (i <= n);
        return result;
    }

    /**
     * Method returns factorial calculated using For-loop.
     *
     * @param n calculates factorial of this number
     * @return Factorial (n)
     */
    private static long getFactorialLoopFor(final int n) {
        long result = 1;
        if (n != 0) {
            for (int i = 2; i <= n; i++) {
                result *= i;
            }
        }
        return result;
    }

    /**
     * Method returns factorial calculated using the required loop.
     *
     * @param loopType choose type of the loop (1-3)
     * @param n        calculates factorial of this number
     * @return String with the required result or an error message
     */
    private static String getFactorial(final int loopType, final int n) {
        String result;

        switch (loopType) {
            case LOOP_FOR:
                result = Long.toString(getFactorialLoopFor(n));
                break;
            case LOOP_DO_WHILE:
                result = Long.toString(getFactorialLoopDoWhile(n));
                break;
            case LOOP_WHILE:
            default:   // method's return value must be defined,
                       // so lets assume that LOOP_WHILE is also default case
                result = Long.toString(getFactorialLoopWhile(n));
                break;
        }
        return result;
    }

    /**
     * Method returns fibonacci numbers usung While-loop.
     *
     * @param n Fibonacci numbers count to display or factorial
     *          of this parameter to calculate
     * @return String with the required result or an error message
     */
    private static String getFibonacciLoopWhile(final int n) {
        String result;
        if (1 == n) {
            result = "0";
        } else {
            int farNumber = 0;
            int nearNumber = 1;
            result = "0 1";
            int i = 2;
            while (i < n) {
                int newNumber = farNumber + nearNumber;
                result += " " + newNumber;
                farNumber = nearNumber;
                nearNumber = newNumber;
                i++;
            }
        }
        return result;
    }

    /**
     * Method returns fibonacci numbers usung Do-While-loop.
     *
     * @param n Fibonacci numbers count to display or factorial
     *          of this parameter to calculate
     * @return String with the required result or an error message
     */
    private static String getFibonacciLoopDoWhile(final int n) {
        String result;
        if (1 == n) {
            result = "0";
        } else if (2 == n) {
            result = "0 1";
        } else {
            int farNumber = 0;
            int nearNumber = 1;
            result = "0 1";
            int i = 2;
            do {
                int newNumber = farNumber + nearNumber;
                result += " " + newNumber;
                farNumber = nearNumber;
                nearNumber = newNumber;
                i++;
            } while (i < n);
        }
        return result;
    }

    /**
     * Method returns fibonacci numbers usung For-loop.
     *
     * @param n Fibonacci numbers count to display or factorial
     *          of this parameter to calculate
     * @return String with the required result or an error message
     */
    private static String getFibonacciLoopFor(final int n) {
        String result;
        if (1 == n) {
            result = "0";
        } else {
            int farNumber = 0;
            int nearNumber = 1;
            result = "0 1";
            int i;
            for (i = 2; i < n; i++) {
                int newNumber = farNumber + nearNumber;
                result += " " + newNumber;
                farNumber = nearNumber;
                nearNumber = newNumber;
            }
        }
        return result;
    }

    /**
     * Method returns fibonacci numbers using the required loop.
     *
     * @param loopType choose type of the loop (1-3)
     * @param n        Fibonacci numbers count to display or factorial
     *                 of this parameter to calculate
     * @return String with the required result or an error message
     */
    private static String getFibonacci(final int loopType, final int n) {
        String result;
        switch (loopType) {
            case LOOP_WHILE:
                result = getFibonacciLoopWhile(n);
                break;
            case LOOP_DO_WHILE:
                result = getFibonacciLoopDoWhile(n);
                break;
            case LOOP_FOR:
            default:  // return value must be defined,
                      // lets assume that loop "for" is also default case
                result = getFibonacciLoopFor(n);
                break;
        }
        return result;
    }

    /**
     * Method calls factorial or fibonacci numbers calculation.
     * using the required loop
     *
     * @param algorithmId algorithm's id (1-2)
     * @param loopType    choose type of the loop (1-3)
     * @param n           Fibonacci numbers count to display or factorial
     *                    of this parameter to calculate
     * @return String with the required result or an error message
     */

    private static String doHomework(final int algorithmId,
                                     final int loopType,
                                     final int n) {
        if (ALGORITHM_FACTORIAL == algorithmId) {
            return getFactorial(loopType, n);
        } else {
            return getFibonacci(loopType, n);
        }
    }

    /**
     * Wrapper for the main method. Can be used for testing.
     *
     * @param args input parameters according with the condition of the task
     * @return String with the required result or an error message
     * @see doHomework
     */

    public static String mainMethodWrapper(final String[] args) {
        String result;
        if (args.length != PARAMETERS_REQIRED) {
            result = ERROR_PARAMETERS_COUNT_MISSMATCH;
        } else {
            try {
                int algorithmId =
                        Integer.parseInt(args[PARAMETER_NUM_ALGORITHM_ID]);
                int loopType =
                        Integer.parseInt(args[PARAMETER_NUM_LOOP_KIND]);
                int n = Integer.parseInt(args[PARAMETER_NUM_N]);
                if ((algorithmId != ALGORITHM_FIBONACCI)
                        && (algorithmId != ALGORITHM_FACTORIAL)) {
                    result = ERROR_ALGORITHM_MISSMATCH;
                } else if ((loopType != LOOP_WHILE)
                        && (loopType != LOOP_DO_WHILE)
                        && (loopType != LOOP_FOR)) {
                    result = ERROR_LOOP_ID_MISSMATCH;
                } else if ((algorithmId == ALGORITHM_FACTORIAL)
                        && ((n < 0) || (n > FACTORIAL_MAX_N))) {
                    result = ERROR_FACTIORIAL_N_OUT_OF_RANGES;
                } else if ((n <= 0) && (algorithmId == ALGORITHM_FIBONACCI)) {
                    result = ERROR_FIBONACCI_N_OUT_OF_RANGES;
                } else {
                    result = doHomework(algorithmId, loopType, n);
                }
            } catch (NumberFormatException e) {
                result = ERROR_PARAMETERS_NOT_NUMBERS;
            }
        }
        return result;
    }

    /**
     * Implements homework 2 (part 2) task.
     * Prints result or anerror message to the System.out
     *
     * @param args input arguments according with the condition of the task
      */

    public static void main(final String[] args) {
        System.out.println(mainMethodWrapper(args));
    }
}
