package com.epam.training.a_karatkou.homework12.task1;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

public class TestMoneyProducer {
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyProducerMoneyDiffIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyProducer(null,
                "str",
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyProducerMoneyDiffIsNotPositive(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyProducer(BigDecimal.ZERO,
                "str",
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyProducerThreadIdIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyProducer(BigDecimal.ONE,
                null,
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyProducerThreadSyncIsNull(){
        new MoneyProducer(BigDecimal.ONE,
                "str",
                null,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyProducerCardIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyProducer(BigDecimal.ONE,
                "id",
                threadSync,
                null);
    }
    @Test
    public void testMoneyProducerNorm(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyProducer(BigDecimal.ONE,
                "id",
                threadSync,
                new CreditCard("d"));
    }
    @Test
    public void testMoneyProducerNorm2(){
        ThreadsSync threadSync = new ThreadsSync();
        CreditCard card = new CreditCard("e", BigDecimal.TEN);
        MoneyProducer producer = new MoneyProducer(BigDecimal.ONE,
                "id",
                threadSync,
                card);
        producer.run();
        assertTrue(card.balance.compareTo(BigDecimal.valueOf(11))==0);



    }

}
