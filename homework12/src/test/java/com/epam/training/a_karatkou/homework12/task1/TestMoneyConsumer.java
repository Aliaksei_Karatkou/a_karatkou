package com.epam.training.a_karatkou.homework12.task1;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestMoneyConsumer {
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyConsumerMoneyDiffIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyConsumer(null,
                "str",
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyConsumerMoneyDiffIsNotPositive(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyConsumer(BigDecimal.ZERO,
                "str",
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyConsumerThreadIdIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyConsumer(BigDecimal.ONE,
                null,
                threadSync,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyConsumerThreadSyncIsNull(){
        new MoneyConsumer(BigDecimal.ONE,
                "str",
                null,
                new CreditCard("d"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMoneyConsumerCardIsNull(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyConsumer(BigDecimal.ONE,
                "id",
                threadSync,
                null);
    }
    @Test
    public void testMoneyConsumerNorm(){
        ThreadsSync threadSync = new ThreadsSync();
        new MoneyConsumer(BigDecimal.ONE,
                "id",
                threadSync,
                new CreditCard("d"));
    }
    @Test
    public void testMoneyConsumerNorm2(){
        ThreadsSync threadSync = new ThreadsSync();
        CreditCard card = new CreditCard("e", BigDecimal.TEN);
        MoneyConsumer consumer = new MoneyConsumer(BigDecimal.ONE,
                "id",
                threadSync,
                card);
        consumer.run();
        assertTrue(card.balance.compareTo(BigDecimal.valueOf(9))==0);



    }

}
