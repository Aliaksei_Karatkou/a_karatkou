package com.epam.training.a_karatkou.homework12.task1;

import java.math.BigDecimal;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Class to implement homework 12.
 */
public final class Juggler {
    /**
     * moneyProducer's ATMs count.
     */
    protected static final int ATM_PRODUCER_TEAM_ITEMS_COUNT = 5;

    /**
     * moneyConsumer's ATMs count.
     */
    protected static final int ATM_CONSUMER_TEAM_ITEMS_COUNT = 3;

    /**
     * Total ATMs count.
     */
    protected static final int ATM_TOTAL_ITEMS_COUNT =
            ATM_PRODUCER_TEAM_ITEMS_COUNT + ATM_CONSUMER_TEAM_ITEMS_COUNT;


    /**
     * Perion for running producer ATM (miliseconds).
     */
    protected static final int ATM_PRODUCER_TEAM_START_PERIOD_MILISECS = 2000;

    /**
     * Perion for running consumer ATM (miliseconds).
     */
    protected static final int ATM_CONSUMER_TEAM_START_PERIOD_MILISECS = 2500;

    /**
     * How much money add producer ATM.
     */
    protected static final int ATM_PRODUCER_MONEY_CHANGE = 5;

    /**
     * How much money withdrow consumer ATM.
     */
    protected static final int ATM_CONSUMER_MONEY_CHANGE = 5;


    /**
     * How often to check result (milisecs).
     */
    protected static final int CHECK_RESULT_PERIOD_MILISECS = 250;

    /**
     * Initial balance.
     */
    protected static final BigDecimal INITIAL_BALANCE = BigDecimal.valueOf(500);

    /**
     * Hidden constructor.
     */
    private Juggler() {

    }

    /**
     * Main method.
     *
     * @param args commsnd line parameters (ignored).
     */
    public static void main(final String[] args) {
        CreditCard card = new CreditCard("public", INITIAL_BALANCE);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(
                ATM_TOTAL_ITEMS_COUNT);
        ThreadsSync threadSync = new ThreadsSync();
        Atm[] atms = new Atm[ATM_TOTAL_ITEMS_COUNT];


        for (int i = 0; i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            atms[i] =
                    new MoneyProducer(BigDecimal.valueOf(
                            ATM_PRODUCER_MONEY_CHANGE), "Prod[" + i + "]",
                            threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            atms[i] =
                    new MoneyConsumer(BigDecimal.valueOf(
                            ATM_CONSUMER_MONEY_CHANGE),
                            "Cons[" + i + "]", threadSync, card);
            atms[i].assignCard(card);
        }
        for (
                int i = 0;
                i < ATM_PRODUCER_TEAM_ITEMS_COUNT; i++) {
            executor.scheduleAtFixedRate((MoneyProducer) atms[i],
                    0, ATM_PRODUCER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }
        for (
                int i = ATM_PRODUCER_TEAM_ITEMS_COUNT;
                i < ATM_TOTAL_ITEMS_COUNT;
                i++) {
            executor.scheduleAtFixedRate((MoneyConsumer) atms[i],
                    0, ATM_CONSUMER_TEAM_START_PERIOD_MILISECS,
                    TimeUnit.MILLISECONDS);
        }

        while (threadSync.isBalanceInRange(card)) {
            try {
                Thread.sleep(CHECK_RESULT_PERIOD_MILISECS);
            } catch (InterruptedException ex) {
                break;
            }
        }
        executor.shutdown();
    }
}
