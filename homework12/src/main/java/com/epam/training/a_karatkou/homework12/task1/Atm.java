package com.epam.training.a_karatkou.homework12.task1;

import java.math.BigDecimal;

/**
 * Class to emulate ATM.
 */
public class Atm {
    /**
     * Message if called methods to work with a card.
     * But card is not assigned
     */
    static final String CARD_NOT_INSERTED = "Card not inserted";

    /**
     * Card to process.
     */
    private Card card;

    /**
     * scale, used to display balance.
     */
    private int scale;

    /**
     * Assigns card.
     *
     * @param newCard card object
     */
    public final void assignCard(final Card newCard) {
        card = newCard;
    }
    /**
     * Default constructor.
     */
    public Atm() {
        scale = Card.DEFAULT_DISPLAY_SCALE;
    }

    /**
     * Withdrow from card method.
     *
     * @param moneyToWithdrow money ammount to withdrow from the card
     * @return true if operation successful, false otherwise
     */
    public final boolean withdrowFromCard(final BigDecimal moneyToWithdrow) {
        if ((moneyToWithdrow != null) && (card != null)) {
            return card.withdrowFromBalance(moneyToWithdrow);
        } else {
            return false;
        }
    }

    /**
     * Method for adding money to card.
     *
     * @param moneyToAdd money ammount to add to the card
     * @return true if operation successful
     */
    public final boolean addMoneyToCard(final BigDecimal moneyToAdd) {
        if ((moneyToAdd != null) && (card != null)) {
            return card.addToBalance(moneyToAdd);
        } else {
            return false;
        }
    }

    /**
     * Gets card's balance and returns it in string format.
     *
     * @return balance of the card or CARD_NOT_INSERTED if
     * card is not assigned
     */
    public final String displayBalance() {
        if (card != null) {
            BigDecimal balance =
                    card.getBalance().setScale(scale,
                            BigDecimal.ROUND_HALF_UP);
            return balance.toString();
        } else {
            return CARD_NOT_INSERTED;
        }
    }


    /**
     * Sets scale for displaying card's balance.
     *
     * @param newScale scale to use.
     *                 If scale negative then will be used Card
     *                 .DEFAULT_DISPLAY_SCALE.
     */
    public final void setScale(final int newScale) {
        if (newScale < 0) {
            scale = Card.DEFAULT_DISPLAY_SCALE;
        } else {
            scale = newScale;
        }
    }


}
