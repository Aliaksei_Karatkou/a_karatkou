package com.epam.training.a_karatkou.hw3_extratask;

/**
 * class Median implements homework3 extra task.
 */
public final class Median {

    /**
     * Returns median for an integer array.
     *
     * @param array array for calculation
     * @return median for the array
     */
    static float median(final int[] array) {
        if (array.length % 2 != 0) {
            return findKthSmallestNumber(array, (array.length - 1) / 2);
        } else {
            float kthSmallest1 = findKthSmallestNumber(array,
                    (array.length) / 2 - 1);
            float kthSmallest2 = findKthSmallestNumber(array,
                    (array.length) / 2);
            return ((kthSmallest1 + kthSmallest2) / 2);
        }
    }

    /**
     * Returns k-th smallest item in an integer array.
     *
     * @param array array which used to search in
     * @param k     number or a smallest item to find
     * @return k-th smallest item
     */
    static float findKthSmallestNumber(final int[] array, final int k) {
        int leftWindowIndex = 0;
        int rightWindowIndex = array.length - 1;
        int i;
        int j;
        int tempValue;
        boolean searchComplete = false;
        while ((leftWindowIndex < rightWindowIndex - 1) && (!searchComplete)) {
            int x = array[k];
            i = leftWindowIndex;
            j = rightWindowIndex;
            do {
                while (array[i] < x) {
                    i++;
                }
                while (array[j] > x) {
                    j--;
                }
                if (i <= j) {
                    tempValue = array[i];
                    array[i] = array[j];
                    array[j] = tempValue;
                    i++;
                    j--;
                }
            } while (i < j);
            if (k > j) {
                leftWindowIndex = i;
            } else if (k < i) {
                rightWindowIndex = j;
            } else {
                searchComplete = true;
            }
        }
        return array[k];
    }

    /**
     * Returns median for a double array.
     *
     * @param array array for calculation
     * @return median for the array
     */
    static double median(final double[] array) {
        if (array.length % 2 != 0) {
            return findKthSmallestNumber(array, (array.length - 1) / 2);
        } else {
            double kthSmallest1 = findKthSmallestNumber(array,
                    (array.length) / 2 - 1);
            double kthSmallest2 = findKthSmallestNumber(array,
                    (array.length) / 2);
            return ((kthSmallest1 + kthSmallest2) / 2);
        }
    }

    /**
     * Returns k-th smallest item in a double array.
     *
     * @param array array which used to search in
     * @param k     number or a smallest item to find
     * @return k-th smallest item
     */
    static double findKthSmallestNumber(final double[] array, final int k) {
        int leftWindowIndex = 0;
        int rightWindowIndex = array.length - 1;
        int i;
        int j;
        double tempValue;
        boolean searchComplete = false;
        while ((leftWindowIndex < rightWindowIndex - 1) && (!searchComplete)) {
            double x = array[k];
            i = leftWindowIndex;
            j = rightWindowIndex;
            do {
                while (array[i] < x) {
                    i++;
                }
                while (array[j] > x) {
                    j--;
                }
                if (i <= j) {
                    tempValue = array[i];
                    array[i] = array[j];
                    array[j] = tempValue;
                    i++;
                    j--;
                }
            } while (i < j);
            if (k > j) {
                leftWindowIndex = i;
            } else if (k < i) {
                rightWindowIndex = j;
            } else {
                searchComplete = true;
            }
        }
        return array[k];
    }

    /**
     * Default constructor (hidden).
     */
    private Median() {
    }
}
