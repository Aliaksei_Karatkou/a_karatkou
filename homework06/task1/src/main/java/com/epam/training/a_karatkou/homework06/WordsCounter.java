package com.epam.training.a_karatkou.homework06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Class to count English words in strings.
 * Different strings can be processed after each other, providing
 * an accumulated result.
 * Class is designed to process а large number of words.
 */
public class WordsCounter {
    /**
     * Prefix to add before word (in "untitled" lines).
     */
    public static final String SPACES_BEFORE_WORDS_WITHOUT_TITLE_LETTER = "   ";

    /**
     * Prefix to add before word (in "titled" lines).
     */
    public static final String SPACES_BEFORE_WORDS_WITH_TITLE_LETTER = " ";

    /**
     * Char which doesn't apply to any English letter.
     */
    private static final char NOT_ENGLISH_LETTER_CHAR = 0;

    /**
     * Regexp to check for words which consist only from English letters.
     */
    private static final String REGEXP_ONLY_ENGLISH_LETTERS = "^[a-zA-Z]+$";

    /**
     * Regexp to split text to words (words' delimiters regexp)
     */
    private static final String REGEXP_NOT_LETTERS = "\\W+";


    /**
     * Hashmap to store word and a number of uses of this word in an input text.
     */
    // HashMap is chosen because we have multiply insert and search
    // operations, and only once we need to order items.
    // Input test can be huge.
    protected HashMap<String, Integer> wordsCounters;

    /**
     * Constructor.
     */
    public WordsCounter() {
        wordsCounters = new HashMap<String, Integer>();
    }

    /**
     * Tests whether a word consists of English letters only.
     *
     * @param testingWord word to test.
     * @return result of the test.
     */
    public final boolean isEnglishWord(final String testingWord) {
        if (testingWord == null) {
            return false;
        }
        return testingWord.matches(REGEXP_ONLY_ENGLISH_LETTERS);
    }

    /**
     * Counts words usage count.
     * Any words which doesn't consist of English words is ignored.
     * The result is accumulates.
     *
     * @param string Text to process.
     */
    public final void countWords(final String string) {
        String regExpNotWords = REGEXP_NOT_LETTERS;
        String[] words = string.toLowerCase().split(regExpNotWords);
        for (String word : words
        ) {
            // we check for words with only english letters
            // after split operation because the source text may include
            // any numbers (like "... since 2019 year") and it
            // must be processed as well, we should ignore only these words,
            // but not whole input text.
            if (isEnglishWord(word)) {
                if (wordsCounters.containsKey(word)) {
                    wordsCounters.put(word, wordsCounters.get(word) + 1);
                } else {
                    wordsCounters.put(word, 1);
                }
            }
        }
    }

    /**
     * Returns a string with the result of word count.
     *
     * @return string with the result of word count
     */
    public final String getWordsCount() {
        StringBuilder resultBuilder = new StringBuilder();
        ArrayList<String> wordsArray =
                new ArrayList<String>(wordsCounters.keySet());

        Collections.sort(wordsArray);
        char lastTitleLetter = NOT_ENGLISH_LETTER_CHAR;
        for (String word : wordsArray
        ) {
            char firstLetter = word.charAt(0);
            if (lastTitleLetter != firstLetter) {
                lastTitleLetter = firstLetter;
                resultBuilder.append(
                        Character.toString(lastTitleLetter).toUpperCase());
                resultBuilder.append(":");
                resultBuilder.append(SPACES_BEFORE_WORDS_WITH_TITLE_LETTER);
            } else {
                resultBuilder.append(SPACES_BEFORE_WORDS_WITHOUT_TITLE_LETTER);
            }
            resultBuilder.append(word);
            resultBuilder.append(" ");
            resultBuilder.append(wordsCounters.get(word));
            resultBuilder.append("\n");
        }
        return resultBuilder.toString();
    }
}
