package com.epam.training.a_karatkou.homework06;

import java.util.Scanner;

/**
 * Class to implement lesson 6 homework.
 */
public final class Task1 {
    /**
     * "main" method of the class.
     *
     * @param args Command line parameters
     */
    public static void main(final String[] args) {
        WordsCounter counter = new WordsCounter();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a text:");
        String inputText = scanner.nextLine();
        counter.countWords(inputText);
        System.out.print(counter.getWordsCount());
    }

    /**
     * Hidden constructor.
     */
    private Task1() {

    }

}
