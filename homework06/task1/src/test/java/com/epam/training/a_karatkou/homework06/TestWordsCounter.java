package com.epam.training.a_karatkou.homework06;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class TestWordsCounter {
    @Test
    public void testIsEnglishWordNull () {
        WordsCounter wordsCounter = new WordsCounter();
        assertFalse(wordsCounter.isEnglishWord(null));
    }
    @Test
    public void testIsEnglishWordEmptyString () {
        WordsCounter wordsCounter = new WordsCounter();
        assertFalse(wordsCounter.isEnglishWord(""));
    }

    @Test
    public void testIsEnglishWordHasNotEnglishLetters () {
        WordsCounter wordsCounter = new WordsCounter();
        assertFalse(wordsCounter.isEnglishWord("sdf4vfv"));
    }

    @Test
    public void testIsEnglishWordHasOnlyEnglishLetters () {
        WordsCounter wordsCounter = new WordsCounter();
        assertTrue(wordsCounter.isEnglishWord("sdfvfv"));
    }

    @Test
    public void testCountWords () {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.countWords("a ab c");
        assertEquals(3, wordsCounter.wordsCounters.size());
        int aCount = wordsCounter.wordsCounters.get("a");
        assertEquals( 1, aCount);
        int abCount = wordsCounter.wordsCounters.get("ab");
        assertEquals( 1, abCount);
        int cCount = wordsCounter.wordsCounters.get("c");
        assertEquals( 1, cCount);
    }

    @Test
    public void testCountWordsUpperCase () {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.countWords("A");
        assertEquals(1, wordsCounter.wordsCounters.size());
        int aCount = wordsCounter.wordsCounters.get("a");
        assertEquals( 1, aCount);
    }

    @Test
    public void testCountWordsWithDoubles () {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.countWords("a a ab c");
        assertEquals(3, wordsCounter.wordsCounters.size());
        int aCount = wordsCounter.wordsCounters.get("a");
        assertEquals( 2, aCount);
        int abCount = wordsCounter.wordsCounters.get("ab");
        assertEquals( 1, abCount);
        int cCount = wordsCounter.wordsCounters.get("c");
        assertEquals( 1, cCount);
    }

    @Test
    public void testCountWordsWithNotEnglishWords () {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.countWords("a 5");
        assertEquals(1, wordsCounter.wordsCounters.size());
        int aCount = wordsCounter.wordsCounters.get("a");
        assertEquals( 1, aCount);
    }

    @Test
    public void testGetWordsCount () {
        WordsCounter wordsCounter = new WordsCounter();
        wordsCounter.wordsCounters.put("c",1);
        wordsCounter.wordsCounters.put("ab",1);
        wordsCounter.wordsCounters.put("a",2);
        String expectedResult = "A: a 2\n" +
                                "   ab 1\n" +
                                "C: c 1\n";

        assertEquals(expectedResult, wordsCounter.getWordsCount());
    }
}
