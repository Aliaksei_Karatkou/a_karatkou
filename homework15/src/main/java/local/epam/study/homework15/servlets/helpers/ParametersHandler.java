package local.epam.study.homework15.servlets.helpers;

import local.epam.study.homework15.servlets.OrderBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class handles request parameters.
 */
public class ParametersHandler {

    /**
     * Gets multiply goods choice items.
     *
     * @param request HttpServletRequest to analyze.
     * @return ArrayList <String> with IDs of whe cosen goods.
     */
    public final ArrayList<String> getRequestedItemsIdString(
            final HttpServletRequest request) {
        String[] paramValues =
                request.getParameterValues(OrderBuilder.PARAMETER_NAME_ITEM_ID);
        if (paramValues != null) {
            return new ArrayList<String>(Arrays.asList(paramValues));
        } else {
            return null;
        }
    }
}

