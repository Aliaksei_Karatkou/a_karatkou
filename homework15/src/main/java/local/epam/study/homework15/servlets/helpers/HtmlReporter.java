package local.epam.study.homework15.servlets.helpers;

import local.epam.study.homework15.goods.Order;

/**
 * Class used to generate different parts of the html for servlets.
 */
public final class HtmlReporter {
    /**
     * Hidden constructor.
     */
    private HtmlReporter() {

    }

    /**
     * Generatates centered table with order data.
     *
     * @param order Order to use
     * @return String with html. If order is null then returns empty string.
     */
    public static String getOrderHtmlTable(final Order order) {
        if (order == null) {
            return "";
        }
        StringBuilder html = new StringBuilder();
        html.append("<table align=\"center\">\n");
        html.append("<th colspan=2>Dear " + order.getClientName() + ", your "
                + "order:</th>\n");
        for (int i = 0; i < order.goodsCount(); i++) {
            html.append("<tr><td align=\"right\">");
            html.append(i + 1 + ")</td><td align=\"left\">"
                    + order.getItem(i).getItemName() + " ");
            html.append(order.getItem(i).getItemCost() + "$</td></tr>\n");
        }
        html.append("<tr><td colspan=2>Total: " + order.getSum()
                + " $</td></tr>");
        html.append("</table>\n");
        return html.toString();
    }
}



