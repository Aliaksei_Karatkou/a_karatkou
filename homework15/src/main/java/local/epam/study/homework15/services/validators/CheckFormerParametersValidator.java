package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;

import java.util.ArrayList;

import static local.epam.study.homework15.services.validators.ValidationHelper.validateClientName;
import static local.epam.study.homework15.services.validators.ValidationHelper.validateRequestedGoods;

/**
 * Validates parameters that must be passed to make an order.
 * (3rd screen of the homework).
 */

public final class CheckFormerParametersValidator {

    /**
     * Hidden constructor.
     */
    private CheckFormerParametersValidator() {

    }

    /**
     * Validates parameters that must be passed to make an order.
     * (3rd screen of the homework).
     *
     * @param clientName   client name
     * @param allowedItems list of the allowed items
     * @param itemIds      list of the requested items.
     * @return true if validation was successful.
     */
    public static boolean validate(final String clientName,
                                   final ArrayList<GoodsItem> allowedItems,
                                   final ArrayList<String> itemIds) {
        return ((validateClientName(clientName))
                && (validateRequestedGoods(allowedItems, itemIds)));
    }
}
