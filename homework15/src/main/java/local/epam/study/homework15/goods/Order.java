package local.epam.study.homework15.goods;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Class to store orders.
 */
public class Order {
    /**
     * Order goods.
     */
    ArrayList<GoodsItem> goods = new ArrayList<>();
    /**
     * Total order cost.
     */
    protected BigDecimal sum = BigDecimal.ZERO;

    /**
     * Client's name.
     */
    protected String clientName;

    /**
     * Returns Sum of all goods in the order.
     *
     * @return Summary costs.
     */
    public final BigDecimal getSum() {
        return sum;
    }

    /**
     * Returns client's name.
     *
     * @return Client's name.
     */
    public final String getClientName() {
        return clientName;
    }

    /**
     * Constructor.
     *
     * @param client Client's name.
     * @throws IllegalArgumentException if client is null or empty, or stores
     *                                  only spaces.
     */
    public Order(final String client) {
        if ((client == null) || (client.trim().isEmpty())) {
            throw new IllegalArgumentException("Bad argument");
        }
        clientName = client;
    }

    /**
     * Adds good item to the order.
     *
     * @param item good to add.
     * @throws IllegalArgumentException if item is null.
     */
    public final void addItem(final GoodsItem item) {
        if (item == null) {
            throw new IllegalArgumentException("Bad argument");
        }
        goods.add(item);
        sum = sum.add(item.getItemCost());
    }

    /**
     * Gets goods item by index.
     *
     * @param index index of the item to return.
     * @return item
     * @throws IndexOutOfBoundsException if index is out of bound.
     */
    public final GoodsItem getItem(final int index) {
        if ((index >= 0) && (index < goodsCount())) {
            return goods.get(index);
        } else {
            throw new IndexOutOfBoundsException("goods index out of range");
        }
    }

    /**
     * Returns order's goods count.
     *
     * @return goods count.
     */
    public final int goodsCount() {
        return goods.size();
    }
}
