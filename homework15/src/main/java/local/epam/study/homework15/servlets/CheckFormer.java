package local.epam.study.homework15.servlets;

import local.epam.study.homework15.goods.GoodsItem;
import local.epam.study.homework15.goods.Order;
import local.epam.study.homework15.services.ItemsService;
import local.epam.study.homework15.services.validators.CheckFormerParametersValidator;
import local.epam.study.homework15.servlets.helpers.ParametersHandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import static local.epam.study.homework15.services.ItemsService.getOrder;
import static local.epam.study.homework15.servlets.Logon.PARAMETER_NAME_CLIENT_NAME;
import static local.epam.study.homework15.servlets.helpers.HtmlReporter.getOrderHtmlTable;

/**
 * Servlet to display order (The 3rd screen of the homework.
 */
public class CheckFormer extends HttpServlet {
    /**
     * Handles POST requests.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse response to sent.
     * @throws IOException if IO error happens.
     */
    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException {
        PrintWriter responseWriter = httpServletResponse.getWriter();


        responseWriter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n");
        String clientName =
                httpServletRequest.getParameter(PARAMETER_NAME_CLIENT_NAME);

        ItemsService itemsService = new ItemsService();
        ArrayList<GoodsItem> allowedItems =
                itemsService.getAllowedItems(getServletContext());
        ParametersHandler parametersHandler = new ParametersHandler();
        ArrayList<String> requeatedGoodsIds =
                parametersHandler.getRequestedItemsIdString(httpServletRequest);
        if (CheckFormerParametersValidator.validate(clientName, allowedItems,
                requeatedGoodsIds)) {
            Order order = getOrder(clientName, allowedItems,
                    requeatedGoodsIds);
            responseWriter.print(getOrderHtmlTable(order));
        } else {
            responseWriter.print("Validation failed<br>");
        }
        responseWriter.print("</body></html>");
    }
}
