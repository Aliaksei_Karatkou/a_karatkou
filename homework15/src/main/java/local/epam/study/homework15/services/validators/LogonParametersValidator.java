package local.epam.study.homework15.services.validators;

/**
 * Validates parameters that must be passed to start order generating.
 * (2nd screen of the homework).
 */
public final class LogonParametersValidator {

    /**
     * Hidden constructor.
     */
    private LogonParametersValidator() {

    }

    /**
     * Validates parameters that must be passed to start order generating.
     * (2nd screen of the homework).
     *
     * @param clientName client name to validate
     * @return true if validation was successful.
     */
    public static boolean validate(final String clientName) {
        return ValidationHelper.validateClientName(clientName);
    }
}
