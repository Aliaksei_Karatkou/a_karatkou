package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;

import java.util.ArrayList;


/**
 * Class for handling validation of different URL-parameters.
 */

public final class ValidationHelper {

    /**
     * Minimal allowed goods list's index.
     */
    private static final int MIN_ALLOWED_GOODS_ID = 0;

    /**
     * hidden constructor.
     */
    private ValidationHelper() {

    }

    /**
     * Validates client name.
     * Must not be null, or empty, or contain only spaces.
     *
     * @param parameterToValidate client name to validate
     * @return true if validation was successful.
     */
    public static boolean validateClientName(final String parameterToValidate) {
        if (parameterToValidate == null) {
            return false;
        }
        if (parameterToValidate.trim().isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Validates required goods.
     * All requested goods' indexes must be in the range
     * [min_index(e.g.0)...allowed_goods_count-1]
     * indexes must be integer numbers.
     *
     * @param allowedGoods      Allowed goods
     * @param requestedGoodsIds Requested good's indexes (strings)
     * @return true if validation was successful.
     */
    public static boolean validateRequestedGoods(
            final ArrayList<GoodsItem> allowedGoods,
            final ArrayList<String> requestedGoodsIds) {

        if ((allowedGoods == null) || (requestedGoodsIds == null)) {
            return false;
        }
        int maxAllowedGoodsId = allowedGoods.size() - 1;
        for (String idReq : requestedGoodsIds) {
            boolean errorOccured = false;
            int id = 0;
            try {
                id = Integer.valueOf(idReq);
            } catch (NumberFormatException ex) {
                errorOccured = true;
            }
            if (!errorOccured) {
                if ((id < MIN_ALLOWED_GOODS_ID) || (id > maxAllowedGoodsId)) {
                    errorOccured = true;
                }
            }
            if (errorOccured) {
                return false;
            }
        }
        return true;
    }


}
