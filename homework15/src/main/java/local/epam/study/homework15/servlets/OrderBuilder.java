package local.epam.study.homework15.servlets;


import local.epam.study.homework15.goods.GoodsItem;
import local.epam.study.homework15.services.ItemsService;
import local.epam.study.homework15.services.validators.LogonParametersValidator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import static local.epam.study.homework15.servlets.Logon.PARAMETER_NAME_CLIENT_NAME;

/**
 * Servlet class to display html page to form the order.
 * (The 2nd screen of the homework).
 */
public class OrderBuilder extends HttpServlet {
    /**
     * URL parameter name to post chosen goods.
     */
    public static final String PARAMETER_NAME_ITEM_ID = "id";

    /**
     * Servlet method POST implementation.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse responce to sent
     * @throws IOException if IO error happens.
     */
    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException {
        PrintWriter responseWriter = httpServletResponse.getWriter();

        responseWriter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n");
        String clientName =
                httpServletRequest.getParameter(PARAMETER_NAME_CLIENT_NAME);
        if (LogonParametersValidator.validate(clientName)) {
            responseWriter.print("Hello, " + clientName + "<p>\n");

            ItemsService itemsService = new ItemsService();
            ArrayList<GoodsItem> items =
                    itemsService.getAllowedItems(getServletContext());
            if (!items.isEmpty()) {
                responseWriter.write("Make your order:<p>");
                responseWriter.write("<form  action=\"Check\" "
                        + "method=\"post\">\n");
                responseWriter.print("<input type=\"hidden\" name=\""
                        + PARAMETER_NAME_CLIENT_NAME
                        + "\" value=\"" + clientName + "\"/>");

                responseWriter.print("<select multiple name=\""
                        + PARAMETER_NAME_ITEM_ID + "\">\n");
                for (int i = 0; i < items.size(); i++) {
                    GoodsItem item = items.get(i);
                    responseWriter.write("<option value=\"" + i + "\">"
                            + item.getItemName() + ", "
                            + item.getItemCost() + " $</option>\n");
                }
                responseWriter.write("</select>\n"
                        + "<br><br><button type=\"submit\">Submit</button>\n"
                        + "</form>");
            } else {
                responseWriter.write("Item list is empty");
            }
        } else {
            responseWriter.print("Bad Client name");
        }
        responseWriter.print("</html>");
    }
}
