package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;

import java.math.BigDecimal;


/**
 * Class for validation startup parameters.
 */

public final class StartUpParametersValidator {
    /**
     * RegExp for names of init parameters with goods.
     */
    private static final String INIT_PARAMS_GOODS_PARAM_NAME_REGEXP =
            "^good\\d+$";

    /**
     * Regexp used to split cost and name of goods.
     */
    private static final String INIT_PARAM_GOODS_PARAM_VALUES_DELIMITER = "\\|";

    /**
     * Hidden constructor.
     */
    private StartUpParametersValidator() {

    }

    /**
     * Check if parameter from web.xml if goods info.
     * If it is true, returns GoodItem object with reqired info.
     *
     * @param parameterName  pamameter name to parse
     * @param parameterValue parameter value to parse
     * @return GoodItem object if success, otherwise null.
     */
    public static GoodsItem startUpGoodsValidator(final String parameterName,
                                                  final String parameterValue) {
        if ((parameterName == null) || (parameterValue == null)) {
            return null;
        }

        if (parameterName.matches(INIT_PARAMS_GOODS_PARAM_NAME_REGEXP)) {
            String[] splittedValues = parameterValue.split(
                    INIT_PARAM_GOODS_PARAM_VALUES_DELIMITER, 2);
            if ( (splittedValues.length != 2) || (splittedValues[0].isEmpty())
                    || (splittedValues[1].isEmpty())) {
                return null;
            }
            BigDecimal cost;
            if (splittedValues.length == 2) {
                try {
                    cost = new BigDecimal(splittedValues[0]);
                } catch (NumberFormatException ex) {
                    cost = null;
                }
                if (cost != null) {
                    return new GoodsItem(splittedValues[1], cost);
                }
            }
        }
        return null;
    }
}
