package local.epam.study.homework15.services;

import local.epam.study.homework15.goods.GoodsItem;
import local.epam.study.homework15.goods.Order;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.Enumeration;

import static local.epam.study.homework15.services.validators.StartUpParametersValidator.startUpGoodsValidator;

/**
 * Service class to fill available goods list.
 */
public final class ItemsService {


    /**
     * Read goods from web.xml.
     *
     * @param servletContext context to use
     * @return Good's items (ArrayList).
     */
    public ArrayList<GoodsItem> getAllowedItems(
            final ServletContext servletContext) {
        // About ServletContextListeners and attributes
        // where talked about in next Lesson (16).
        // I didn't know about them.... :-(
        // It could be a great solution...
        ArrayList<GoodsItem> items = new ArrayList<>();
        Enumeration paramNames = servletContext.getInitParameterNames();
        while (paramNames.hasMoreElements()) {
            String initParamName = (String) paramNames.nextElement();
            String initParamValue =
                    servletContext.getInitParameter(initParamName);
            GoodsItem newGoodsItem = startUpGoodsValidator(initParamName,
                    initParamValue);
            if (newGoodsItem != null) {
                items.add(newGoodsItem);
            }
        }
        return items;
    }

    /**
     * Return Order using allowed goods and chosen good's indexes.
     *
     * @param clientName   Client name to use in Order.
     * @param allowedItems List of available goods.
     * @param checkedIds   List with indexes of goods, which must be in the
     *                     order.
     * @return Order object.
     * @throws IllegalArgumentException if any parameter is null
     */
    public static Order getOrder(final String clientName,
                                 final ArrayList<GoodsItem> allowedItems,
                                 final ArrayList<String> checkedIds) {
        if ((clientName == null) || (allowedItems == null)
                || (checkedIds == null)) {
            throw new IllegalArgumentException("Parameter error");
        }
        Order order = new Order(clientName);
        for (String idReq : checkedIds) {
            GoodsItem item = allowedItems.get(Integer.valueOf(idReq));
            order.addItem(item);
        }
        return order;
    }
}
