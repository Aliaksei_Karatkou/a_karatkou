package local.epam.study.homework15.servlets;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet class to display "Logon" html page (The 1st screen of the homework).
 */
public class Logon extends HttpServlet {
    /**
     * Parameter name to post client name.
     */
    public static final String PARAMETER_NAME_CLIENT_NAME = "name";


    /**
     * Servlet method POST implementation.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse responce to sent
     * @throws IOException if IO error happens.
     */
    @Override
    public final void doGet(final HttpServletRequest httpServletRequest,
                            final HttpServletResponse httpServletResponse)
            throws IOException {
        PrintWriter responseWrter = httpServletResponse.getWriter();
        responseWrter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n"
                + "\n"
                + " <h1>Welcome to Online Shop</h1><br/>\n"
                + "         <form action=\"Order\" method=\"post\">\n"
                + "         <input name=\"" + PARAMETER_NAME_CLIENT_NAME
                + "\"><p>\n"
                + "         <button type=\"submit\">Enter</button>\n"
                + "         </form>\n"
                + "         </body>\n"
                + "</html>");
    }
}
