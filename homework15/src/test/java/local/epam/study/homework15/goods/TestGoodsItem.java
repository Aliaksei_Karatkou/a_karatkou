package local.epam.study.homework15.goods;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;


public class TestGoodsItem {
    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorNameIsNull() {
        new GoodsItem(null, BigDecimal.ONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorCostIsNull() {
        new GoodsItem("name", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorNameIsEmpty() {
        new GoodsItem("  ", BigDecimal.ONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorCostIsNegative() {
        new GoodsItem("name", new BigDecimal(-1));
    }

    // ------ getters
    @Test
    public void TestGetItemName() {
        GoodsItem item = new GoodsItem("name", BigDecimal.ONE);
        assertEquals("name", item.itemName);
        assertEquals("name", item.getItemName());
    }

    @Test
    public void TestGetItemCost() {
        GoodsItem item = new GoodsItem("name", BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, item.itemCost);
        assertEquals(BigDecimal.ONE, item.getItemCost());
    }

}
