package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static local.epam.study.homework15.services.validators.CheckFormerParametersValidator.validate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestCheckFormerParametersValidator {
    @Test
    public void TestValidateClientNameNameIsNull() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("0");

        assertFalse(validate(null, allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateClientNameNameIsEmpty() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("0");
        assertFalse(validate("  ", allowedGoods, requestedGoods));
    }


    @Test
    public void TestValidateRequestedGoodsAllowedGoodsIsNull() {
        assertFalse(validate("name", null, new ArrayList<>()));
    }

    @Test
    public void TestValidateRequestedRequestedGoodsIsNull() {
        assertFalse(validate("name", new ArrayList<>(), null));
    }

    @Test
    public void TestValidateRequestedNormal() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("0");
        assertTrue(validate("name", allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNotNumberInRequestedList() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("a");
        assertFalse(validate("name", allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNumberInRequestedListIsTooSmall() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("-1");
        assertFalse(validate("name", allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNumberInRequestedListIsTooBif() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("1");
        assertFalse(validate("name", allowedGoods, requestedGoods));
    }

}
