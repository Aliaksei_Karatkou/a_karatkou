package local.epam.study.homework15.services;

import local.epam.study.homework15.goods.GoodsItem;
import local.epam.study.homework15.goods.Order;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestItemsService {


    @Test(expected = IllegalArgumentException.class)
    public void TestGetOrderNullParam1() {
        ItemsService.getOrder(null, new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestGetOrderNullParam2() {
        ItemsService.getOrder("cl", null, new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestGetOrderNullParam3() {
        ItemsService.getOrder("cl", new ArrayList<>(), null);
    }

    @Test
    public void TestGetOrderNormal() {
        ArrayList<GoodsItem> allowed = new ArrayList<>();
        ArrayList<String> requested = new ArrayList<>();
        GoodsItem item1 = new GoodsItem("1", BigDecimal.ONE);
        GoodsItem item2 = new GoodsItem("2", BigDecimal.TEN);
        allowed.add(item1);
        allowed.add(item2);
        requested.add("1");
        Order order = ItemsService.getOrder("cl", allowed, requested);
        assertEquals(1, order.goodsCount());
        assertEquals(item2, order.getItem(0));

    }
}
