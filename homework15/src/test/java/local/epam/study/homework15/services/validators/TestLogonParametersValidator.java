package local.epam.study.homework15.services.validators;

import org.junit.Test;

import static local.epam.study.homework15.services.validators.LogonParametersValidator.validate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestLogonParametersValidator {
    @Test
    public void TestValidateClientNameNameIsNull() {
        assertFalse(validate(null));
    }

    @Test
    public void TestValidateClientNameNameIsEmpty() {
        assertFalse(validate("  "));
    }

    @Test
    public void TestValidateClientNameNormal() {
        assertTrue(validate("Normal name"));
    }
}
