package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static local.epam.study.homework15.services.validators.ValidationHelper.validateClientName;
import static local.epam.study.homework15.services.validators.ValidationHelper.validateRequestedGoods;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestValidationHelper {
    @Test
    public void TestValidateClientNameNameIsNull() {
        assertFalse(validateClientName(null));
    }

    @Test
    public void TestValidateClientNameNameIsEmpty() {
        assertFalse(validateClientName("  "));
    }

    @Test
    public void TestValidateClientNameNormal() {
        assertTrue(validateClientName("Normal name"));
    }

    // ------------
    @Test
    public void TestValidateRequestedGoodsAllowedGoodsIsNull() {
        assertFalse(validateRequestedGoods(null, new ArrayList<>()));
    }

    @Test
    public void TestValidateRequestedRequestedGoodsIsNull() {
        assertFalse(validateRequestedGoods(new ArrayList<>(), null));
    }

    @Test
    public void TestValidateRequestedNormal() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("0");
        assertTrue(validateRequestedGoods(allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNotNumberInRequestedList() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("a");
        assertFalse(validateRequestedGoods(allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNumberInRequestedListIsTooSmall() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("-1");
        assertFalse(validateRequestedGoods(allowedGoods, requestedGoods));
    }

    @Test
    public void TestValidateRequestedNumberInRequestedListIsTooBif() {
        ArrayList<GoodsItem> allowedGoods = new ArrayList<>();
        ArrayList<String> requestedGoods = new ArrayList<>();
        allowedGoods.add(new GoodsItem("item", BigDecimal.ONE));
        requestedGoods.add("1");
        assertFalse(validateRequestedGoods(allowedGoods, requestedGoods));
    }


}
