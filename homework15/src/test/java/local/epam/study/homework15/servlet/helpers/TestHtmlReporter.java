package local.epam.study.homework15.servlet.helpers;

import local.epam.study.homework15.goods.GoodsItem;
import local.epam.study.homework15.goods.Order;
import org.junit.Test;

import java.math.BigDecimal;

import static local.epam.study.homework15.servlets.helpers.HtmlReporter.getOrderHtmlTable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestHtmlReporter {
    @Test
    public void testGetOrderHtmlTableParameterNull() {
        assertTrue(getOrderHtmlTable(null).isEmpty());
    }

    @Test
    public void testGetOrderHtmlTableNormal() {
        Order order = new Order("client");
        order.addItem(new GoodsItem("item1", BigDecimal.ONE));
        order.addItem(new GoodsItem("item2", BigDecimal.TEN));
        String expectedString = "<table align=\"center\">\n" +
                "<th colspan=2>Dear client, your order:</th>\n" +
                "<tr><td align=\"right\">1)</td><td align=\"left\">item1 " +
                "1$</td></tr>\n" +
                "<tr><td align=\"right\">2)</td><td align=\"left\">item2 " +
                "10$</td></tr>\n" +
                "<tr><td colspan=2>Total: 11 $</td></tr></table>\n";
        String actualString = getOrderHtmlTable(order);
        assertEquals(expectedString, actualString);

    }
}
