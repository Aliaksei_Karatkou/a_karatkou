package local.epam.study.homework15.services.validators;

import local.epam.study.homework15.goods.GoodsItem;
import org.junit.Test;

import java.math.BigDecimal;

import static local.epam.study.homework15.services.validators.StartUpParametersValidator.startUpGoodsValidator;
import static org.junit.Assert.*;

public class TestStartUpParametersValidator {
    @Test
    public void TestStartUpGoodsValidatorParam1IsNull() {
        assertNull(startUpGoodsValidator(null, "df"));
    }

    @Test
    public void TestStartUpGoodsValidatorParam2IsNull() {
        assertNull(startUpGoodsValidator("sdf", null));
    }

    @Test
    public void TestStartUpGoodsValidatorWrongParamName() {
        assertNull(startUpGoodsValidator("sdfsdf", "4|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorWrongCost() {
        assertNull(startUpGoodsValidator("good3", "4r|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorNoCost() {
        assertNull(startUpGoodsValidator("good3", "|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorNoName() {
        assertNull(startUpGoodsValidator("good3", "4|"));
    }

    @Test
    public void TestStartUpGoodsValidatorNormal() {
        GoodsItem item = startUpGoodsValidator("good3", "10|Some item");
        assertNotNull(item);
        assertEquals(BigDecimal.TEN, item.getItemCost());
        assertEquals("Some item", item.getItemName());


    }
}
