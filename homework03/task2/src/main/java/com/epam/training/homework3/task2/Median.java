package com.epam.training.homework3.task2;

import java.util.Arrays;

/**
 * Class Median finds a median for an array.
 * of integer of double values
 */
public final class Median {
    /**
     * Finds median for the array of integer values.
     *
     * @param intArray An array for which to find the median
     * @return median value
     */
    public static float median(final int[] intArray) {
        Arrays.sort(intArray);
        if (intArray.length % 2 != 0) {
            int middleItemIndex = (intArray.length) / 2;
            return intArray[middleItemIndex];
        } else {
            int highIndex = intArray.length / 2;
            int loIndex = highIndex - 1;
            return (((float) intArray[highIndex] + intArray[loIndex]) / 2);
        }
    }

    /**
     * Finds median for the array of double values.
     *
     * @param doubleArray An array for which to find the median
     * @return median value
     */
    public static double median(final double[] doubleArray) {
        Arrays.sort(doubleArray);
        if (doubleArray.length % 2 != 0) {
            int middleItemIndex = (doubleArray.length - 1) / 2;
            return doubleArray[middleItemIndex];
        } else {
            int highIndex = doubleArray.length / 2;
            int loIndex = highIndex - 1;
            return ((doubleArray[highIndex] + doubleArray[loIndex]) / 2);
        }
    }

    /**
     * Default constructor (hidden).
     */
    private Median() {

    }

}
