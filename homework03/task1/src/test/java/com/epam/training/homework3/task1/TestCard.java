package com.epam.training.homework3.task1;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TestCard {
    // getOwner
    @Test
    public void testCardGetOwnerName() {
        Card card = new Card("test Owner");
        assertEquals("getOwner works wrong", "test Owner", card.getOwner());
    }

    // getBalance
    @Test
    public void testCardGetBalance() {
        Card card = new Card("test Owner");
        card.balance = BigDecimal.TEN;
        assertEquals("getBalance works wrong", BigDecimal.TEN, card.getBalance());
    }

    //  Constructors
    @Test
    public void testCardConstructorByOwner() {
        Card card = new Card("test Owner");
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", card.getOwner());
        assertEquals("Constructor works wrong: balance not equal to zero", new BigDecimal(Card.DEFAULT_BALANCE), card.getBalance());
    }

    @Test
    public void testCardConstructorByHolderAndBalancePositive() {
        Card card = new Card("test Owner", new BigDecimal("10"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", card.getOwner());
        assertEquals("Constructor works wrong: balance differs from expected", BigDecimal.TEN, card.getBalance());
    }

    @Test
    public void testCardConstructorByHolderAndBalanceNegative() {
        Card card = new Card("test Owner", new BigDecimal("-100.5"));
        assertEquals("Constructor works wrong: owner name differs from expected", "test Owner", card.getOwner());
        BigDecimal expectedBalanceValue = new BigDecimal(Card.DEFAULT_BALANCE);
        assertEquals("Result balance differs from expected", expectedBalanceValue, card.getBalance());
    }

    @Test
    public void testCardConstructorByOwnerAndBalanceNulls() {
        Card card = new Card(null, null);
        assertEquals("Constructor works wrong: owner name differs from expected", Card.DEFAULT_CARD_OWNER_NAME, card.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(Card.DEFAULT_BALANCE), card.getBalance());
    }

    @Test
    public void testCardConstructorByOwnerWithNull() {
        Card card = new Card(null);
        assertEquals("Constructor works wrong: owner name differs from expected", Card.DEFAULT_CARD_OWNER_NAME, card.getOwner());
        assertEquals("Result balance differs from expected", new BigDecimal(Card.DEFAULT_BALANCE), card.getBalance());
    }

    // add balance
    @Test
    public void testCardAddBalancePositive() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = card.addToBalance(new BigDecimal("45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(58.6);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertTrue("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNegative() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = card.addToBalance(new BigDecimal("-45.6"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    @Test
    public void testCardAddBalanceNullReference() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualAddToBalanceResult = card.addToBalance(null);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertFalse("method returned wrong result", actualAddToBalanceResult);
    }

    // withdrowFromBalance
    @Test
    public void testCardTakeFromBalancePositiveValue() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = card.withdrowFromBalance(new BigDecimal("80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(-67);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertTrue("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceNegativeValue() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = card.withdrowFromBalance(new BigDecimal("-80"));
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    @Test
    public void testCardTakeFromBalanceZeroValue() {
        Card card = new Card("test Owner", new BigDecimal("13"));
        boolean actualWithdrowResult = card.withdrowFromBalance(BigDecimal.ZERO);
        BigDecimal expectedCardBalance = BigDecimal.valueOf(13);
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Result balance differs from expected", expectedCardBalance, card.getBalance());
        assertFalse("method returned wrong result", actualWithdrowResult);
    }

    // Another currency
    @Test
    public void testCardAnotherCurrencyRateIsOne() {
        Card card = new Card("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = card.getBalance(BigDecimal.ONE,2);
        String expectedCardBalance = "20.00";
        assertEquals("Owner name differs from expected after runnig the method", "test Owner", card.getOwner());
        assertEquals("Converted balance differs from expected", expectedCardBalance, anotherCurrencyValue);
        assertEquals("Result balance differs from expected", new BigDecimal("20"), card.getBalance());
    }

    @Test
    public void testCardAnotherCurrencyRateIsOneAndNegativeScale() {
        Card card = new Card("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = card.getBalance(BigDecimal.ONE,-1);
        String expectedCardBalance = "20.00";
        assertEquals("Another currency balance differs from expected", expectedCardBalance, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyZeroRate() {
        Card card = new Card("test Owner", new BigDecimal("20"));
        String anotherCurrencyValue = card.getBalance(BigDecimal.ZERO,2);
        assertEquals("Conversion to another currency successed, but must fail", Card.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencNegativeRate() {
        Card card = new Card("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(-1);
        String anotherCurrencyValue = card.getBalance(ratioRate,2);
        assertEquals("Conversion to another currency successed, but must fail", Card.MESSAGE_CONVERSION_ERROR, anotherCurrencyValue);
    }

    @Test
    public void testCardAnotherCurrencyRateHalf() {
        Card card = new Card("test Owner", new BigDecimal("20"));
        BigDecimal ratioRate = BigDecimal.valueOf(0.5);
        String anotherCurrencyValue = card.getBalance(ratioRate,2);
        String ExpectedCardBalance = "40.00";
        assertEquals("Another currency balance differs from expected", ExpectedCardBalance, anotherCurrencyValue);
    }

    // display balance
    @Test
    public void testDisplayBalanceScalePositive() {
        Card card = new Card("test Owner", new BigDecimal("20.1234"));
        String actualText = card.displayBalance(2);
        String expectedText = "20.12";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    @Test
    public void testDisplayBalanceScaleNegative() {
        Card card = new Card("test Owner", new BigDecimal("20.1234"));
        String actualText = card.displayBalance(-2);
        String expectedText = "20";
        assertEquals("Balance text is wrong", expectedText, actualText);
    }

    // isBlankString
    @Test
    public void testIsBlankStringNormalString() {
        Card card = new Card("test Owner");
        assertFalse("isBlankString failed for normal string",
                card.isBlankString("abc"));
    }

    @Test
    public void testIsBlankStringNull() {
        Card card = new Card("test Owner");
        assertTrue("isBlankString failed for null-pointer",
                card.isBlankString(null));
    }

    @Test
    public void testIsBlankStringSpacesAndTabs() {
        Card card = new Card("test Owner");
        assertTrue("isBlankString failed for string with spaces and tabs",
                 card.isBlankString("  \t \t"));
    }

    @Test
    public void testIsBlankStringEmpty() {
        Card card = new Card("test Owner");
        assertTrue("isBlankString failed for empty",
                 card.isBlankString(""));
    }
}
