package local.epam.study.homework16.servlets;

import local.epam.study.homework16.goods.GoodsInfoStorage;
import local.epam.study.homework16.goods.GoodsItem;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.math.BigDecimal;
import java.util.Enumeration;

import static local.epam.study.homework16.servlets.Logon.SERVLETS_GOODS_PROVIDER_ATTRIBUTE_NAME;


/**
 * Class used to load an information about allowed items.
 */

public class ContextListener implements ServletContextListener {
    /**
     * RegExp for names of init parameters with goods.
     */
    private static final String INIT_PARAMS_GOODS_PARAM_NAME_REGEXP =
            "^good\\d+$";

    /**
     * Regexp used to split cost and name of goods.
     */
    private static final String INIT_PARAM_GOODS_PARAM_VALUES_DELIMITER = "\\|";



    /**
     * Check if parameter from web.xml is goods info.
     * If it is true, returns GoodItem object with reqired info.
     *
     * @param parameterName  pamameter name to parse
     * @param parameterValue parameter value to parse
     * @return GoodItem object if success, otherwise null.
     */
    public static GoodsItem startUpGoodsValidator(final String parameterName,
                                                 final String parameterValue) {
        if ((parameterName == null) || (parameterValue == null)) {
            return null;
        }

        if (parameterName.matches(INIT_PARAMS_GOODS_PARAM_NAME_REGEXP)) {
            String[] splittedValues = parameterValue.split(
                    INIT_PARAM_GOODS_PARAM_VALUES_DELIMITER, 2);
            if ((splittedValues.length != 2) || (splittedValues[0].isEmpty())
                    || (splittedValues[1].isEmpty())) {
                return null;
            }
            BigDecimal cost;
            if (splittedValues.length == 2) {
                try {
                    cost = new BigDecimal(splittedValues[0]);
                } catch (NumberFormatException ex) {
                    cost = null;
                }
                if (cost != null) {
                    return new GoodsItem(splittedValues[1], cost);
                }
            }
        }
        return null;
    }

    /**
     * Loads servlet context init parameters with values.
     * Then pass this information for analyzing and processing.
     *
     * @param event servlet initialization event.
     */
    public  void contextInitialized( ServletContextEvent event) {
        ServletContext sc = event.getServletContext();
        Enumeration paramNames = sc.getInitParameterNames();
        GoodsInfoStorage goodsInfo = new GoodsInfoStorage();

        while (paramNames.hasMoreElements()) {
            String initParamName = (String) paramNames.nextElement();
            String initParamValue =
                    sc.getInitParameter(initParamName);
            GoodsItem newGoodsItem = startUpGoodsValidator(initParamName,
                    initParamValue);
            if (newGoodsItem != null) {
                goodsInfo.addItem(newGoodsItem);
            }
        }
        sc.setAttribute(SERVLETS_GOODS_PROVIDER_ATTRIBUTE_NAME, goodsInfo);

    }


    /**
     * Empty method.
     *
     * @param event doesn't matter.
     */
    public void contextDestroyed(final ServletContextEvent event) {
// nothing to do here
    }


}
