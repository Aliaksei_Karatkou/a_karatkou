package local.epam.study.homework16.services.validators;

import local.epam.study.homework16.goods.GoodsInfoStorage;


/**
 * Class for handling validation of different URL-parameters.
 */

public final class ValidationHelper {

    /**
     * Minimal allowed goods list's index.
     */
    private static final int MIN_ALLOWED_GOODS_ID = 0;

    /**
     * hidden constructor.
     */
    private ValidationHelper() {

    }

    /**
     * Validates client name.
     * Must not be null, or empty, or contain only spaces.
     *
     * @param parameterToValidate client name to validate
     * @return true if validation was successful.
     */
    public static boolean validateClientName(final String parameterToValidate) {
        if (parameterToValidate == null) {
            return false;
        }
        if (parameterToValidate.trim().isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Validates requested good's id.
     *
     * @param allowedGoodsInfoProvider Allowed goods information storage.
     * @param requestedGoodId          requested good's id.
     * @return true if validation successfully completed
     */

    public static boolean validateRequestedGoodItem(
            final GoodsInfoStorage allowedGoodsInfoProvider,
            final Integer requestedGoodId) {

        if ((allowedGoodsInfoProvider == null) || (requestedGoodId == null)) {
            return false;
        }
        int maxAllowedGoodsId = allowedGoodsInfoProvider.getItemsCount() - 1;

        return (!((requestedGoodId < MIN_ALLOWED_GOODS_ID)
                || (requestedGoodId > maxAllowedGoodsId)));
    }

}
