package local.epam.study.homework16.services.validators;

import local.epam.study.homework16.goods.GoodsInfoStorage;

import static local.epam.study.homework16.services.validators.ValidationHelper.validateClientName;

/**
 * Validates parameters that must be passed to start order generating.
 * (2nd screen of the homework).
 */
public final class LogonParametersValidator {

    /**
     * Hidden constructor.
     */
    private LogonParametersValidator() {

    }

    /**
     * Validates client name and new item id.
     * Used for parameters, passed to form-order page.
     *
     * @param clientName client name to validate
     * @param goodsInfoStorage items storage.
     * @param itemId item id to check
     * @return true if validation was successful.
     */
    public static boolean validateNewGoodAdding(
            final String clientName,
            final GoodsInfoStorage goodsInfoStorage,
            final Integer itemId) {
        boolean result;
        result = validateClientName(clientName);
        if (result && (itemId != null)) {
            result =
                    ValidationHelper.validateRequestedGoodItem(goodsInfoStorage,
                            itemId);
        }
        return result;
    }
}
