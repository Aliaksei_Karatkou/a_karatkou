package local.epam.study.homework16.servlets;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static local.epam.study.homework16.servlets.ShopFilter.ARGEEMENT_PARAMETER_OR_ATTRIBUTE_NAME;
import static local.epam.study.homework16.servlets.ShopFilter.ARGEEMENT_VALUE_REQ;

/**
 * Servlet class to display "Logon" html page (The 1st screen of the homework).
 */
public class Logon extends HttpServlet {
    /**
     * Parameter name to post client name.
     */
    public static final String PARAMETER_NAME_CLIENT_NAME = "name";

    /**
     * An agreement message.
     */

    public static final String AGREEMENT_TITLE = "I agree with the terms of "
            + "service";


    /**
     * Servlet attribute name to hold allowed goods information.
     */
    public static final String SERVLETS_GOODS_PROVIDER_ATTRIBUTE_NAME =
            "GoodsProvider";

    /**
     * Session attibute to hold forming order data.
     */
    public static final String SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME =
            "CurrentOrder";


    /**
     * Servlet method POST implementation.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse responce to sent
     * @throws IOException if IO error happens.
     */

    @Override
    public final void doGet(final HttpServletRequest httpServletRequest,
                            final HttpServletResponse httpServletResponse)
            throws IOException {
        PrintWriter responseWrter = httpServletResponse.getWriter();
        responseWrter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n"
                + "\n"
                + " <h1>Welcome to Online Shop</h1><br/>\n"
                + "         <form action=\"Order\" method=\"post\">\n"
                + "         <input name=\"" + PARAMETER_NAME_CLIENT_NAME
                + "\"><p>\n"
                + "<input type=\"checkbox\" name=\""
                + ARGEEMENT_PARAMETER_OR_ATTRIBUTE_NAME + "\" value=\""
                + ARGEEMENT_VALUE_REQ + "\""
                + " >" + AGREEMENT_TITLE + "<br>"
                + "         <button type=\"submit\">Enter</button>\n"
                + "         </form>\n"
                + "         </body>\n"
                + "</html>");
    }
}
