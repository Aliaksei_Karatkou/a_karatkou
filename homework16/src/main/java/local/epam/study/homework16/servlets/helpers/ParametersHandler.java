package local.epam.study.homework16.servlets.helpers;

import javax.servlet.http.HttpServletRequest;

import static local.epam.study.homework16.servlets.Logon.PARAMETER_NAME_CLIENT_NAME;
import static local.epam.study.homework16.servlets.OrderBuilder.PARAMETER_NAME_NEW_ITEM_ID;

/**
 * Class handles request parameters.
 */
public class ParametersHandler {

    /**
     * request used to run methods.
     *
     */
    private HttpServletRequest request;

    /**
     * Constructor.
     * @param httpServletRequest request to use after object creation.
     */
    public ParametersHandler(final HttpServletRequest httpServletRequest) {
        if (httpServletRequest == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        request = httpServletRequest;
    }

    /**
     * Convers String to Integer.
     * @param value Integer value
     * @return null if parameter is null or conversion error happens.
     */
    public final Integer stringToInteger(final String value) {
        Integer result;
        if (value == null) {
            return null;
        }
        try {
            result = Integer.valueOf(value);
        } catch (NumberFormatException ex) {
            result = null;
        }
        return result;
    }

    /**
     * Get client name (from request parameters).
     * @return client name.
     */
    public final String getClientName() {
        return request.getParameter(PARAMETER_NAME_CLIENT_NAME);
    }


    /**
     * get requested good id.
     * @return good's id, or null if any error happens.
     */
    public final Integer getNewRequestedItemId() {
        String goodIdStr = request.getParameter(PARAMETER_NAME_NEW_ITEM_ID);
        Integer id = stringToInteger(goodIdStr);
        return id;
    }

}

