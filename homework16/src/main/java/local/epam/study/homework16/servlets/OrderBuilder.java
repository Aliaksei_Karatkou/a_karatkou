package local.epam.study.homework16.servlets;


import local.epam.study.homework16.goods.GoodsInfoStorage;
import local.epam.study.homework16.goods.GoodsItem;
import local.epam.study.homework16.goods.Order;
import local.epam.study.homework16.goods.OrderHandler;
import local.epam.study.homework16.services.validators.LogonParametersValidator;
import local.epam.study.homework16.servlets.helpers.ParametersHandler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import static local.epam.study.homework16.servlets.Logon.SERVLETS_GOODS_PROVIDER_ATTRIBUTE_NAME;
import static local.epam.study.homework16.servlets.Logon.SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME;
import static local.epam.study.homework16.servlets.Logon.PARAMETER_NAME_CLIENT_NAME;
import static local.epam.study.homework16.servlets.helpers.HtmlReporter.getOrderHtmlTable;

/**
 * Servlet class to display html page to form the order.
 * (The 2nd screen of the homework).
 */
public class OrderBuilder extends HttpServlet {
    /**
     * URL parameter name to post chosen goods.
     */
    public static final String PARAMETER_NAME_NEW_ITEM_ID = "newid";

    /**
     * Page name to form order.
     */
    public static final String HTML_PAGE_NAME_ORDER_BUILDER = "Order";

    /**
     * Generates html code for drop-down menu with allowed items.
     *
     * @param clientName client name.
     * @param goods      allowed goods storage.
     * @return html text.
     */

    protected final String choiceDropdownMenuHtmlBody(
            final String clientName,
            final GoodsInfoStorage goods) {
        StringBuilder html = new StringBuilder();
        html.append("<form  action=\"Check\" "
                + "method=\"post\">\n");
        html.append("<input type=\"hidden\" name=\""
                + PARAMETER_NAME_CLIENT_NAME
                + "\" value=\"" + clientName + "\"/>");

        html.append("<select name=\""
                + PARAMETER_NAME_NEW_ITEM_ID + "\">\n");

        for (int i = 0; i < goods.getItemsCount(); i++) {
            GoodsItem item = goods.getItemByIndex(i);
            html.append("<option value=\"" + i + "\">"
                    + item.getItemName() + ", "
                    + item.getItemCost() + " $</option>\n");
        }
        return html.toString();
    }

    /**
     * returns html for items that were prepared to order.
     *
     * @param goods allowed goods information storage.
     * @param order forming order object.
     * @return html
     * @throws IllegalArgumentException if any parameter is null.
     */
    protected final String getChosenItemsHtmlBody(final GoodsInfoStorage goods,
                                                  final OrderHandler order) {
        if ((goods == null) || (order == null)) {
            throw new IllegalArgumentException("Parameter is null");
        }
        return getOrderHtmlTable(order, false);
    }


    /**
     * Servlet method POST implementation.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse responce to sent
     * @throws IOException if IO error happens.
     */


    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException {

        PrintWriter responseWriter = httpServletResponse.getWriter();
        HttpSession session = httpServletRequest.getSession();

        ServletContext servletContext = getServletContext();

        GoodsInfoStorage goodsInfoProvider = (GoodsInfoStorage)
                servletContext.getAttribute(
                        SERVLETS_GOODS_PROVIDER_ATTRIBUTE_NAME);


        responseWriter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n");
        ParametersHandler parametersHandler =
                new ParametersHandler(httpServletRequest);
        OrderHandler order =
                (OrderHandler) session.getAttribute(
                        SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME);

        String clientName = parametersHandler.getClientName();
        if (order != null) {
            clientName = order.getClientName();
        }

        Integer newGoodId = parametersHandler.getNewRequestedItemId();

        if (LogonParametersValidator.validateNewGoodAdding(clientName,
                goodsInfoProvider,
                newGoodId)) {
            if (order == null) {
                order = new Order(clientName);
                session.setAttribute(SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME,
                        order);
            }
            if (newGoodId != null) {
                order.addItem(goodsInfoProvider.getItemByIndex(newGoodId));
            }
            responseWriter.print("Hello, " + clientName + "<p>\n");


            if (goodsInfoProvider.getItemsCount() != 0) {
                responseWriter.write(getChosenItemsHtmlBody(goodsInfoProvider,
                        order));
                responseWriter.write(choiceDropdownMenuHtmlBody(clientName,
                        goodsInfoProvider));
                responseWriter.write("</select>\n"
                        + "<br><br>\n"
                        + "<button type=\"submit\" formaction=\""
                        + HTML_PAGE_NAME_ORDER_BUILDER
                        + "\">Add Item</button>\n"
                        + "<button type=\"submit\">Submit</button>\n"
                        + "</form>");
            } else {
                responseWriter.write("Item list is empty");
            }
        } else {
            responseWriter.print("Bad data");
        }
        responseWriter.print("</html>");
    }


}
