package local.epam.study.homework16.servlets.helpers;

import local.epam.study.homework16.goods.OrderHandler;

/**
 * Class used to generate different parts of the html for servlets.
 */
public final class HtmlReporter {
    /**
     * Hidden constructor.
     */
    private HtmlReporter() {

    }

    /**
     * Generatates centered table with order data.
     *
     * @param order Order to use
     * @param isOrderComplete true if order completed.
     * @return String with html. If order is null then returns empty string.
     */
    public static String getOrderHtmlTable(final OrderHandler order,
                                           final boolean isOrderComplete) {
        if (order == null) {
            return "";
        }
        StringBuilder html = new StringBuilder();
        html.append("<table align=\"center\">\n");

        String header;
        if (isOrderComplete) {
            header = "Dear " + order.getClientName() + ", your order:";
        } else {
            if (order.goodsCount() == 0) {
                header = "Make your order:";
            } else {
                header = "You have already chosen:";
            }
        }

        html.append("<th colspan=2>" + header + "</th>\n");
        for (int i = 0; i < order.goodsCount(); i++) {
            html.append("<tr><td align=\"right\">");
            html.append(i + 1 + ")</td><td align=\"left\">"
                    + order.getItem(i).getItemName() + " ");
            html.append(order.getItem(i).getItemCost() + "$</td></tr>\n");
        }
        if (isOrderComplete) {
            html.append("<tr><td colspan=2>Total: " + order.getSum()
                    + " $</td></tr>");
        }
        html.append("</table>\n");
        return html.toString();
    }
}



