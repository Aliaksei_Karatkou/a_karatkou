package local.epam.study.homework16.servlets;

import local.epam.study.homework16.goods.Order;
import local.epam.study.homework16.goods.OrderHandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static local.epam.study.homework16.servlets.Logon.SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME;
import static local.epam.study.homework16.servlets.helpers.HtmlReporter.getOrderHtmlTable;

/**
 * Servlet to display order (The 3rd screen of the homework.
 */
public class CheckFormer extends HttpServlet {

    @Override
    public final void doGet(final HttpServletRequest httpServletRequest,
                            final HttpServletResponse httpServletResponse)
            throws IOException {
        doPost(httpServletRequest, httpServletResponse);
    }


    /**
     * Handles POST requests.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse response to sent.
     * @throws IOException if IO error happens.
     */
    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException {
        HttpSession session = httpServletRequest.getSession();

        OrderHandler order =
                (Order) session.getAttribute(
                        SERVLETS_CURRENT_ORDER_ATTRIBUTE_NAME);


        PrintWriter responseWriter = httpServletResponse.getWriter();


        responseWriter.print("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n");


        if (order != null) {
            if (order.goodsCount() != 0) {
                responseWriter.print(getOrderHtmlTable(order, true));
            } else {
                responseWriter.print("You haven't ordered any good yet.<br>");
            }
        } else {
            responseWriter.print("Error<br>");
        }

        responseWriter.print("</body></html>");
    }
}
