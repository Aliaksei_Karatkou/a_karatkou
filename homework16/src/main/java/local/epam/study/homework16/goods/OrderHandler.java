package local.epam.study.homework16.goods;

import java.math.BigDecimal;

/**
 * Interface to handle an order information.
 */

public interface OrderHandler {

    /**
     * Returns client's name.
     *
     * @return Client's name.
     */
    String getClientName();

    /**
     * Returns Sum of all goods in the order.
     *
     * @return Summary costs.
     */
    BigDecimal getSum();

    /**
     * Sets client name.
     *
     * @param clientName Client name to set.
     */
    void setClientName(String clientName);


    /**
     * Adds good item to the order.
     *
     * @param item good to add.
     * @throws IllegalArgumentException if item is null.
     */
    void addItem(final GoodsItem item);

    /**
     * Gets goods item by index.
     *
     * @param index index of the item to return.
     * @return item
     * @throws IndexOutOfBoundsException if index is out of bound.
     */
    GoodsItem getItem(final int index);

    /**
     * Returns order's goods count.
     *
     * @return goods count.
     */
    int goodsCount();
}
