package local.epam.study.homework16.goods;

import java.util.ArrayList;

/**
 * Class to hold item's information (name and cost).
 */
public class GoodsInfoStorage {
    /**
     * List to hold items.
     */
    protected ArrayList<GoodsItem> items = new ArrayList<>();

    /**
     * Adds information about the item into this "storage catalog".
     * @param item item to add
     * @return index (id) of the added item, and -1 item is null.
     */
    public final int addItem(final GoodsItem item) {
        int result = -1;
        if (item != null) {
            result = items.size();
            items.add(item);
        }
        return result;
    }

    /**
     * Return item informaton (oject) by item id (index).
     * @param index index of the item that must be returned.
     * @return item object or null if itemd with requested id doesn't exists.
     */
    public final GoodsItem getItemByIndex(final int index) {
        if ((index < 0) || (index > items.size() - 1)) {
            return null;
        }
        return items.get(index);
    }

    /**
     * Get total items count.
     * @return items count
     */
    public final int getItemsCount() {
        return items.size();
    }
}
