package local.epam.study.homework16.goods;

import java.math.BigDecimal;

/**
 * Class to hold goods data (name, price).
 */
public final class GoodsItem {
    /**
     * Good's name.
     */
    protected String itemName;

    /**
     * Good's cost.
     */
    protected BigDecimal itemCost;


    /**
     * Constructor.
     *
     * @param name Good's name
     * @param cost Good's cost
     * @throws IllegalArgumentException if any parameter is null,
     *                                  or name is empty or consists of
     *                                  spaces, of if cost is negative.
     */
    public GoodsItem(final String name, final BigDecimal cost) {
        if ((name == null) || (cost == null)
                || (name.trim().isEmpty()) || (cost.signum() < 0)) {
            throw new IllegalArgumentException("Bad argument");
        }
        itemName = name;
        itemCost = cost;
    }

    /**
     * Getter for good's name.
     *
     * @return good's name
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Getter for good's cost.
     *
     * @return good's cost.
     */
    public BigDecimal getItemCost() {
        return itemCost;
    }

}
