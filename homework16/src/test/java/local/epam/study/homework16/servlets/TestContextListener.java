package local.epam.study.homework16.servlets;

import local.epam.study.homework16.goods.GoodsItem;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TestContextListener {

    @Test
    public void TestStartUpGoodsValidatorParam1IsNull() {

        //ContextListener.startUpGoodsValidator(null,)

        assertNull(ContextListener.startUpGoodsValidator(null, "df"));
    }

    @Test
    public void TestStartUpGoodsValidatorParam2IsNull() {
        assertNull(ContextListener.startUpGoodsValidator("sdf", null));
    }

    @Test
    public void TestStartUpGoodsValidatorWrongParamName() {
        assertNull(ContextListener.startUpGoodsValidator("sdfsdf", "4|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorWrongCost() {
        assertNull(ContextListener.startUpGoodsValidator("good3", "4r|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorNoCost() {
        assertNull(ContextListener.startUpGoodsValidator("good3", "|fsd"));
    }

    @Test
    public void TestStartUpGoodsValidatorNoName() {
        assertNull(ContextListener.startUpGoodsValidator("good3", "4|"));
    }

    @Test
    public void TestStartUpGoodsValidatorNormal() {
        GoodsItem item = ContextListener.startUpGoodsValidator("good3", "10" +
                "|Some item");
        assertNotNull(item);
        assertEquals(BigDecimal.TEN, item.getItemCost());
        assertEquals("Some item", item.getItemName());
    }


}
