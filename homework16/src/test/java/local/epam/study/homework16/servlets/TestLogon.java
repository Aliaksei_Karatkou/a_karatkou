package local.epam.study.homework16.servlet;

import local.epam.study.homework16.servlets.Logon;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//import static org.mockito.*;
//import org.apache.commons.io.FileUtils;


public class TestLogon {


    @Test
    public void testLogon() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new Logon().doGet(request, response);

        String expectedHtml = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<title>Pseudo shop</title>\n"
                + "</head>\n"
                + "<body style=\"text-align: center\">\n"
                + "\n"
                + " <h1>Welcome to Online Shop</h1><br/>\n"
                + "         <form action=\"Order\" method=\"post\">\n"
                + "         <input name=\"name\"><p>\n"
                + "<input type=\"checkbox\" name=\"Agreement\" value=\"agree\" "
                + ">I agree with the terms of service<br>         <button "
                + "type=\"submit\">Enter</button>\n"
                + "         </form>\n"
                + "         </body>\n"
                +"</html>";
        writer.flush(); // it may not have been flushed yet...
        assertEquals(expectedHtml, stringWriter.toString());
    }


}
