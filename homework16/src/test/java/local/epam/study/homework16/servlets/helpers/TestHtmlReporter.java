package local.epam.study.homework16.servlet.helpers;

import local.epam.study.homework16.goods.GoodsItem;
import local.epam.study.homework16.goods.Order;
import org.junit.Test;

import java.math.BigDecimal;

import static local.epam.study.homework16.servlets.helpers.HtmlReporter.getOrderHtmlTable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestHtmlReporter {
    @Test
    public void testGetOrderHtmlTableParameterNull() {
        assertTrue(getOrderHtmlTable(null, true).isEmpty());
    }

    @Test
    public void testGetOrderHtmlTableNormalOrderComplete() {
        Order order = new Order("client");
        order.addItem(new GoodsItem("item1", BigDecimal.ONE));
        order.addItem(new GoodsItem("item2", BigDecimal.TEN));
        String expectedString = "<table align=\"center\">\n" +
                "<th colspan=2>Dear client, your order:</th>\n" +
                "<tr><td align=\"right\">1)</td><td align=\"left\">item1 " +
                "1$</td></tr>\n" +
                "<tr><td align=\"right\">2)</td><td align=\"left\">item2 " +
                "10$</td></tr>\n" +
                "<tr><td colspan=2>Total: 11 $</td></tr></table>\n";
        String actualString = getOrderHtmlTable(order, true);
        assertEquals(expectedString, actualString);
    }

    @Test
    public void testGetOrderHtmlTableNormalOrderNotComplete() {
        Order order = new Order("client");
        order.addItem(new GoodsItem("item1", BigDecimal.ONE));
        order.addItem(new GoodsItem("item2", BigDecimal.TEN));
        String expectedString = "<table align=\"center\">\n" +
                "<th colspan=2>You have already chosen:</th>\n" +
                "<tr><td align=\"right\">1)</td><td align=\"left\">item1 " +
                "1$</td></tr>\n" +
                "<tr><td align=\"right\">2)</td><td align=\"left\">item2 " +
                "10$</td></tr>\n" +
                "</table>\n";
        String actualString = getOrderHtmlTable(order, false);
        assertEquals(expectedString, actualString);
    }

    @Test
    public void testGetOrderHtmlTableNormalEmptyOrderNotComplete() {
        Order order = new Order("client");
        String expectedString = "<table align=\"center\">\n" +
                "<th colspan=2>Make your order:</th>\n" +
                "</table>\n";
        String actualString = getOrderHtmlTable(order, false);
        assertEquals(expectedString, actualString);
    }

}
