package local.epam.study.homework16.services.validators;

import local.epam.study.homework16.goods.GoodsInfoStorage;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class TestLogonParametersValidator {
    @Test
    public void TestValidateNewGoodAddingNameIsNull() {
        assertFalse(LogonParametersValidator.validateNewGoodAdding(null,
                new GoodsInfoStorage(), 4));
    }

    @Test
    public void TestValidateNewGoodAddingWrongItemId() {
        assertFalse(LogonParametersValidator.validateNewGoodAdding("client",
                new GoodsInfoStorage(), 4));
    }
}
