package local.epam.study.homework16.goods;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestGoodsInfoStorage {


    @Test
    public void TestAddItemNull() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        assertEquals(-1, goodsInfoStorage.addItem(null));
    }

    @Test
    public void TestAddItemNormal() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        GoodsItem item = new GoodsItem("item", BigDecimal.ONE);
        int i = goodsInfoStorage.addItem(item);
        assertEquals(1, goodsInfoStorage.items.size());
        assertEquals(0, i);
        assertEquals(item, goodsInfoStorage.items.get(0));
    }

    @Test
    public void TestGetItemByIndex() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        GoodsItem item = new GoodsItem("item", BigDecimal.ONE);
        goodsInfoStorage.addItem(item);
        GoodsItem itemReturned = goodsInfoStorage.getItemByIndex(0);
        assertEquals(item, itemReturned);
    }

    @Test
    public void TestGetItemByIndexWrongIndex() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        GoodsItem itemReturned = goodsInfoStorage.getItemByIndex(0);
        assertNull(itemReturned);
    }

    @Test
    public void TestGetItemsCount() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        GoodsItem item = new GoodsItem("item", BigDecimal.ONE);
        goodsInfoStorage.addItem(item);
        assertEquals(1, goodsInfoStorage.getItemsCount());
    }

}
