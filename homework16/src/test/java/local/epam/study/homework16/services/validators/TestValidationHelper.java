package local.epam.study.homework16.services.validators;

import local.epam.study.homework16.goods.GoodsInfoStorage;
import local.epam.study.homework16.goods.GoodsItem;
import org.junit.Test;

import java.math.BigDecimal;

import static local.epam.study.homework16.services.validators.ValidationHelper.validateClientName;
import static local.epam.study.homework16.services.validators.ValidationHelper.validateRequestedGoodItem;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestValidationHelper {

    @Test
    public void TestValidateRequestedGoodItem_GoodsInfoIsNull() {
        assertFalse(validateRequestedGoodItem(null, 2));
    }

    @Test
    public void TestValidateRequestedGoodItemNoGood() {
        assertFalse(validateRequestedGoodItem(new GoodsInfoStorage(), 0));
    }

    @Test
    public void TestValidateRequestedGoodItemGoodIsNull() {
        assertFalse(validateRequestedGoodItem(new GoodsInfoStorage(), null));
    }


    @Test
    public void TestValidateRequestedGoodItemNormal() {
        GoodsInfoStorage goodsInfoStorage = new GoodsInfoStorage();
        goodsInfoStorage.addItem(new GoodsItem("item", BigDecimal.ONE));
        assertTrue(validateRequestedGoodItem(goodsInfoStorage, 0));
    }

    //----
    @Test
    public void TestValidateClientNameNameIsNull() {
        assertFalse(validateClientName(null));
    }

    @Test
    public void TestValidateClientNameNameIsEmpty() {
        assertFalse(validateClientName("  "));
    }

    @Test
    public void TestValidateClientNameNormal() {
        assertTrue(validateClientName("Normal name"));
    }
}
