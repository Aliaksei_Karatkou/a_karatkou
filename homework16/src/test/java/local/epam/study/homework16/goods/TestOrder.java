package local.epam.study.homework16.goods;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TestOrder {
    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorNameIsNull() {
        new Order(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestConstructorNameIsEmpty() {
        new Order("  ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestAddItemItemIsNull() {
        Order order = new Order("some name");
        order.addItem(null);
    }

    // ---
    @Test
    public void TestGetSum() {
        Order order = new Order("some name");
        order.sum = BigDecimal.TEN;
        assertEquals(0, order.getSum().compareTo(BigDecimal.TEN));
    }

    @Test
    public void TestGetClientName() {
        Order order = new Order("some name");
        assertEquals("some name", order.getClientName());
    }

    @Test
    public void TestSetClientName() {
        Order order = new Order("some name");
        order.setClientName("another name");
        assertEquals("another name", order.getClientName());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void TestGetItemIndexNegtive() {
        Order order = new Order("some name");
        order.getItem(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void TestGetItemIndexIsTooHigh() {
        Order order = new Order("some name");
        order.addItem(new GoodsItem("sdf", BigDecimal.TEN));
        order.getItem(1);
    }

    @Test
    public void TestGetItemNormal() {
        Order order = new Order("some name");
        GoodsItem itemOriginal = new GoodsItem("some client name",
                BigDecimal.TEN);
        order.addItem(itemOriginal);
        assertEquals(itemOriginal, order.getItem(0));
    }


}
