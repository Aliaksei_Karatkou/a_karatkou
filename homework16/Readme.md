##  Small readme.
Use apache tomcat.

The information about goods is stored in the web.xml

    <context-param>
        <param-name>good5</param-name>
        <param-value>2|item-5</param-value>
    </context-param>

param-name must be "goodN"
where N - digits
param-value stores cost and name of a good,
delimiter - "|"


how to run:

mvn tomcat:run

then visit first web-page

or

run "package" stage in maven (IDE), copy target\homework16.war
into webapps directory of the Tomcat and start/restart tomcat.

first web page:

http://localhost:8080/homework16/logon

