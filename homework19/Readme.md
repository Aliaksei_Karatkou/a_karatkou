##  Small readme.
Use apache tomcat, h2 database

The information about goods is stored in the h2 database,
initializated by init-script

how to run:

mvn clean tomcat7:run-war

then visit the first web-page
http://localhost:8080/logon

or

run "package" stage in maven, copy target\homework19.war
into the webapps folder of the Tomcat and start/restart tomcat.

first web page:
http://localhost:8080/homework19/logon

