package local.epam.study.homework19.dao.exceptions;

/**
 * Exception class to be thrown when requested data not found.
 */
public class DaoDataNotFound extends RuntimeException {

    /**
     * Default message to pass to this exception if required data not found.
     */
    public static final String EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND =
            "Required data (used in SQL statement) not found.";


    /**
     * Default message to pass to this exception if requested data not found.
     */
    public static final String EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND =
            "Requested data not found.";

    /**
     * Exception throw method if parsed of validated some value.
     *
     * @param message message to explain exception.
     */
    public DaoDataNotFound(final String message) {
        super(message);
    }

}
