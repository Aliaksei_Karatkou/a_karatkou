package local.epam.study.homework19.dao.implementations;

import local.epam.study.homework19.dao.databaseConnection.DbConnectionInformationInterface;
import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.exceptions.DaoFailedException;
import local.epam.study.homework19.dao.interfaces.OrderDao;
import local.epam.study.homework19.dao.objects.Order;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_CONNECTION_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_QUERY_FAILED;

/**
 * Class to implement data access to ORDERS table.
 */
public class OrderDaoImpl implements OrderDao {
    /**
     * SQL statement to insert new order.
     */
    private static final String INSERT_ORDER_SQL_STATEMENT =
            "INSERT INTO ORDERS (user_id, total_price) values (?,0);";
    /**
     * SQL statement to query order information by order id.
     */
    private static final String FIND_ORDER_BY_USERID_SQL_STATEMENT =
            "SELECT id, total_price FROM ORDERS  where user_id = ?;";
    /**
     * SQL statement to update (increase) order total price field.
     */
    private static final String UPDATE_TOTAL_PRICE_SQL_STATEMENT =
            "UPDATE ORDERS SET total_price = total_price + ? where id=?;";

    /**
     * Database connection information class.
     */
    private DbConnectionInformationInterface dbConnectionInfo;

    /**
     * Setter for dbConnectionInfo property.
     *
     * @param dbConnectionInfoToSet dbConnectionInfo property value.
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setDbConnectionInfo(
            final DbConnectionInformationInterface dbConnectionInfoToSet) {
        if (dbConnectionInfoToSet == null) {
            throw new IllegalArgumentException();
        }
        dbConnectionInfo = dbConnectionInfoToSet;
    }

    /**
     * Add Order to the database and return it's id.
     *
     * @param userId user id for order.
     * @return id of the new order.
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final int addOrder(final int userId) {
        int id;
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {

            try (PreparedStatement st =
                         conn.prepareStatement(INSERT_ORDER_SQL_STATEMENT)) {
                st.setInt(1, userId);
                st.executeUpdate();
                try (ResultSet resultSet = st.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        id = resultSet.getInt(1);
                    } else {
                        throw new DaoFailedException(
                                EXCEPTION_MSG_DAO_QUERY_FAILED);
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return id;
    }

    /**
     * Get order by user id.
     * Assume it can be one order per user.
     *
     * @param userId user id
     * @return order.
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final Order getOrderByUserId(final int userId) {
        Order order;
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {

            try (PreparedStatement st =
                         conn.prepareStatement(
                                 FIND_ORDER_BY_USERID_SQL_STATEMENT)) {
                st.setLong(1, userId);
                ResultSet rs = st.executeQuery();
                boolean anyOrderFound = rs.next();
                if (anyOrderFound) {
                    BigDecimal totalPrice = rs.getBigDecimal("total_price");
                    int orderId = rs.getInt("id");
                    order = new Order(orderId, userId, totalPrice);
                } else {
                    throw new DaoDataNotFound(
                            EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND);
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return order;
    }

    /**
     * Increase order total price.
     *
     * @param orderId        order id
     * @param priceIncrement price to increment by.
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final void increaseTotalPrice(final int orderId,
                                         final BigDecimal priceIncrement) {
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {

            try (PreparedStatement st =
                         conn.prepareStatement(
                                 UPDATE_TOTAL_PRICE_SQL_STATEMENT)) {
                st.setBigDecimal(1, priceIncrement);
                st.setLong(2, orderId);
                st.executeUpdate();
                if (st.getUpdateCount() == 0) {
                    throw new DaoDataNotFound(
                            EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND);
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
    }
}
