/**
 * Exception classes to use in data access methods.
 */
package local.epam.study.homework19.dao.exceptions;
