package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.interfaces.GoodDao;
import local.epam.study.homework19.dao.interfaces.OrderDao;
import local.epam.study.homework19.dao.interfaces.OrderGoodDao;
import local.epam.study.homework19.dao.objects.Good;
import local.epam.study.homework19.services.interfaces.OrderGoodServiceInterface;

import java.util.List;

/**
 * Class to handle order-good operations.
 */
public class OrderGoodService implements OrderGoodServiceInterface {

    /**
     * Service to handle OrderGood's dao requests.
     */
    private OrderGoodDao orderGoodDao = ServiceFacade.getOrderGoodDao();
    /**
     * Service to handle Good's dao requests.
     */
    private GoodDao goodDao = ServiceFacade.getGoodDao();
    /**
     * Service to handle Order's dao requests.
     */
    private OrderDao orderDao = ServiceFacade.getOrderDao();

    /**
     * Setter for orderGoodDao property.
     *
     * @param orderGoodDaoToSet orderGoodDao value to set.
     * @throws IllegalArgumentException if any parameter is null.
     */
    public final void setOrderGoodDao(final OrderGoodDao orderGoodDaoToSet) {
        if (orderGoodDaoToSet == null) {
            throw new IllegalArgumentException();
        }
        orderGoodDao = orderGoodDaoToSet;
    }

    /**
     * Setter for orderGoodDao property.
     *
     * @param goodDaoToSet orderGoodDao value to set.
     * @throws IllegalArgumentException if any parameter is null.
     */
    public final void setGoodDao(final GoodDao goodDaoToSet) {
        if (goodDaoToSet == null) {
            throw new IllegalArgumentException();
        }
        goodDao = goodDaoToSet;
    }

    /**
     * Setter for orderDao property.
     *
     * @param orderDaoToSet orderGoodDao value to set.
     * @throws IllegalArgumentException if any parameter is null.
     */
    public final void setOrderDao(final OrderDao orderDaoToSet) {
        if (orderDaoToSet == null) {
            throw new IllegalArgumentException();
        }
        orderDao = orderDaoToSet;
    }

    /**
     * Add good to the order.
     *
     * @param orderId order's id.
     * @param goodId  good's id.
     */
    @Override
    public final void addGoodToOrder(final int orderId, final int goodId) {
        orderGoodDao.addGoodToOrder(orderId, goodId);
        Good good = goodDao.getGood(goodId);
        orderDao.increaseTotalPrice(orderId, good.getPrice());
    }

    /**
     * Get all goods in required order.
     *
     * @param orderId order's id
     * @return List of the goods.
     */
    @Override
    public final List<Good> getOrderedGoods(final int orderId) {
        return orderGoodDao.getOrderGoodIds(orderId);
    }


}
