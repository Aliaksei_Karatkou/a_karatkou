package local.epam.study.homework19.dao.implementations;

import local.epam.study.homework19.dao.databaseConnection.DbConnectionInformationInterface;
import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.exceptions.DaoFailedException;
import local.epam.study.homework19.dao.interfaces.OrderGoodDao;
import local.epam.study.homework19.dao.objects.Good;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_CONNECTION_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED;

/**
 * Class to implement data access to ORDERS table.
 */
public class OrderGoodDaoImpl implements OrderGoodDao {
    /**
     * SQL statement to insert data into the ORDERS_GOOD table.
     */
    private static final String ADD_ORDER_GOOD_SQL_STATEMENT =
            "INSERT INTO ORDERS_GOOD (order_id, good_id) values (?,?);";

    /**
     * SQL statement to query all goods for the required order (by order id).
     * Use table GOOD and ORDERS_GOOD.
     */
    private static final String GET_GOODS_BY_ORDER_SQL_STATEMENT =
            "SELECT GOOD.*  FROM GOOD, ORDERS_GOOD where "
                    + "ORDERS_GOOD.good_id = GOOD.id AND ORDERS_GOOD"
                    + ".order_id=?;";

    /**
     * Database connection information class.
     */
    private DbConnectionInformationInterface dbConnectionInfo;

    /**
     * Setter for dbConnectionInfo property.
     *
     * @param dbConnectionInfoToSet dbConnectionInfo property value.
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setDbConnectionInfo(
            final DbConnectionInformationInterface dbConnectionInfoToSet) {
        if (dbConnectionInfoToSet == null) {
            throw new IllegalArgumentException();
        }
        dbConnectionInfo = dbConnectionInfoToSet;
    }

    /**
     * Class to add order to the order (by order id).
     *
     * @param orderId order id of the order to add to.
     * @param goodId  id of the good.
     * @return new record id in the ORDERS_GOOD table.
     * @throws DaoFailedException if database connection/query failed.
     * @throws DaoDataNotFound    if required data not found.
     */
    @Override
    public final int addGoodToOrder(final int orderId, final int goodId) {
        int id;
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(ADD_ORDER_GOOD_SQL_STATEMENT)) {
                st.setInt(1, orderId);
                st.setInt(2, goodId);
                st.executeUpdate();
                try (ResultSet resultSet = st.getGeneratedKeys()) {

                    if (resultSet.next()) {
                        id = resultSet.getInt(1);
                    } else {
                        throw new DaoDataNotFound(
                                EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND);
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return id;
    }

    /**
     * Get order goods by order id.
     *
     * @param orderId order id
     * @return List of the good objects.
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final List<Good> getOrderGoodIds(final int orderId) {
        ArrayList<Good> goods = new ArrayList<>();
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(
                                 GET_GOODS_BY_ORDER_SQL_STATEMENT)) {
                st.setInt(1, orderId);
                st.executeQuery();
                try (ResultSet resultSet = st.executeQuery()) {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String title = resultSet.getString("title");
                        BigDecimal price = resultSet.getBigDecimal("price");
                        goods.add(new Good(id, title, price));
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return goods;
    }

}
