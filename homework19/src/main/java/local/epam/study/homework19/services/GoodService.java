package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.implementations.GoodDaoImpl;
import local.epam.study.homework19.dao.interfaces.GoodDao;
import local.epam.study.homework19.dao.objects.Good;
import local.epam.study.homework19.services.interfaces.GoodServiceInterface;

import java.util.List;

/**
 * Class to handle operations with goods.
 */
public class GoodService implements GoodServiceInterface {

    /**
     * Service to handle dao requests  for good object.
     */
    private GoodDao goodDao = ServiceFacade.getGoodDao();


    /**
     * Setter for goodDao.
     * @param goodDaoToSet goodDao to set.
     */
    public final void setGoodDao(final GoodDaoImpl goodDaoToSet) {
        goodDao = goodDaoToSet;
    }

    /**
     * Get all goods.
     *
     * @return List of goods.
     */
    @Override
    public final List<Good> getAllGoods() {
        return goodDao.getAllGoods();
    }
}
