package local.epam.study.homework19.dao.databaseConnection;

/**
 * Class to store database connection inforamtion.
 */
public final class DbConnectionInformation
        implements DbConnectionInformationInterface {
    /**
     * Is database connection url for initialization was returned.
     */
    private static boolean initUrlReturned = false;
    /**
     * Database connection URL.
     */
    private String dbUrl;
    /**
     * Database connection user name.
     */
    private String dbUser;
    /**
     * Database connection password.
     */
    private String dbPassword;
    /**
     * Database connection driver name.
     */
    private String dbDriverName;
    /**
     * Init script url part.
     */
    private String dbInitScript;

    /**
     * Load database driver class.
     */
    public void loadDriverClass() {
        if (dbDriverName != null) {
            try {
                Class.forName(dbDriverName);

            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
                throw new DbConnectionInformationUnknown(
                        "data not loaded (IOError)");
            }
        }
    }


    @Override
    public String getConnectionUrl() {
        if (initUrlReturned) {
            return dbUrl;
        } else {
            initUrlReturned = true;
            return dbUrl + dbInitScript;
        }
    }

    /**
     * Get dbUrl property.
      * @return dbUrl property
     */
    public String getDbUrl() {
        return dbUrl;
    }

    /**
     * Set dbUrl property.
     * @param dbUrlToSet property value to set.
     */
    public void setDbUrl(final String dbUrlToSet) {
        dbUrl = dbUrlToSet;
    }

    /**
     * Get database connection user name.
     * @return database connection user name.
     */
    public String getDbUser() {
        return dbUser;
    }

    /**
     * Set database connection user name.
     * @param dbUserToSet database connection user name to set.
     */
    public void setDbUser(final String dbUserToSet) {
        dbUser = dbUserToSet;
    }

    /**
     * Get database connection password.
     * @return database connection password.
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * Set Get database connection password.
     * @param dbPasswordToSet database connection password to set.
     */
    public void setDbPassword(final String dbPasswordToSet) {
        dbPassword = dbPasswordToSet;
    }
    /**
     * Get database driver name.
     * @return database driver name.
     */
    @Override
    public String getDbDriverName() {
        return dbDriverName;
    }

    /**
     * Set database driver name.
     * @param dbDriverNameToSet database driver name.
     */
    @Override
    public void setDbDriverName(final String dbDriverNameToSet) {
        dbDriverName = dbDriverNameToSet;
    }
    /**
     * Get database connection URL init part.
     * @return dbInitScript database connection URL init part.
     */
    public String getDbInitScript() {
        return dbInitScript;
    }

    /**
     * Set database connection URL init part.
     * @param dbInitScriptToSet database connection URL init part to set.
     */
    public void setDbInitScript(final String dbInitScriptToSet) {
        dbInitScript = dbInitScriptToSet;
    }
}
