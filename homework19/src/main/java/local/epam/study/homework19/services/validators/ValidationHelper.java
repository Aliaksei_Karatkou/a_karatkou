package local.epam.study.homework19.services.validators;


/**
 * Class for handling validation of different URL-parameters.
 */

public final class ValidationHelper {

    /**
     * Minimal allowed goods list's index.
     */
    private static final int MIN_ALLOWED_GOODS_ID = 1;

    /**
     * hidden constructor.
     */
    private ValidationHelper() {

    }

    /**
     * Validates client name.
     * Must not be null, or empty, or contain only spaces.
     *
     * @param parameterToValidate client name to validate
     * @return true if validation was successful.
     */
    public static boolean validateClientName(final String parameterToValidate) {
        if (parameterToValidate == null) {
            return false;
        }
        return !parameterToValidate.trim().isEmpty();
    }

    /**
     * Validates requested good's id.
     *
     * @param requestedGoodId requested good's id.
     * @return true if validation successfully completed
     */

    public static boolean validateRequestedGoodItem(
            final Integer requestedGoodId) {

        if (requestedGoodId == null) {
            return false;
        }
        return (requestedGoodId >= MIN_ALLOWED_GOODS_ID);
    }
}
