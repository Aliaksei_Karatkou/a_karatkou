package local.epam.study.homework19.dao.interfaces;

import local.epam.study.homework19.dao.objects.User;

/**
 * User objects access interface.
 */
public interface UserDao {
    /**
     * Get user by id.
     *
     * @param id user id.
     * @return user object.
     */

    User getUser(int id);

    /**
     * Add user to USER table.
     *
     * @param user user to add.
     */
    void addUser(User user);

    /**
     * Return User object by login name.
     *
     * @param login login name.
     * @return User object.
     */

    User getUserByLogin(String login);
}
