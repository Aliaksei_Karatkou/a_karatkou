package local.epam.study.homework19.servlets;


/**
 * Class to store html parameters' names.
 */
public final class ParameterNames {
    /**
     * Parameter name to post client name.
     */
    public static final String PARAMETER_NAME_CLIENT_NAME = "name";

    /**
     * An agreement message.
     */

    public static final String AGREEMENT_TITLE = "I agree with the terms of "
            + "service";

    /**
     * Hidden Constructor.
     */
    private ParameterNames() {

    }

}
