package local.epam.study.homework19.servlets.helpers;

import javax.servlet.http.HttpServletRequest;

import static local.epam.study.homework19.servlets.OrderBuilder.PARAMETER_NAME_NEW_ITEM_ID;
import static local.epam.study.homework19.servlets.ParameterNames.PARAMETER_NAME_CLIENT_NAME;

/**
 * Class handles request parameters.
 */
public class ParametersHandler {

    /**
     * request used to run methods.
     */
    private HttpServletRequest request;

    /**
     * Constructor.
     *
     * @param httpServletRequest request to use after object creation.
     */
    public ParametersHandler(final HttpServletRequest httpServletRequest) {
        if (httpServletRequest == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        request = httpServletRequest;
    }

    /**
     * Converts String to Integer.
     *
     * @param value Integer value
     * @return null if parameter is null or conversion error happens.
     */
    public static Integer stringToInteger(final String value) {
        Integer result;
        if (value == null) {
            return null;
        }
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            result = null;
        }
        return result;
    }

    /**
     * Get client name (from request parameters).
     *
     * @return client name.
     */
    public final String getClientName() {
        return request.getParameter(PARAMETER_NAME_CLIENT_NAME);
    }

    /**
     * get requested good id.
     *
     * @return good's id, or null if any error happens.
     */
    public final Integer getNewRequestedItemId() {
        String goodIdStr = request.getParameter(PARAMETER_NAME_NEW_ITEM_ID);
        return stringToInteger(goodIdStr);
    }
}

