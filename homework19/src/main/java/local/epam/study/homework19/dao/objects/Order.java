package local.epam.study.homework19.dao.objects;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Class represents Order object.
 */
public final class Order {
    /**
     * Order's id.
     */
    protected int id;
    /**
     * User's id for the order.
     */
    protected int userId;
    /**
     * Total order's price.
     */
    protected BigDecimal totalPrice;

    /**
     * Goods of this order.
     */
    protected List<Good> goods;

    /**
     * Constructor.
     *
     * @param creatingId     order's id.
     * @param creatingUserId user's id.
     */
    public Order(final int creatingId, final int creatingUserId) {
        id = creatingId;
        userId = creatingUserId;
        totalPrice = BigDecimal.ZERO;
        goods = new ArrayList<>();
    }

    /**
     * Constructor.
     *
     * @param creatingId     order's id.
     * @param creatingUserId user's id.
     * @param creatingPrice  inital order's price.
     * @throws IllegalArgumentException if price is null
     */
    public Order(final int creatingId, final int creatingUserId,
                 final BigDecimal creatingPrice) {
        if (creatingPrice == null) {
            throw new IllegalArgumentException();
        }
        id = creatingId;
        userId = creatingUserId;
        totalPrice = creatingPrice;
        goods = new ArrayList<>();
    }

    /**
     * Getter for id field.
     *
     * @return id field.
     */
    public int getId() {
        return id;
    }

    /**
     * Setter of id field.
     *
     * @param idToSet id filed value to set.
     */
    public void setId(final int idToSet) {
        id = idToSet;
    }

    /**
     * Getter for userId field.
     *
     * @return userId field.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Setter for userId field.
     *
     * @param userIdToSet userId value to set.
     */
    public void setUserId(final int userIdToSet) {
        userId = userIdToSet;
    }

    /**
     * Getter for totalPrice field.
     *
     * @return totalPrice field.
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * Setter for totalPrice field.
     *
     * @param totalPriceToSet totalPrice field to set.
     * @throws IllegalArgumentException if parameter is null
     */
    public void setTotalPrice(final BigDecimal totalPriceToSet) {
        if (totalPriceToSet == null) {
            throw new IllegalArgumentException();
        }
        totalPrice = totalPriceToSet;
    }

    /**
     * Getter for goods list.
     *
     * @return Goods list
     */
    public List<Good> getGoods() {
        return goods;
    }

    /**
     * Setter for goods list.
     *
     * @param goodsToSet goods list to set.
     * @throws IllegalArgumentException if parameter is null.
     */
    public void setGoods(final List<Good> goodsToSet) {
        if (goodsToSet == null) {
            throw new IllegalArgumentException();
        }
        goods = goodsToSet;
    }
}
