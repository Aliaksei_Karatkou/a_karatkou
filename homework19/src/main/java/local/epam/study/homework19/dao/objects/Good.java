package local.epam.study.homework19.dao.objects;

import java.math.BigDecimal;

/**
 * Class represents Good object.
 */
public final class Good {
    /**
     * Good's id.
     */
    protected int id;
    /**
     * Good's title.
     */
    protected String title;

    /**
     * Good's price.
     */
    protected BigDecimal price;

    /**
     * Default constructor.
     */
    protected Good() {

    }

    /**
     * Constructor.
     *
     * @param goodId    good's id.
     * @param goodTitle good's title.
     * @param goodPrice good's price.
     * @throws IllegalArgumentException if
     */
    public Good(final int goodId, final String goodTitle,
                final BigDecimal goodPrice) {
        if (goodTitle == null || goodPrice == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        id = goodId;
        title = goodTitle;
        price = goodPrice;
    }

    /**
     * Getter for id field.
     *
     * @return id value
     */
    public int getId() {
        return id;
    }

    /**
     * Setter for id filed.
     *
     * @param newId value of the id field to set.
     */
    public void setId(final int newId) {
        id = newId;
    }

    /**
     * Getter for title filed.
     *
     * @return title value.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for title filed.
     *
     * @param newTitle value of the title field to set.
     * @throws IllegalArgumentException if parameter is null
     */
    public void setTitle(final String newTitle) {
        if (newTitle == null) {
            throw new IllegalArgumentException();
        }
        title = newTitle;
    }

    /**
     * Getter for price field.
     *
     * @return price value
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Setter for price filed.
     *
     * @param newPrice value of the price field to set.
     * @throws IllegalArgumentException if parameter is null
     */
    public void setPrice(final BigDecimal newPrice) {
        if (newPrice == null) {
            throw new IllegalArgumentException();
        }
        price = newPrice;
    }
}
