package local.epam.study.homework19.dao.databaseConnection;

/**
 * Interface to interact with database connetion information class.
 */
public interface DbConnectionInformationInterface {

    /**
     * Get database connection user name.
     * @return database connection user name.
     */

    String getDbUser();

    /**
     * Set database connection user name.
     * @param dbUser database connection user name to set.
     */
    void setDbUser(String dbUser);

    /**
     * Get database connection password.
     * @return database connection password.
     */
    String getDbPassword();

    /**
     * Set Get database connection password.
     * @param dbPassword database connection password to set.
     */
    void setDbPassword(String dbPassword);

    /**
     * Get database driver name.
     * @return database driver name.
     */
    String getDbDriverName();

    /**
     * Set database driver name.
     * @param dbDriverName database driver name.
     */
    void setDbDriverName(String dbDriverName);

    /**
     * Get database connection user name.
     * @return database connection user name.
     */
    String getConnectionUrl();

}
