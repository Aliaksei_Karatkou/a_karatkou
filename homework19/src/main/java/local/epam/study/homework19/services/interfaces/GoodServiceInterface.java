package local.epam.study.homework19.services.interfaces;

import local.epam.study.homework19.dao.objects.Good;

import java.util.List;

/**
 * Interface for service classes for handling goods.
 */
public interface GoodServiceInterface {
    /**
     * Get all goods.
     * @return List of all available goods.
     */
    List<Good> getAllGoods();
}
