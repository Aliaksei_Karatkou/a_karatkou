package local.epam.study.homework19.dao.databaseConnection;


/**
 * Exception to be thrown if unable to read database connection information.
 */
public class DbConnectionInformationUnknown extends RuntimeException {
    /**
     * Exception throw method if parsed of validated some value.
     *
     * @param message message to explain exception.
     */
    public DbConnectionInformationUnknown(
            final String message) {
        super(message);
    }

}
