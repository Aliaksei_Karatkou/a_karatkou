package local.epam.study.homework19.servlets;

import local.epam.study.homework19.dao.objects.Good;
import local.epam.study.homework19.dao.objects.Order;
import local.epam.study.homework19.dao.objects.User;
import local.epam.study.homework19.services.ServiceFacade;
import local.epam.study.homework19.services.interfaces.OrderGoodServiceInterface;
import local.epam.study.homework19.services.interfaces.OrderServiceInterface;
import local.epam.study.homework19.services.interfaces.UserServiceInterface;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import static local.epam.study.homework19.servlets.OrderBuilder.SESSION_ATTRIBUTE_USED_ID;
import static local.epam.study.homework19.servlets.ParameterNames.PARAMETER_NAME_CLIENT_NAME;


/**
 * Servlet to display order (The 3rd screen of the homework.
 */
public class CheckFormer extends HttpServlet {

    /**
     * Handles POST requests.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse response to sent.
     * @throws IOException if IO error happens.
     */
    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException, ServletException {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher(
                "checkFormer.jsp");
        HttpSession session = httpServletRequest.getSession();

        Integer userID = (Integer) session.getAttribute(
                SESSION_ATTRIBUTE_USED_ID);

        OrderGoodServiceInterface orderGoodService =
                ServiceFacade.getOrderGoodService();

        UserServiceInterface userService = ServiceFacade.getUserService();
        User user = userService.getUserNameById(userID);

        OrderServiceInterface orderService = ServiceFacade.getOrderService();
        Order order = orderService.getSuitableOrder(userID);
        BigDecimal orderTotalPrice = order.getTotalPrice();
        int orderId = order.getId();


        ArrayList<Good> orderedGoods =
                (ArrayList) orderGoodService.getOrderedGoods(orderId);

        httpServletRequest.setAttribute(
                PARAMETER_NAME_CLIENT_NAME, user.getLogin());
        httpServletRequest.setAttribute("orderedGoods", orderedGoods);
        httpServletRequest.setAttribute("orderTotalPrice", orderTotalPrice);
        httpServletRequest.setAttribute("name", user.getLogin());

        view.forward(httpServletRequest, httpServletResponse);
    }
}
