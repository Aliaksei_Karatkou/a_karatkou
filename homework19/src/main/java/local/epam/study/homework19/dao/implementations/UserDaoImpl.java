package local.epam.study.homework19.dao.implementations;

import local.epam.study.homework19.dao.databaseConnection.DbConnectionInformationInterface;
import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.exceptions.DaoFailedException;
import local.epam.study.homework19.dao.interfaces.UserDao;
import local.epam.study.homework19.dao.objects.User;
import local.epam.study.homework19.services.ServiceFacade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_CONNECTION_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_QUERY_FAILED;

/**
 * Class to implement data access to USER table.
 */
public class UserDaoImpl implements UserDao {
    /**
     * SQL statement to find user by id.
     */
    private static final String FIND_USER_BY_ID_SQL_STATEMENT =
            "SELECT login, password FROM USER where id=?;";
    /**
     * SQL statement to insert new user login.
     */
    private static final String INSERT_USERS_SQL_STATEMENT =
            "INSERT INTO USER (login,password) values (?,'');";
    /**
     * SQL statement to find user information by login.
     */
    private static final String FIND_USER_BY_LOGIN_SQL_STATEMENT =
            "SELECT id, password FROM USER where login = ?";
    /**
     * Database connection information class.
     */
    private DbConnectionInformationInterface dbConnectionInfo =
            ServiceFacade.getDbConnectionInfo();

    /**
     * Setter for dbConnectionInfo property.
     *
     * @param dbConnectionInfoToSet dbConnectionInfo property value.
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setDbConnectionInfo(
            final DbConnectionInformationInterface dbConnectionInfoToSet) {
        if (dbConnectionInfoToSet == null) {
            throw new IllegalArgumentException();
        }
        dbConnectionInfo = dbConnectionInfoToSet;
    }


    /**
     * Get user by id.
     *
     * @param id user id.
     * @return user object.
     * @throws DaoDataNotFound    if data not found
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final User getUser(final int id) {
        User user;
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {

            try (PreparedStatement st =
                         conn.prepareStatement(FIND_USER_BY_ID_SQL_STATEMENT)) {
                st.setLong(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    boolean anyUserFound = rs.next();
                    if (anyUserFound) {
                        String login = rs.getString("login");
                        String password = rs.getString("password");
                        return new User(login, password, id);
                    } else {
                        throw new DaoDataNotFound(
                                EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND);
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
    }


    /**
     * Add user to USER table.
     *
     * @param user user to add.
     * @throws DaoDataNotFound    if data not found
     * @throws DaoFailedException if database connection/query failed.
     *                            After addition change user id of the passed
     *                            object.
     */
    @Override
    public  void addUser(final User user) {

        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(INSERT_USERS_SQL_STATEMENT)) {
                st.setString(1, user.getLogin());
                st.executeUpdate();
                try (ResultSet resultSet = st.getGeneratedKeys()) {
                    int id;
                    if (resultSet.next()) {
                        id = resultSet.getInt(1);
                    } else {
                        throw new DaoDataNotFound(
                                EXCEPTION_MSG_DAO_QUERY_FAILED);
                    }
                    user.setId(id);
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (
                SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
    }

    /**
     * Return User object by login name.
     *
     * @param login login name.
     * @return User object.
     * @throws DaoDataNotFound    if data not found
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public   User getUserByLogin( final String login) {

        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(
                                 FIND_USER_BY_LOGIN_SQL_STATEMENT)) {
                st.setString(1, login);
                try (ResultSet rs = st.executeQuery()) {
                    boolean anyUserFound = rs.next();
                    if (anyUserFound) {
                        int id = rs.getInt("id");
                        String password = rs.getString("password");

                        return new User(login, password, id);
                    } else {
                        throw new DaoDataNotFound(
                                EXCEPTION_MSG_DAO_REQIRED_DATA_NOT_FOUND);
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
    }

}


