package local.epam.study.homework19.dao.interfaces;

import local.epam.study.homework19.dao.objects.Good;

import java.util.List;

/**
 * Some order and good objects access interface.
 */
public interface OrderGoodDao {
    /**
     * Class to add order to the order (by order id).
     *
     * @param orderId order id of the order to add to.
     * @param goodId  id of the good.
     * @return new record id in the ORDERS_GOOD table.
     */
    int addGoodToOrder(int orderId, int goodId);

    /**
     * Get order goods by order id.
     *
     * @param orderId order id
     * @return List of the good objects.
     */
    List<Good> getOrderGoodIds(int orderId);

}
