package local.epam.study.homework19.servlets;

import local.epam.study.homework19.services.ServiceFacade;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * Class used to load an information about allowed items.
 */

public class ContextListener implements ServletContextListener {

    /**
     * Initialization method. Call injection of dependencies by Spring.
     * @param event ServletContextEvent event.
     */
    public final void contextInitialized(final ServletContextEvent event) {
        ServiceFacade.init();
    }


    /**
     * Empty method.
     *
     * @param event doesn't matter.
     */
    public void contextDestroyed(final ServletContextEvent event) {
// nothing to do here
    }


}
