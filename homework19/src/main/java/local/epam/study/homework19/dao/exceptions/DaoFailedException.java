package local.epam.study.homework19.dao.exceptions;

/**
 * Class of an exception to be thrown when data access layer method failed.
 */
public class DaoFailedException extends RuntimeException {
    /**
     * Default message to pass to this exception if DAO connection failed.
     */
    public static final String EXCEPTION_MSG_DAO_CONNECTION_FAILED =
            "DAO connection failed";

    /**
     * Default message to pass to this exception if DAO query failed.
     */
    public static final String EXCEPTION_MSG_DAO_QUERY_FAILED =
            "DAO query failed";

    /**
     * Default message to pass to this exception if DAO query failed.
     */
    public static final String EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED =
            "DAO prepare SQL statement failed";

    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public DaoFailedException(final String message) {
        super(message);
    }

}
