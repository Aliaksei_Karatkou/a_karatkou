package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.interfaces.OrderDao;
import local.epam.study.homework19.dao.objects.Order;
import local.epam.study.homework19.services.interfaces.OrderServiceInterface;

import java.math.BigDecimal;

/**
 * Class to handle orders.
 */

public class OrderService implements OrderServiceInterface {

    /**
     * object to implement dao Order routines.
     */
    private OrderDao orderDao = ServiceFacade.getOrderDao();

    /**
     * Constructor.
     */
    public OrderService() {

    }

    /**
     * Setter for orderDaoToSet property.
     * @param orderDaoToSet orderDaoToSet property to set.
     */
    public final void setOrderDao(final OrderDao orderDaoToSet) {
        orderDao = orderDaoToSet;
    }

    /**
     * Add new order to user and get it back.
     *
     * @param userId user id
     * @return Order object
     */
    @Override
    public final Order addOrderGetItBack(final int userId) {
        int orderId = orderDao.addOrder(userId);
        return new Order(orderId, userId, BigDecimal.ZERO);
    }

    /**
     * Return order object, that corresponds to the user.
     * If order doesn't exists then method creates new one.
     *
     * @param userId User id
     * @return Order object for the user.
     */
    @Override
    public final Order getSuitableOrder(final int userId) {
        Order order;
        try {
            order = orderDao.getOrderByUserId(userId);
        } catch (DaoDataNotFound ex) {
            return addOrderGetItBack(userId);
        }
        return order;
    }

}
