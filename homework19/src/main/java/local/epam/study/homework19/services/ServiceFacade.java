package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.databaseConnection.DbConnectionInformationInterface;
import local.epam.study.homework19.dao.interfaces.GoodDao;
import local.epam.study.homework19.dao.interfaces.OrderDao;
import local.epam.study.homework19.dao.interfaces.OrderGoodDao;
import local.epam.study.homework19.dao.interfaces.UserDao;
import local.epam.study.homework19.services.interfaces.GoodServiceInterface;
import local.epam.study.homework19.services.interfaces.OrderGoodServiceInterface;
import local.epam.study.homework19.services.interfaces.OrderServiceInterface;
import local.epam.study.homework19.services.interfaces.UserServiceInterface;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Class to create and return injected implementations of classes.
 */

public final class ServiceFacade {
    /**
     * DbConnectionInformationInterface implementation object.
     */
    private static DbConnectionInformationInterface dbConnectionInfo;

    /**
     * GoodServiceInterface implementation object.
     */

    private static GoodServiceInterface goodService;
    /**
     * OrderGoodServiceInterface implementation object.
     */

    private static OrderGoodServiceInterface orderGoodService;
    /**
     * UserServiceInterface implementation object.
     */

    private static UserServiceInterface userService;
    /**
     * OrderServiceInterface implementation object.
     */

    private static OrderServiceInterface orderService;
    /**
     * GoodDao implementation object.
     */
    private static GoodDao goodDao;
    /**
     * OrderDao implementation object.
     */
    private static OrderDao orderDao;
    /**
     * OrderGoodDao implementation object.
     */
    private static OrderGoodDao orderGoodDao;
    /**
     * UserDao implementation object.
     */
    private static UserDao userDao;

    /**
     * Hidden constructor.
     */
    private ServiceFacade() {

    }

    /**
     * Create injected objects.
     */
    public static void init() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        dbConnectionInfo = context.getBean("dbConnectionInfo",
                DbConnectionInformationInterface.class);

        // to use by services
        userDao = context.getBean("idUserDaoImpl", UserDao.class);
        goodDao = context.getBean("idGoodDaoImpl", GoodDao.class);
        orderGoodDao = context.getBean("idOrderGoodDaoImpl",
                OrderGoodDao.class);
        orderDao = context.getBean("idOrderDaoImpl", OrderDao.class);

        // To use by servlet
        goodService = context.getBean("idGoodService",
                GoodServiceInterface.class);
        userService = context.getBean("idUserService",
                UserServiceInterface.class);
        orderService = context.getBean("idOrderService",
                OrderServiceInterface.class);
        orderGoodService = context.getBean("idOrderGoodService",
                OrderGoodServiceInterface.class);
    }


    /**
     * Getter for the dbConnectionInfo property.
     *
     * @return dbConnectionInfo value
     */
    public static DbConnectionInformationInterface getDbConnectionInfo() {
        return dbConnectionInfo;
    }

    /**
     * Getter for the goodService property.
     *
     * @return goodService property value.
     */
    public static GoodServiceInterface getGoodService() {
        return goodService;
    }

    /**
     * Getter for the orderGoodService property.
     *
     * @return orderGoodService property value.
     */
    public static OrderGoodServiceInterface getOrderGoodService() {
        return orderGoodService;
    }

    /**
     * Getter for the userService property.
     *
     * @return userService property value.
     */
    public static UserServiceInterface getUserService() {
        return userService;
    }

    /**
     * Getter for the orderService property.
     *
     * @return orderService property value.
     */
    public static OrderServiceInterface getOrderService() {
        return orderService;
    }

    /**
     * Getter for the orderDao property.
     *
     * @return orderDao property value.
     */
    public static OrderDao getOrderDao() {
        return orderDao;
    }

    /**
     * Getter for the orderGoodDao property.
     *
     * @return orderGoodDao property value.
     */
    public static OrderGoodDao getOrderGoodDao() {
        return orderGoodDao;
    }

    /**
     * Getter for the userDao property.
     *
     * @return userDao property value.
     */
    public static UserDao getUserDao() {
        return userDao;
    }

    /**
     * Getter for the goodDao property.
     *
     * @return goodDao property value.
     */
    public static GoodDao getGoodDao() {
        return goodDao;
    }
}
