package local.epam.study.homework19.dao.interfaces;

import local.epam.study.homework19.dao.objects.Order;

import java.math.BigDecimal;

/**
 * Order objects access interface.
 */
public interface OrderDao {

    /**
     * Add Order to the database and return it's id.
     *
     * @param userId user id for order.
     * @return id of the new order.
     */
    int addOrder(int userId);

    /**
     * Get order by user id.
     * Assume it can be one order per user.
     *
     * @param userId user id
     * @return order.
     */
    Order getOrderByUserId(int userId);

    /**
     * Increase order total price.
     *
     * @param orderId        order id
     * @param priceIncrement price to increment by.
     */
    void increaseTotalPrice(int orderId, BigDecimal priceIncrement);

}
