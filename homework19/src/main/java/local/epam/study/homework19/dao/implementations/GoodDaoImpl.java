package local.epam.study.homework19.dao.implementations;

import local.epam.study.homework19.dao.databaseConnection.DbConnectionInformationInterface;
import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.exceptions.DaoFailedException;
import local.epam.study.homework19.dao.interfaces.GoodDao;
import local.epam.study.homework19.dao.objects.Good;
import local.epam.study.homework19.services.ServiceFacade;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static local.epam.study.homework19.dao.exceptions.DaoDataNotFound.EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_CONNECTION_FAILED;
import static local.epam.study.homework19.dao.exceptions.DaoFailedException.EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED;

/**
 * Class implements data access to the Good table.
 */
public class GoodDaoImpl implements GoodDao {
    /**
     * SQL statement to query  all goods.
     */
    private static final String GET_ALL_GOODS_SQL_STATEMENT =
            "select * from GOOD";
    /**
     * SQL statement to query getting a good with required id.
     */
    private static final String GET_GOOD_SQL_STATEMENT =
            "select * from GOOD where id =?;";

    /**
     * Database connection information class.
     */
    private DbConnectionInformationInterface dbConnectionInfo =
            ServiceFacade.getDbConnectionInfo();

    /**
     * Return all goods.
     *
     * @return ArrayList of goods.
     * @throws DaoDataNotFound    if data not found
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final List<Good> getAllGoods() {
        ArrayList<Good> goods = new ArrayList<>();

        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(GET_ALL_GOODS_SQL_STATEMENT)) {
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String title = rs.getString("title");
                        BigDecimal price = rs.getBigDecimal("price");
                        goods.add(new Good(id, title, price));
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return goods;
    }

    /**
     * Return Good object by id.
     *
     * @param id object's id
     * @return Good object
     * @throws DaoDataNotFound    if data not found
     * @throws DaoFailedException if database connection/query failed.
     */
    @Override
    public final Good getGood(final int id) {
        Good good;
        try (Connection conn =
                     DriverManager.getConnection(
                             dbConnectionInfo.getConnectionUrl(),
                             dbConnectionInfo.getDbUser(),
                             dbConnectionInfo.getDbPassword())) {
            try (PreparedStatement st =
                         conn.prepareStatement(GET_GOOD_SQL_STATEMENT)) {
                st.setInt(1, id);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        int goodId = rs.getInt("id");
                        String title = rs.getString("title");
                        BigDecimal price = rs.getBigDecimal("price");
                        good = new Good(goodId, title, price);
                    } else {
                        throw new DaoDataNotFound(
                                EXCEPTION_MSG_DAO_REQUESTED_DATA_NOT_FOUND);
                    }
                }
            } catch (SQLException e) {
                throw new DaoFailedException(
                        EXCEPTION_MSG_DAO_PREPARE_SQL_STATEMENT_FAILED);
            }
        } catch (SQLException ex) {
            throw new DaoFailedException(
                    EXCEPTION_MSG_DAO_CONNECTION_FAILED);
        }
        return good;
    }

    /**
     * Getter for dbConnectionInfo property.
     * @return dbConnectionInfo value.
     */
    public final DbConnectionInformationInterface getDbConnectionInfo() {
        return dbConnectionInfo;
    }
    /**
     * Setter for dbConnectionInfo property.
     *
     * @param dbConnectionInfoToSet dbConnectionInfo property value.
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setDbConnectionInfo(
            final DbConnectionInformationInterface dbConnectionInfoToSet) {
        if (dbConnectionInfoToSet == null) {
            throw new IllegalArgumentException();
        }
        dbConnectionInfo = dbConnectionInfoToSet;
    }

}
