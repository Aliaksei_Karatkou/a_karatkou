package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.interfaces.UserDao;
import local.epam.study.homework19.dao.objects.User;
import local.epam.study.homework19.services.interfaces.UserServiceInterface;

/**
 * Class to handle users.
 */
public class UserService implements UserServiceInterface {

    /**
     * Service to handle data access requests.
     */
    private UserDao userDao = ServiceFacade.getUserDao();

    /**
     * Setter for userDao property.
     * @param userDaoToSet userDao value to set.
     */
    public final void setUserDao(final UserDao userDaoToSet) {
        userDao = userDaoToSet;
    }

    /**
     * Get user object by login name.
     * If user doesn't exist then method creates new user object.
     *
     * @param loginName user login name
     * @return User object with required login.
     */
    @Override
    public  final int getUserIdByLoginName(final String loginName) {
        int userId;
        try {
            User user = userDao.getUserByLogin(loginName);
            userId = user.getId();
        } catch (DaoDataNotFound ex) {
            userId = -1;
        }
        if (userId >= 0) {
            return userId;
        }
        User user = new User(loginName, "");
        userDao.addUser(user);
        return user.getId();
    }

    /**
     * Get username id user id.
     *
     * @param id user id used to search user.
     * @return User object.
     */
    @Override
    public final User getUserNameById(final int id) {
        return userDao.getUser(id);
    }

}
