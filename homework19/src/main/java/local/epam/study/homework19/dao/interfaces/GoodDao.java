package local.epam.study.homework19.dao.interfaces;

import local.epam.study.homework19.dao.objects.Good;

import java.util.List;

/**
 * Good objects access interface.
 */
public interface GoodDao {
    /**
     * Return all goods.
     *
     * @return ArrayList of goods.
     */
    List<Good> getAllGoods();


    /**
     * Return Good object by id.
     *
     * @param id object's id
     * @return Good object
     */
    Good getGood(int id);
}
