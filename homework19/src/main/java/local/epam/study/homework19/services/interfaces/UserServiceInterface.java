package local.epam.study.homework19.services.interfaces;

import local.epam.study.homework19.dao.objects.User;

/**
 * Interface for service class for handling Users.
 */
public interface UserServiceInterface {
    /**
     * Get user object by login name.
     * If user doesn't exist then method creates new user object.
     *
     * @param loginName user login name
     * @return User object with required login.
     */
    int getUserIdByLoginName(final String loginName);

    /**
     * Get username id user id.
     *
     * @param id user id used to search user.
     * @return User object.
     */
    User getUserNameById(final int id);

}
