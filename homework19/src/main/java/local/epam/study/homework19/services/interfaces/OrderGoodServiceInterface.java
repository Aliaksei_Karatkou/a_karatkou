package local.epam.study.homework19.services.interfaces;

import local.epam.study.homework19.dao.objects.Good;

import java.util.List;

/**
 * Interface for service class for handling order-good relations.
 */
public interface OrderGoodServiceInterface {

    /**
     * Add good to the order.
     *
     * @param orderId order's id.
     * @param goodId  good's id.
     */
    void addGoodToOrder(final int orderId, final int goodId);

    /**
     * Get all goods in required order.
     *
     * @param orderId order's id
     * @return List of the goods.
     */
    List<Good> getOrderedGoods(final int orderId);
}
