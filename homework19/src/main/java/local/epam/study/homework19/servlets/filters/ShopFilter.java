package local.epam.study.homework19.servlets.filters;


import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter to avoid serving clients that doesn't agree with terms of use.
 */

public class ShopFilter extends HttpServlet implements Filter {
    /**
     * Page which will be ignored by the filter.
     */
    public static final String FILTER_EXCLUDE_URI = "/logon";

    /**
     * Page where check for agreement with terms of use.
     */
    public static final String FILTER_CHECK_FOR_AGREEMENT_URI = "/Order";

    /**
     * Page to make order.
     */
    public static final String URI_ORDER_SHOW = "/Check";


    /**
     * Page which must be not agree-client forwarded to.
     */
    public static final String FILTER_OOPS_URI = "/WEB-INF/html/oops.html";
    /**
     * Attribute/parameter name for agreement information.
     */
    public static final String AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME =
            "Agreement";
    /**
     * Agreement parameter value required to processing of all pages.
     */
    public static final String AGREEMENT_VALUE_REQ = "agree";


    /**
     * Empty method.
     *
     * @param config doesn't matter.
     * @throws ServletException if error.
     */
    public void init(final FilterConfig config) {
    }


    /**
     * Filter implementation.
     *
     * @param request  servlet request.
     * @param response servlet response.
     * @param chain    filter chain.
     * @throws IOException      if IO-error happens.
     * @throws ServletException if servlet error happens.
     */
    public final void doFilter(final ServletRequest request,
                               final ServletResponse response,
                               final FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String url = req.getRequestURI();

        // The application can be runned from the root of a web server,
        // like http://host:port/
        // or it can be runned from some webapp folder,
        // like http://host:port/homewok18/
        // so we going to "normalize" url to exclude possible webapp folder.
        // url will be used to compare what pages must be "protected".
        String contextPath = ((HttpServletRequest) request).getContextPath();
        if (!contextPath.isEmpty()) {
            int len = contextPath.length();
            url = url.substring(len);
        }
        HttpSession session = req.getSession();
        if (url.equals(FILTER_EXCLUDE_URI)) {
            session.removeAttribute(AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME);
            chain.doFilter(request, response);
        } else if (url.equals(FILTER_CHECK_FOR_AGREEMENT_URI)
                || url.equals(URI_ORDER_SHOW)) {
            if (url.equals(FILTER_CHECK_FOR_AGREEMENT_URI)) {
                String agreementByParams =
                        req.getParameter(AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME);
                if ((agreementByParams != null)
                        && (agreementByParams.equals(AGREEMENT_VALUE_REQ))) {
                    session.setAttribute(
                            AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME,
                            AGREEMENT_VALUE_REQ);
                }
            }
            String agreementValue =
                    (String) session.getAttribute(
                            AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME);
            if ((agreementValue != null)
                    && (agreementValue.equals(AGREEMENT_VALUE_REQ))) {
                chain.doFilter(request, response);
            } else {
                req.getRequestDispatcher(FILTER_OOPS_URI).forward(request,
                        response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * Empty method.
     */
    public void destroy() {
    }
}
