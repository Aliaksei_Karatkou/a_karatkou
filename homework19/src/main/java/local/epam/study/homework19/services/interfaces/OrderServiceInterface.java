package local.epam.study.homework19.services.interfaces;

import local.epam.study.homework19.dao.objects.Order;

/**
 * Interface for service classes for handling Orders.
 */
public interface OrderServiceInterface {

    /**
     * Add new order to user and get it back.
     *
     * @param userId user id
     * @return Order object
     */
    Order addOrderGetItBack(final int userId);

    /**
     * Return order object, that corresponds to the user.
     * If order doesn't exists then method creates new one.
     *
     * @param userId User id
     * @return Order object for the user.
     */
    Order getSuitableOrder(final int userId);
}
