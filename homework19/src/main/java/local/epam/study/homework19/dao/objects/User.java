package local.epam.study.homework19.dao.objects;

/**
 * Class represents Order object.
 */
public class User {
    /**
     * User's login.
     */
    protected String login;
    /**
     * User's password.
     */
    protected String password;
    /**
     * User's id.
     */
    protected int id = -1;

    /**
     * Constructor.
     *
     * @param userLogin    login name
     * @param userPassword password
     * @param userId       user id
     * @throws IllegalArgumentException if any parameter is null
     */
    public User(final String userLogin, final String userPassword,
                final int userId) {
        if (userLogin == null || userPassword == null) {
            throw new IllegalArgumentException();
        }
        login = userLogin;
        password = userPassword;
        id = userId;
    }

    /**
     * Constructor.
     *
     * @param userLogin    login
     * @param userPassword password
     */
    public User(final String userLogin, final String userPassword) {
        this(userLogin, userPassword, -1);
    }


    /**
     * Getter for login field.
     *
     * @return login.
     */
    public final String getLogin() {
        return login;
    }

    /**
     * Setter for login field.
     *
     * @param userLogin login field value
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setLogin(final String userLogin) {
        if (userLogin == null) {
            throw new IllegalArgumentException();
        }
        login = userLogin;
    }

    /**
     * Getter for password field.
     *
     * @return password.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Setter for password field.
     *
     * @param userPassword password field value.
     * @throws IllegalArgumentException if parameter is null.
     */
    public final void setPassword(final String userPassword) {
        if (userPassword == null) {
            throw new IllegalArgumentException();
        }

        password = userPassword;
    }

    /**
     * Getter for id field.
     *
     * @return id
     */
    public final int getId() {
        return id;
    }

    /**
     * Setter for id field.
     *
     * @param userId id field value
     */
    public final void setId(final int userId) {
        id = userId;
    }

}
