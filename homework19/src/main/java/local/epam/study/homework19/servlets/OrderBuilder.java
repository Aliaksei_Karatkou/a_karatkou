package local.epam.study.homework19.servlets;


import local.epam.study.homework19.dao.objects.Good;
import local.epam.study.homework19.services.GoodService;
import local.epam.study.homework19.services.OrderGoodService;
import local.epam.study.homework19.services.OrderService;
import local.epam.study.homework19.services.ServiceFacade;
import local.epam.study.homework19.services.UserService;
import local.epam.study.homework19.services.validators.ValidationHelper;
import local.epam.study.homework19.servlets.helpers.ParametersHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static local.epam.study.homework19.servlets.ParameterNames.PARAMETER_NAME_CLIENT_NAME;

/**
 * Servlet class to display html page to form the order.
 * (The 2nd screen of the homework).
 */
public class OrderBuilder extends HttpServlet {
    /**
     * URL parameter name to post chosen goods.
     */
    public static final String PARAMETER_NAME_NEW_ITEM_ID = "newid";

    /**
     * Session attribute to store user Id.
     */
    public static final String SESSION_ATTRIBUTE_USED_ID = "userID";

    @Override
    public final void doGet(final HttpServletRequest httpServletRequest,
                            final HttpServletResponse httpServletResponse)
            throws IOException, ServletException {
        doPost(httpServletRequest, httpServletResponse);
    }


    /**
     * Servlet method POST implementation.
     *
     * @param httpServletRequest  request to handle.
     * @param httpServletResponse response to sent
     * @throws IOException if IO error happens.
     */

    @Override
    public final void doPost(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse)
            throws IOException, ServletException {

        // we are going to divide logic (java) and a view parts.
        // At first, we should do a logic-part.

        RequestDispatcher view = httpServletRequest.getRequestDispatcher(
                "orderBuilder.jsp");

        HttpSession session = httpServletRequest.getSession();

        ParametersHandler parametersHandler =
                new ParametersHandler(httpServletRequest);


        String clientName = parametersHandler.getClientName().trim();
        UserService userService =
                (UserService) ServiceFacade.getUserService();
        Integer userID;

        boolean userNameCorrect = false;
        if (ValidationHelper.validateClientName(clientName)) {
            // change current user
            userID = userService.getUserIdByLoginName(clientName);
            session.setAttribute(SESSION_ATTRIBUTE_USED_ID, userID);
            userNameCorrect = true;
        } else {
            userID = (Integer) session.getAttribute(SESSION_ATTRIBUTE_USED_ID);
            if (userID != null) {
                userNameCorrect = true;
                clientName = userService.getUserNameById(userID).getLogin();
            }
        }



        boolean parametersCorrect = false;

        Integer newGoodId = parametersHandler.getNewRequestedItemId();

        GoodService goodService =
                (GoodService) ServiceFacade.getGoodService();
        ArrayList<Good> allGoods = (ArrayList) goodService.getAllGoods();
        ArrayList<Good> orderedGoods = new ArrayList<>();


        boolean newGoodSatisfied;
        if (newGoodId != null) {
            newGoodSatisfied =
                    ValidationHelper.validateRequestedGoodItem(newGoodId);
        } else {
            newGoodSatisfied = true;
        }

        if (userNameCorrect && newGoodSatisfied) {
            parametersCorrect = true;

            OrderService orderService =
                    (OrderService) ServiceFacade.getOrderService();
            int orderId = orderService.getSuitableOrder(userID).getId();


            OrderGoodService orderGoodService =
                    (OrderGoodService) ServiceFacade.getOrderGoodService();

            if (newGoodId != null) {
                orderGoodService.addGoodToOrder(orderId, newGoodId);
            }
            orderedGoods =
                    (ArrayList) orderGoodService.getOrderedGoods(orderId);
            httpServletRequest.setAttribute(PARAMETER_NAME_CLIENT_NAME,
                    clientName);
        }
        httpServletRequest.setAttribute("parametersCorrect", parametersCorrect);

        httpServletRequest.setAttribute("allGoods", allGoods);
        httpServletRequest.setAttribute("orderedGoods", orderedGoods);

        view.forward(httpServletRequest, httpServletResponse);
    }
}
