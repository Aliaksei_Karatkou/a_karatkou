DROP TABLE IF EXISTS USER;
CREATE TABLE USER (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    login character varying(255) NOT NULL,
    password character varying(31),
    UNIQUE KEY login (login)
);

DROP TABLE IF EXISTS GOOD;
CREATE TABLE GOOD (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    title character varying(255) NOT NULL,
    price DECIMAL(20, 2) NOT NULL
);


DROP TABLE IF EXISTS ORDERS;
CREATE TABLE ORDERS(
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    user_id integer NOT NULL,
   total_price DECIMAL(20, 2) NOT NULL,
foreign key (user_id ) references USER(id)
);

DROP TABLE IF EXISTS ORDERS_GOOD;
CREATE TABLE ORDERS_GOOD(
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    order_id integer NOT NULL,
   good_id integer NOT NULL,
  foreign key (order_id ) references ORDERS(id),
  foreign key (good_id ) references GOOD(id),
);

INSERT INTO USER (login, password) values 
('user1','password1'),
('user2','password2'),
('user3','password3'),
('user4','password4');


INSERT INTO GOOD (title, price) values 
('item-1', 10),
('item-2', 20),
('item-3', 30),
('item-4', 40),
('item-5', 50),
('item-6', 60),
('item-7', 70);
