<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
    <c:when test="${fn:length(orderedGoods) == 0}">
        ${param.zeroItemsText}
    </c:when>
    <c:otherwise>
            ${param.notZeroItemsText}
    </c:otherwise>
</c:choose>

<c:if test="${fn:length(orderedGoods) > 0}">
    <table align="center">
        <c:forEach items="${orderedGoods}" var="item" varStatus="loopCounter">
            <tr>
                <td align="right">${loopCounter.count})</td>
                <td align="left"> ${item.title} ${item.price} $</td>
            </tr>
        </c:forEach>
        <c:if test="${param.showOrderTotalCost == 1}">
            <th colspan=2>Total: ${orderTotalPrice} $</th>
        </c:if>
    </table>

</c:if>