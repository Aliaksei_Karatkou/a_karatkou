<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pseudo shop</title>
</head>
<body style="text-align: center">
Dear ${name}
<c:if test="${fn:length(orderedGoods) != 0}">, your order:</c:if>
<br>
<c:set var="showOrderTotalCost" value="1"/>
<jsp:include page="helpers/getOrderTable.jsp">
    <jsp:param name="zeroItemsText" value="You haven't ordered any good yet."/>
    <jsp:param name="showOrderTotalCost" value="1"/>
</jsp:include>


</body>
</html>
