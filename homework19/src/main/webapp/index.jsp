<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Redirect</title>
</head>
<body>
<%
    response.setStatus(301);
    response.setHeader("Location", "logon");
    response.setHeader("Connection", "close");
%>
</body>
</html>
