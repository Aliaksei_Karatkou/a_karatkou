<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pseudo shop</title>
</head>
<body style="text-align: center">
<c:choose>
    <c:when test="${not parametersCorrect}">
        <h1>Data is incorrect.</h1>
    </c:when>
    <c:otherwise>
        Hello, ${name}<br>
        <p></p>
        <c:set var="showOrderTotalCost" value="1"/>
        <jsp:include page="helpers/getOrderTable.jsp">
            <jsp:param name="zeroItemsText" value="Make your order:"/>
            <jsp:param name="showOrderTotalCost" value="0"/>
            <jsp:param name="notZeroItemsText" value="You have already chosen:"/>
        </jsp:include>

        <form action="Check" method="post">
            <input type="hidden" name="name" value="${order.getClientName()}">
            <select name="newid">
                <c:forEach items="${allGoods}" var="item" varStatus="loop">
                    <option value="${item.id}">${item.title} (${item.price} $)</option>
                </c:forEach>
            </select>
            <br><br>
            <button type="submit" formaction="Order">Add Item</button>
            <button type="submit">Submit</button>

        </form>
    </c:otherwise>
</c:choose>

</body>
</html>
