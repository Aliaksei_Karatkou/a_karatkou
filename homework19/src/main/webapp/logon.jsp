<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="static local.epam.study.homework19.servlets.ParameterNames.PARAMETER_NAME_CLIENT_NAME" %>
<%@ page import="static local.epam.study.homework19.servlets.ParameterNames.AGREEMENT_TITLE" %>
<%@ page import="static local.epam.study.homework19.servlets.filters.ShopFilter.AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME" %>
<%@ page import="static local.epam.study.homework19.servlets.filters.ShopFilter.AGREEMENT_VALUE_REQ" %>

<html>
<head>
    <title>Pseudo shop</title>
</head>
<body style="text-align: center">

<h1>Welcome to Online Shop</h1><br/>
<form action="Order" method="post">
    <input name="<%=PARAMETER_NAME_CLIENT_NAME%>">
    <p>
        <input type="checkbox" name="<%=AGREEMENT_PARAMETER_OR_ATTRIBUTE_NAME%>"
               value="<%=AGREEMENT_VALUE_REQ%>"><%=AGREEMENT_TITLE%><br>
        <button type="submit">Enter</button>
</form>
</body>
</html>
