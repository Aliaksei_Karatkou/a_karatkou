package local.epam.study.homework19.services;

import local.epam.study.homework19.dao.exceptions.DaoDataNotFound;
import local.epam.study.homework19.dao.implementations.UserDaoImpl;
import local.epam.study.homework19.dao.objects.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * This class to test UserService.
 * But it won't be executed because of requirement of finalization of
 * classes and parameters by checkstyle...
 */
public class TestUserService {

    /**
     * Test new user addition.
     */

    @Test
    public void testGetUserIdByLoginNameNewUser() {

//  given:
        UserDaoImpl userDao = mock(UserDaoImpl.class);
        when(userDao.getUserByLogin(anyString())).thenThrow(new DaoDataNotFound("qq"));
        doNothing().when(userDao).addUser(any());
//  when:
        UserService userService = new UserService();
        userService.setUserDao(userDao);
        int i = userService.getUserIdByLoginName("test");
//  then:
        verify(userDao, times(1)).getUserByLogin(eq("test"));
        verify(userDao, times(1)).addUser(any());

    }


    /**
     * Test getting old user information.
     */
    @Test
    public void testGetUserIdByLoginNameExistingUser() {

//  given:
        UserDaoImpl userDao = mock(UserDaoImpl.class);
        User databaseUser = new User("username", "password", 2);
        when(userDao.getUserByLogin(anyString())).thenReturn(databaseUser);
//  when:
        UserService userService = new UserService();
        userService.setUserDao(userDao);
        int i = userService.getUserIdByLoginName("test");
//  then:
        assertEquals(2, i);
        verify(userDao, times(1)).getUserByLogin(eq("test"));
        verify(userDao, never()).addUser(any());

    }

}

