package local.epam.study.homework19.dao.objects;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
public class TestUser {
    @Test(expected = IllegalArgumentException.class)
    public void testUserConstructor3LoginIsNull() {
        User user = new User(null,"pass",1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testUserConstructor3PasswordIsNull() {
        User user = new User("log",null,1);
    }

    @Test
    public void testUserConstructor3() {
        User user = new User("log","pass",1);
        assertEquals("log", user.login);
        assertEquals("pass",user.password);
        assertEquals(1,user.id);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testUserConstructor2LoginIsNull() {
        User user = new User(null,"pass");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testUserConstructor2PasswordIsNull() {
        User user = new User("log",null);
    }
    @Test
    public void testUserConstructor2() {
        User user = new User("log","pass");
        assertEquals("log", user.login);
        assertEquals("pass",user.password);
        assertEquals(-1,user.id);
    }
    // --- id ---
    @Test
    public void TestGetId() {
        User user = new User("login","password",2);
        assertEquals(2,user.getId());
    }

    @Test
    public void TestSetId() {
        User user = new User("login","password",2);
        user.setId(3);
        assertEquals(3,user.id);
    }
    // --- login ---
    @Test
    public void TestGetLogin() {
        User user = new User("login","password",2);
        assertEquals("login",user.getLogin());
    }

    @Test
    public void TestSetLogin() {
        User user = new User("login","password",2);
        user.setLogin("another login");
        assertEquals("another login",user.login);
    }
    @Test (expected = IllegalArgumentException.class)
    public void TestSetLoginNull() {
        User user = new User("login","password",2);
        user.setLogin(null);
    }
// --- password ---
@Test
public void TestGetPassword() {
    User user = new User("login","password",2);
    assertEquals("password",user.getPassword());
}

    @Test
    public void TestSetPassword() {
        User user = new User("login","password",2);
        user.setPassword("another value");
        assertEquals("another value",user.password);
    }
    @Test (expected = IllegalArgumentException.class)
    public void TestSetPasswordNull() {
        User user = new User("login","password",2);
        user.setPassword(null);
    }


}
