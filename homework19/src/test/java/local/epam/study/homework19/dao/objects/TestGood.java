package local.epam.study.homework19.dao.objects;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TestGood {


    @Test(expected = IllegalArgumentException.class)
    public void TestGoodConstructorTitleIsNull() {
        Good good = new Good(1, null, BigDecimal.ZERO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestGoodConstructorPriceIsNull() {
        Good good = new Good(1, "title", null);
    }

    @Test
    public void TestGoodConstructor() {
        Good good = new Good(1, "title", new BigDecimal(1));
        assertEquals(1, good.id);
        assertEquals("title", good.title);
        assertEquals(BigDecimal.ONE, good.price);
    }

    // ---- id
    @Test
    public void TestGetId() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        assertEquals(1, good.getId());
    }

    @Test
    public void TestSetId() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        good.setId(2);
        assertEquals(2, good.id);
    }

    // --- title ---
    @Test
    public void TestGetTitle() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        assertEquals("title", good.getTitle());
    }

    @Test
    public void TestSetTitle() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        good.setTitle("new title");
        assertEquals("new title", good.title);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestSetNullTitle() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        good.setTitle(null);
    }

    // --- price ---
    @Test
    public void TestGetPrice() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, good.getPrice());
    }

    @Test
    public void TestSetPrice() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        good.setPrice(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, good.price);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestSetNullPrice() {
        Good good = new Good(1, "title", BigDecimal.ONE);
        good.setPrice(null);
    }


}