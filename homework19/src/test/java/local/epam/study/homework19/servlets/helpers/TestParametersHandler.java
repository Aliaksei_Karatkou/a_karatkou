package local.epam.study.homework19.servlet.helpers;

import local.epam.study.homework19.servlets.helpers.ParametersHandler;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestParametersHandler {

    @Test(expected = IllegalArgumentException.class)
    public void testParametersHandlerNull() {
        new ParametersHandler(null);
    }

    // ----
    @Test
    public void testStringToIntegerNull() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        assertNull(parametersHandler.stringToInteger(null));
    }

    @Test
    public void testStringToIntegerNotInteger() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        assertNull(parametersHandler.stringToInteger("fdf"));
    }

    @Test
    public void testStringToIntegerNormal() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        assertTrue(parametersHandler.stringToInteger("5") == 5);
    }

    // ------
    @Test
    public void testGetClientName() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        when(request.getParameter("name")).thenReturn("Some name");
        assertEquals("Some name", parametersHandler.getClientName());
    }

    // ----
    @Test
    public void testGetNewRequestedItemIdNormal() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        when(request.getParameter("newid")).thenReturn("7");
        assertTrue(parametersHandler.getNewRequestedItemId() == 7);
    }

    @Test
    public void testGetNewRequestedItemIdNo() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        ParametersHandler parametersHandler = new ParametersHandler(request);
        when(request.getParameter("newid")).thenReturn(null);
        assertNull(parametersHandler.getNewRequestedItemId());
    }
}
