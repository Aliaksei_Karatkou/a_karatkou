package local.epam.study.homework19.services.validators;

import org.junit.Test;


import static local.epam.study.homework19.services.validators.ValidationHelper.validateClientName;
import static local.epam.study.homework19.services.validators.ValidationHelper.validateRequestedGoodItem;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestValidationHelper {

    @Test
    public void TestValidateRequestedGoodItemNormal() {
        assertTrue(validateRequestedGoodItem( 2));
    }

    @Test
    public void TestValidateRequestedGoodItemMininal() {
        assertTrue(validateRequestedGoodItem( 1));
    }
    @Test
    public void TestValidateRequestedGoodItemWrong() {
        assertFalse(validateRequestedGoodItem( 0));
    }
    @Test
    public void TestValidateRequestedGoodItemNull() {
        assertFalse(validateRequestedGoodItem( null));
    }




    //----
    @Test
    public void TestValidateClientNameNameIsNull() {
        assertFalse(validateClientName(null));
    }

    @Test
    public void TestValidateClientNameNameIsEmpty() {
        assertFalse(validateClientName("  "));
    }

    @Test
    public void TestValidateClientNameNormal() {
        assertTrue(validateClientName("Normal name"));
    }
}
