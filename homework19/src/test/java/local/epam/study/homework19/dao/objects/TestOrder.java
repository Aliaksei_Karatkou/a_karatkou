package local.epam.study.homework19.dao.objects;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestOrder {
    @Test
    public void TestOrderConstructor2() {
        Order order = new Order(1,2);
        assertEquals(1,order.id);
        assertEquals(2,order.userId);
        assertEquals(0, order.totalPrice.compareTo(BigDecimal.ZERO));
        ArrayList<Good> goods = (ArrayList) order.getGoods();
        assertTrue(goods.isEmpty());

    }

    @Test(expected = IllegalArgumentException.class)
    public void TestOrderConstructor3Param3Null() {
        Order order = new Order(1,2, null);
    }
    @Test
    public void TestOrderConstructor3() {
        Order order = new Order(1,2, BigDecimal.ONE);
        assertEquals(1,order.id);
        assertEquals(2,order.userId);
        assertEquals(0,order.totalPrice.compareTo(BigDecimal.ONE));
    }
    // --- order id ---
    @Test
    public void TestGetId() {
        Order order = new Order(1,2);
        assertEquals(1,order.getId());
    }

    @Test
    public void TestSetId() {
        Order order = new Order(1,2);
        order.setId(3);
        assertEquals(3,order.id);
    }
    // --- user id ---
    @Test
    public void TestGetUserId() {
        Order order = new Order(1,2);
        assertEquals(2,order.getUserId());
    }

    @Test
    public void TestSetUserId() {
        Order order = new Order(1,2);
        order.setUserId(3);
        assertEquals(3,order.userId);
    }
    // --- user total price  ---
    @Test
    public void TestGetTotalPrice() {
        Order order = new Order(1,2,BigDecimal.TEN);
        assertEquals(0,order.getTotalPrice().compareTo(BigDecimal.TEN));
    }

    @Test
    public void TestSetTotalPrice() {
        Order order = new Order(1,2, BigDecimal.ONE);
        order.setTotalPrice(BigDecimal.TEN);
        assertEquals(0,order.totalPrice.compareTo(BigDecimal.TEN));
    }
    @Test (expected = IllegalArgumentException.class)
    public void TestSetTotalPriceNull() {
        Order order = new Order(1,2);
        order.setTotalPrice(null);
    }
    // --- list ---
    @Test
    public void TestGetGoods() {
        Order order = new Order(1, 2);
        ArrayList<Good> list1 = new ArrayList<>();
        order.goods=list1;
        assertEquals(list1,order.getGoods());
    }
    @Test
    public void TestSetGoods() {
        Order order = new Order(1, 2);
        ArrayList<Good> list1 = new ArrayList<>();
        order.setGoods(list1);
        assertEquals(list1,order.goods);
    }
    @Test (expected = IllegalArgumentException.class)
    public void TestSetGoodsNull() {
        Order order = new Order(1, 2);
        order.setGoods(null);
    }






}
