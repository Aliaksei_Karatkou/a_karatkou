package com.epam.training.a_karatkou.homework05;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.*;


public class TestCommandsInterpreter {

    @Test
    public void testCommandsInterpreterWrongInititalization(){
        boolean thrown = false;
        try {
            CommandsInterpreter cmd = new CommandsInterpreter(null, null);
        } catch (CommandLineInterpreterWrongInitializationException e){
            thrown = true;
        }
        assertTrue(thrown);
    }
    @Test
    public void testCommandsInterpreterSetFileSystemTree() {
        Directory root = new Directory("");
        CommandsInterpreter cmd = new CommandsInterpreter(root, new Scanner(System.in));
        cmd.doInterpreterCommand("a1/b1/c.c");
        Directory dir1 = (Directory) root.items.get("a1");
        assertNotEquals(null,dir1);
        Directory dir2 = (Directory) dir1.items.get("b1");
        assertNotEquals(null,dir2);
        FileSystemItem file = dir2.items.get("c.c");
        assertNotEquals(null,file);
    }

    @Test
    public void testCommandInterpreterShowTree (){
        Directory root = new Directory("");
        CommandsInterpreter cmd = new CommandsInterpreter(root, new Scanner(System.in));
        cmd.doInterpreterCommand("a1/b1/c.c");
        String actual_result = cmd.getDirectoriesStructureString();
        String expected_result = " a1/\n"
                +"  b1/\n"
                +"   c.c\n";
        assertEquals(expected_result,actual_result);
    }
    @Test
    public void testCommandInterpreterAddDirectoryTwice (){
        Directory root = new Directory("");
        CommandsInterpreter cmd = new CommandsInterpreter(root, new Scanner(System.in));
        cmd.doInterpreterCommand("a1/b1/c.c");
        cmd.doInterpreterCommand("a1/b2/c.c");
        String actual_result = cmd.getDirectoriesStructureString();
        String expected_result = " a1/\n"
                +"  b1/\n"
                +"   c.c\n"
                +"  b2/\n"
                +"   c.c\n";
        assertEquals(expected_result,actual_result);
    }

    @Test
    public void testCommandInterpreterDoNullCommand (){
        Directory root = new Directory("");
        CommandsInterpreter cmd = new CommandsInterpreter(root, new Scanner(System.in));
        String actualResult = cmd.doInterpreterCommand(null);
        assertEquals(CommandsInterpreter.STRING_INPUT_PARAMETERS_ERROR, actualResult);
    }

    @Test
    public void testCommandInterpreterParseError (){
        Directory root = new Directory("");
        CommandsInterpreter cmd = new CommandsInterpreter(root, new Scanner(System.in));
        String actualResult = cmd.doInterpreterCommand("///");
        assertEquals(CommandsInterpreter.STRING_PARSING_ERROR, actualResult);
    }



}
