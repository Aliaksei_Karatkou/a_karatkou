package com.epam.training.a_karatkou.homework05;

import org.junit.Test;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestDirectory {
    @Test
    public void testDirectoryPrintNameOneDir() {
        Directory directory = new Directory ("dir1");
        assertEquals("  dir1/\n",directory.printName("  "));
    }

    @Test
    public void testDirectoryAddDirectoryItem() {
        Directory directory = new Directory ("");
        Directory new_dir = directory.addItem("dir1");
        assertEquals("  dir1/\n",new_dir.printName("  "));
    }

    @Test
    public void testDirectoryAddFileItem() {
        Directory directory = new Directory ("");
        String filename = "file1.txt";
        Directory newDirectoryRef = directory.addItem("file1.txt");
        assertEquals("Directory.addItem must return null if file is added",null,newDirectoryRef);
        FileSystemItem fileItem = directory.items.get(filename);
        assertNotEquals("Reference to file item doesn't exist",null,fileItem);
    }
}
