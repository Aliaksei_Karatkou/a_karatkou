package com.epam.training.a_karatkou.homework05;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class to interact with user.
 */
public class CommandsInterpreter {
    /**
     * Command prompt string.
     */
    private static final String COMMAND_PROMPT = "$>";
    /**
     * String to inform about wrong input parameters.
     */
    protected static final String STRING_INPUT_PARAMETERS_ERROR = "Subrouting"
            + " input parameters error\n";
    /**
     * String to inform about parsing error.
     */
    protected static final String STRING_PARSING_ERROR = "Parse error\n";

    /**
     * Sets to true is user want to interrupt input.
     */
    private boolean exitCommandEntered = false;
    /**
     * root directory.
     */
    private Directory root;
    /**
     * used scanner to interact with user.
     */
    private Scanner scanner;

    /**
     * Command to exit from this program.
     */
    private static final String COMMAND_EXIT = "exit";
    /**
     * Command to print filesystem tree.
     */
    private static final String COMMAND_PRINT_TREE = "print";

    /**
     * Constructor.
     * @param rootDirectory root directory object
     * @param usedScanner scanner to use
     */
    public CommandsInterpreter(final Directory rootDirectory,
                               final Scanner usedScanner) {
        if (rootDirectory == null) {
            throw new CommandLineInterpreterWrongInitializationException(
                    "rootDirectory is null");
        }
        if (usedScanner == null) {
            throw new CommandLineInterpreterWrongInitializationException(
                    "scaner is null");
        }
        root = rootDirectory;
        scanner = usedScanner;
    }

    /**
     * Runs filesystem tree generating.
     * @return String with filesystem tree
     */
    final String getDirectoriesStructureString() {
        return root.printName("");
    }

    /**
     * Main mathod for interacting with user.
     */
    public final void listenToCommands() {
        if ((root == null) || (scanner == null)) {
            return;
        }
        do {
            System.out.print(COMMAND_PROMPT);
            String commandLine = scanner.nextLine();
            commandLine = commandLine.trim();
            if (commandLine.isEmpty()) {
                continue;
            } else {
                System.out.print(doInterpreterCommand(commandLine));
            }
        } while (!exitCommandEntered);
    }

    /**
     * Execut user's command.
     * @param command command to execute
     * @return result of the excecution of the command
     */
    final String doInterpreterCommand(final String command) {
        if (command == null) {
            return STRING_INPUT_PARAMETERS_ERROR;
        }
        if (command.equals(COMMAND_PRINT_TREE)) {
            return getDirectoriesStructureString();
        } else if (command.equals(COMMAND_EXIT)) {
            exitCommandEntered = true;
            return "";
        } else {
            PathParser parser = new PathParser();
            ArrayList<String> parsedPath = new ArrayList<String>();
            if (parser.parsePath(command, parsedPath)) {
                Directory currentDir = root;
                for (String newItem : parsedPath) {
                    currentDir = currentDir.addItem(newItem);
                }
                return "";
            } else {
                return STRING_PARSING_ERROR;
            }
        }
    }
}
