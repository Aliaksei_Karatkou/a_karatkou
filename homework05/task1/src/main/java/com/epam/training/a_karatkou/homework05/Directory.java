package com.epam.training.a_karatkou.homework05;

import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Class repesents directory item.
 */
public class Directory implements FileSystemItem {
    /**
     * Directory name.
     */
    private String name;

    /**
     * HashMap to store information about sub-items of the directory.
     * (names and references to sub-items)
     */
    protected HashMap<String, FileSystemItem> items = new HashMap<String,
            FileSystemItem>();

    /**
     * Constructor.
     *
     * @param newDirectoryName The name of the new directory
     */
     public Directory(final String newDirectoryName) {
        name = newDirectoryName;
    }

    /**
     * Returns String with formatted output of the directory content.
     * Output also insludes subdirectories.
     *
     * @param spacesForFormating String that used to format output
     * @return String of the directory content.
     * Also includes subdirectories.
     */
    public final  String printName(final String spacesForFormating) {


        /**
         * result accumulating variable
         */
        String result = "";
        if (spacesForFormating.length() > 0) {
            result = spacesForFormating + name + "/\n";
        }
        String newLevelSpacesForFormating = spacesForFormating + " ";
        SortedSet<String> itemsList = new TreeSet<String>(items.keySet());
        for (String itemName : itemsList) {
            FileSystemItem nextItem = items.get(itemName);
            result += nextItem.printName(newLevelSpacesForFormating);
        }
        return result;
    }

    /**
     * Adds item to the directory.
     *
     * @param itemName new item name
     * @return Sub-Directory or null (in case of a file)
     */

    public final  Directory addItem(final String itemName) {
        if (items.containsKey(itemName)) {
            return (Directory) items.get(itemName);
        }
        Directory newRef = null;
        if (itemName.contains(".")) {
            items.put(itemName, new File(itemName));
        } else {
            newRef = new Directory(itemName);
            items.put(itemName, newRef);
        }
        return newRef;
    }
}
