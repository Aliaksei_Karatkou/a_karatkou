package com.epam.training.a_karatkou.homework05;

import java.util.Scanner;



/**
 * Class for implementing homework 5.
 */
public final class Task1 {
    /**
     * main method.
     *
     * @param args Command line arguments
     */
    public static void main(final String[] args) {
        CommandsInterpreter commandsInterpreter =
                new CommandsInterpreter(new Directory(""),
                        new Scanner(System.in));
        commandsInterpreter.listenToCommands();
    }

    /**
     * Constructor (hidden).
     */
    private Task1() {

    }
}

