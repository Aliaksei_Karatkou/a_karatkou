package com.epam.training.a_karatkou.homework05;

/**
 * interface to implement Composite pattern.
 */
interface FileSystemItem {
    /**
     * Method used to generate directory tree.
     *
     * @param spacesForFormating Addition of spaces,
     *                           that used to format output.
     * @return string with directory tree (may include "\\n")
     */
    String printName(String spacesForFormating);
}
