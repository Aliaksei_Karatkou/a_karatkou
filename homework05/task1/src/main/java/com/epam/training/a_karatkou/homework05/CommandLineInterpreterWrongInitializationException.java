package com.epam.training.a_karatkou.homework05;

/**
 * Exception to throw if any parameter of a constructor is incorrect.
 */
public class CommandLineInterpreterWrongInitializationException
        extends RuntimeException {
    /**
     * Exception throw method.
     * @param message message to explain exception.
     */
    protected CommandLineInterpreterWrongInitializationException(
            final String message) {
        super(message);
    }
}
